class ClientUsersDatatable
  delegate :params, :h, :link_to, :raw, :edit_client_user_path, :policy, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(ClientUser).count,
      iTotalDisplayRecords: client_users.total_entries,
      aaData: data
    }
  end

private

  def data
    client_users.map do |client_user|
      [
        client_user.email,
        client_user.client.name,
        (client_user.current_sign_in_at.nil? ? '' : client_user.current_sign_in_at.strftime('%Y-%m-%d %H:%M:%S')),
				'<span class="btn-group">'+
          (policy(client_user).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_client_user_path(client_user), { :class => 'btn btn-small' }) : "")+
          (policy(client_user).destroy? ? link_to(raw("<i class='icon-trash'></i>"), client_user, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "") +
          '</span>'
      ]
    end
  end

  def client_users
    @client_users ||= fetch_client_users
  end

  def fetch_client_users
    client_users = policy_scope(ClientUser).order("#{sort_column} #{sort_direction}")
    client_users = client_users.joins(:client).includes(:client)
    
    client_users = client_users.page(page).per_page(per_page)
    client_users
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[client_users.email client_users.name client_users.current_sign_in_at]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
