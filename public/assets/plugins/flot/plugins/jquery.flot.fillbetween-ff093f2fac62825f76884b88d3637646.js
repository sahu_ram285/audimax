/*
Flot plugin for computing bottoms for filled line and bar charts.

The case: you've got two series that you want to fill the area
between. In Flot terms, you need to use one as the fill bottom of the
other. You can specify the bottom of each data point as the third
coordinate manually, or you can use this plugin to compute it for you.

In order to name the other series, you need to give it an id, like this

  var dataset = [
       { data: [ ... ], id: "foo" } ,         // use default bottom
       { data: [ ... ], fillBetween: "foo" }, // use first dataset as bottom
       ];

  $.plot($("#placeholder"), dataset, { line: { show: true, fill: true }});

As a convenience, if the id given is a number that doesn't appear as
an id in the series, it is interpreted as the index in the array
instead (so fillBetween: 0 can also mean the first series).
  
Internally, the plugin modifies the datapoints in each series. For
line series, extra data points might be inserted through
interpolation. Note that at points where the bottom line is not
defined (due to a null point or start/end of line), the current line
will show a gap too. The algorithm comes from the jquery.flot.stack.js
plugin, possibly some code could be shared.
*/
(function(e){function n(e){function t(e,t){var n;for(n=0;n<t.length;++n)if(t[n].id==e.fillBetween)return t[n];return typeof e.fillBetween=="number"?(n=e.fillBetween,n<0||n>=t.length?null:t[n]):null}function n(e,n,r){if(n.fillBetween==null)return;var i=t(n,e.getData());if(!i)return;var s=r.pointsize,o=r.points,u=i.datapoints.pointsize,a=i.datapoints.points,f=[],l,c,h,p,d,v,g=n.lines.show,y=s>2&&r.format[2].y,b=g&&n.lines.steps,w=!0,E=0,S=0,x;for(;;){if(E>=o.length)break;x=f.length;if(o[E]==null){for(m=0;m<s;++m)f.push(o[E+m]);E+=s}else if(S>=a.length){if(!g)for(m=0;m<s;++m)f.push(o[E+m]);E+=s}else if(a[S]==null){for(m=0;m<s;++m)f.push(null);w=!0,S+=u}else{l=o[E],c=o[E+1],p=a[S],d=a[S+1],v=0;if(l==p){for(m=0;m<s;++m)f.push(o[E+m]);v=d,E+=s,S+=u}else if(l>p){if(g&&E>0&&o[E-s]!=null){h=c+(o[E-s+1]-c)*(p-l)/(o[E-s]-l),f.push(p),f.push(h);for(m=2;m<s;++m)f.push(o[E+m]);v=d}S+=u}else{if(w&&g){E+=s;continue}for(m=0;m<s;++m)f.push(o[E+m]);g&&S>0&&a[S-u]!=null&&(v=d+(a[S-u+1]-d)*(l-p)/(a[S-u]-p)),E+=s}w=!1,x!=f.length&&y&&(f[x+2]=v)}if(b&&x!=f.length&&x>0&&f[x]!=null&&f[x]!=f[x-s]&&f[x+1]!=f[x-s+1]){for(m=0;m<s;++m)f[x+s+m]=f[x+m];f[x+1]=f[x-s+1]}}r.points=f}e.hooks.processDatapoints.push(n)}var t={series:{fillBetween:null}};e.plot.plugins.push({init:n,options:t,name:"fillbetween",version:"1.0"})})(jQuery);