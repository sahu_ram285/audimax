class CreateMaintenances < ActiveRecord::Migration
  def change
    create_table :maintenances do |t|
      t.integer :equipment_id
      t.string :maintenance_type
      t.date :maintenance_date
      t.integer :employee_id
      t.float :calibration_result
      t.string :calibration_approved

      t.timestamps
    end
  end
end
