class AddIndexToObjectAttachmentsOnInspectionObjectId < ActiveRecord::Migration
  def change
    add_index :object_attachments, :inspection_object_id
  end
end
