class EquipmentController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /equipment
  # GET /equipment.json
  def index
    @equipment = policy_scope(Equipment).search(params[:equipment_type_id], params[:nr], params[:serial_nr], params[:employee_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: EquipmentDatatable.new(view_context) }
    end
  end

  # GET /equipment/1
  # GET /equipment/1.json
  def show
    @equipment = Equipment.find(params[:id])
    authorize @equipment

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @equipment }
    end
  end

  # GET /equipment/new
  # GET /equipment/new.json
  def new
    @equipment = Equipment.new
    authorize @equipment

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @equipment }
    end
  end

  # GET /equipment/1/edit
  def edit
    @equipment = Equipment.find(params[:id])
    authorize @equipment
  end

  # POST /equipment
  # POST /equipment.json
  def create
    @equipment = Equipment.new(params[:equipment])
    authorize @equipment

    respond_to do |format|
      if @equipment.save
        format.html { redirect_to equipment_index_path, notice: I18n.t('notices.object_sucessfully_created', model: Equipment.model_name.human) }
        format.json { render json: @equipment, status: :created, location: @equipment }
      else
        format.html { render action: "new" }
        format.json { render json: @equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /equipment/1
  # PUT /equipment/1.json
  def update
    @equipment = Equipment.find(params[:id])
    authorize @equipment

    respond_to do |format|
      if @equipment.update_attributes(params[:equipment])
        format.html { redirect_to equipment_index_path, notice: I18n.t('notices.object_sucessfully_updated', model: Equipment.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment/1
  # DELETE /equipment/1.json
  def destroy
    @equipment = Equipment.find(params[:id])
    authorize @equipment
    @equipment.destroy

    respond_to do |format|
      format.html { redirect_to equipment_index_url }
      format.json { head :no_content }
    end
  end
  
  def set_current_section
    self.current_section = :assets
  end  
  
end
