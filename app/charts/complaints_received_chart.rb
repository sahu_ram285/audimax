class ComplaintsReceivedChart

  def ticks
    (1..12).map { |n| [n, (Date.today - (12-n).months).strftime("%Y-%m")] }
  end
  
  def series_count
    1
  end
  
  def series_data(series)
    data = Array.new(12, 0)
    Complaint.select("11 - period_diff(date_format(now(), '%Y%m'), date_format(complaints.received_date, '%Y%m')) as month, count(*) as nr_complaints").where("complaints.received_date > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)").group("1").order("1").each_with_index do |c|
      data[c.month] = c.nr_complaints
    end
    data.each_with_index.map { |e, i| [i+1, e]}
  end
  
  def series_title(series)
    I18n.t('charts.complaints_received.complaints_received')
  end
  
  def as_json(options = {})
    {
      data: [{
        data: series_data(0),
      	label: series_title(0),
      	color: "#c75d7b"
      }],
      options: {
        tooltip: true,
        series: {
          lines: {
        	  show: true
        	},
        	points: {
        	  show: true
        	}
        },
				grid: {
          borderWidth: 0, 
        	hoverable: true,
        	clickable: true
        },
        xaxis: {
          ticks: ticks
        },
        yaxis: {
          min: 0,
          tickDecimals: 0
        }
      }
    }
  end

end
