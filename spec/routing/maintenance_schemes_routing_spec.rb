require "spec_helper"

describe MaintenanceSchemesController do
  describe "routing" do

    it "routes to #index" do
      get("/maintenance_schemes").should route_to("maintenance_schemes#index")
    end

    it "routes to #new" do
      get("/maintenance_schemes/new").should route_to("maintenance_schemes#new")
    end

    it "routes to #show" do
      get("/maintenance_schemes/1").should route_to("maintenance_schemes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/maintenance_schemes/1/edit").should route_to("maintenance_schemes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/maintenance_schemes").should route_to("maintenance_schemes#create")
    end

    it "routes to #update" do
      put("/maintenance_schemes/1").should route_to("maintenance_schemes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/maintenance_schemes/1").should route_to("maintenance_schemes#destroy", :id => "1")
    end

  end
end
