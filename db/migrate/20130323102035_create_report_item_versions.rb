class CreateReportItemVersions < ActiveRecord::Migration
  def change
    create_table :report_item_versions do |t|
      t.integer :report_item_id
      t.integer :report_id
      t.integer :section_id
      t.string :assessment
      t.text :comments
      t.integer :user_id
      t.string :action

      t.timestamps
    end
  end
end
