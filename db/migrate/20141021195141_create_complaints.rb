class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.string :status
      t.string :priority
      t.string :complaint_type
      t.date :received_date
      t.string :complainant_name
      t.string :complainant_address
      t.string :complainant_postalcode
      t.string :complainant_city
      t.string :complainant_state
      t.string :complainant_country
      t.string :complainant_phone1
      t.string :complainant_phone2
      t.string :complainant_email
      t.integer :complainant_employee_id
      t.integer :complainant_client_id
      t.text :notes
      t.integer :assigned_to_id
      t.integer :owner_id
      t.integer :inspection_object_id
      t.integer :client_id
      t.integer :employee_id
      t.integer :report_id
      t.date :planned_handled_date
      t.date :handled_date
      t.text :complaint
      t.text :cause
      t.text :impact
      t.text :resolution

      t.timestamps
    end
  end
end
