class Equipment < ActiveRecord::Base
  attr_accessible :employee_id, :equipment_type_id, :in_service_date, :last_calibration_date, :nr, :out_of_service_date, :serial_nr, :usage
  
  belongs_to :equipment_type
  belongs_to :employee
  has_many :maintenances, :dependent => :destroy
  
  validates :employee_id, :equipment_type_id, :presence => true
  
  #
  # search
  #
  
  def self.search(equipment_type_id, nr, serial_nr, employee_id)
    equipment = scoped
    equipment = equipment.joins(:equipment_type, :employee).includes(:equipment_type, :employee)
    equipment = equipment.where("equipment.equipment_type_id = :equipment_type_id", :equipment_type_id => equipment_type_id) if equipment_type_id.present?
    equipment = equipment.where("equipment.nr = :nr", :nr => nr) if nr.present?
    equipment = equipment.where("equipment.serial_nr = :serial_nr", :serial_nr => serial_nr) if serial_nr.present?
    equipment = equipment.where("equipment.employee_id = :employee_id", :employee_id => employee_id) if employee_id.present?
    equipment = equipment.order("equipment_types.name")
  end 
  
end
