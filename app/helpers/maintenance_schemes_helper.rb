module MaintenanceSchemesHelper
  
	def maintenance_type_select_options
    [[Maintenance.human_attribute_name('maintenance_type.ca'), 'CA']]
	end
  
	def maintenance_type_label(maintenance_type)
    Maintenance.human_attribute_name("maintenance_type.#{maintenance_type.downcase}")
	end
  
end
