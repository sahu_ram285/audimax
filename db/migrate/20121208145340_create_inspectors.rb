class CreateInspectors < ActiveRecord::Migration
  def change
    create_table :inspectors do |t|
      t.string :firstname
      t.string :lastname
      t.string :address
      t.string :postalcode
      t.string :city
      t.string :state
      t.string :phone
      t.string :email
      t.string :image

      t.timestamps
    end
  end
end
