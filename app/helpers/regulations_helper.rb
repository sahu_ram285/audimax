module RegulationsHelper
  
	def regulation_status_label(status)
    label_class = {'NEW' => 'default', 'FINAL' => 'success'}[status]
    status_name = Regulation.human_attribute_name("status.#{status.downcase}")
		raw("<span class='label label-#{label_class}'>#{status_name}</span>")
	end
  
end
