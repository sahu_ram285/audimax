class ReportErrorCategory < ActiveRecord::Base
  attr_accessible :description, :name, :requires_assessment
end
