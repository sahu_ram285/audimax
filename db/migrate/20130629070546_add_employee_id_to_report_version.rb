class AddEmployeeIdToReportVersion < ActiveRecord::Migration
  def change
    add_column :report_versions, :employee_id, :integer
  end
end
