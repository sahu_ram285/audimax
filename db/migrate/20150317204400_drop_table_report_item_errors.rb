class DropTableReportItemErrors < ActiveRecord::Migration
  def up
    drop_table :report_item_errors
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
