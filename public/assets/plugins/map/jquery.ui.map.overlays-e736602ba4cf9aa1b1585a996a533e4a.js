/*!
 * jQuery UI Google Map 3.0-rc
 * http://code.google.com/p/jquery-ui-map/
 * Copyright (c) 2010 - 2012 Johan Säll Larsson
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *      jquery.ui.map.js
 */
(function(e){e.extend(e.ui.gmap.prototype,{addShape:function(t,n){var r=new google.maps[t](jQuery.extend({map:this.get("map")},n));return this.get("overlays > "+t,[]).push(r),e(r)},loadFusion:function(e,t){(t?this.get("overlays > FusionTablesLayer",new google.maps.FusionTablesLayer(t,e)):this.get("overlays > FusionTablesLayer",new google.maps.FusionTablesLayer)).setOptions(jQuery.extend({map:this.get("map")},e))},loadKML:function(e,t,n){this.get("overlays > "+e,new google.maps.KmlLayer(t,jQuery.extend({map:this.get("map")},n)))}})})(jQuery);