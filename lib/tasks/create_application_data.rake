namespace :audimax do
	desc "Create application data"
	task :create_application_data => :environment do
            assessment = Assessment.new(:code => 'COM', :name => 'Compliant')
            assessment.save!

            assessment = Assessment.new(:code => 'NC1', :name => 'Non Compliant Major')
            assessment.save!

            assessment = Assessment.new(:code => 'NC2', :name => 'Non Compliant Minor')
            assessment.save!

            assessment = Assessment.new(:code => 'NA', :name => 'Not Applicable')
            assessment.save!

            assessment = Assessment.new(:code => 'NONE', :name => 'Not Assessed')
            assessment.save!
	end
end
