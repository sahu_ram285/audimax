class ReportErrorCategoriesController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /report_error_categories
  # GET /report_error_categories.json
  def index
    @report_error_categories = policy_scope(ReportErrorCategory).order("name")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @report_error_categories }
    end
  end

  # GET /report_error_categories/1
  # GET /report_error_categories/1.json
  def show
    @report_error_category = ReportErrorCategory.find(params[:id])
    authorize @report_error_category

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @report_error_category }
    end
  end

  # GET /report_error_categories/new
  # GET /report_error_categories/new.json
  def new
    @report_error_category = ReportErrorCategory.new
    authorize @report_error_category

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @report_error_category }
    end
  end

  # GET /report_error_categories/1/edit
  def edit
    @report_error_category = ReportErrorCategory.find(params[:id])
    authorize @report_error_category
  end

  # POST /report_error_categories
  # POST /report_error_categories.json
  def create
    @report_error_category = ReportErrorCategory.new(params[:report_error_category])
    authorize @report_error_category

    respond_to do |format|
      if @report_error_category.save
        format.html { redirect_to report_error_categories_path, notice: I18n.t('notices.object_sucessfully_created', model: ReportErrorCategory.model_name.human) }
        format.json { render json: @report_error_category, status: :created, location: @report_error_category }
      else
        format.html { render action: "new" }
        format.json { render json: @report_error_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /report_error_categories/1
  # PUT /report_error_categories/1.json
  def update
    @report_error_category = ReportErrorCategory.find(params[:id])
    authorize @report_error_category

    respond_to do |format|
      if @report_error_category.update_attributes(params[:report_error_category])
        format.html { redirect_to report_error_categories_path, notice: I18n.t('notices.object_sucessfully_updated', model: ReportErrorCategory.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @report_error_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /report_error_categories/1
  # DELETE /report_error_categories/1.json
  def destroy
    @report_error_category = ReportErrorCategory.find(params[:id])
    authorize @report_error_category
    @report_error_category.destroy

    respond_to do |format|
      format.html { redirect_to report_error_categories_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end  
  
end
