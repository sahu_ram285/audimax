class AddDeletedToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :deleted, :boolean
  end
end
