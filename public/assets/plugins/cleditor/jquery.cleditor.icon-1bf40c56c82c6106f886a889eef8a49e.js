/**
 @preserve CLEditor Icon Plugin v1.0
 http://premiumsoftware.net/cleditor
 requires CLEditor v1.2 or later
 
 Copyright 2010, Chris Landowski, Premium Software, LLC
 Dual licensed under the MIT or GPL Version 2 licenses.
*/
// ==ClosureCompiler==
// @compilation_level SIMPLE_OPTIMIZATIONS
// @output_file_name jquery.cleditor.icon.min.js
// ==/ClosureCompiler==
(function(e){var t=e.cleditor.imagesPath()+"icons/",n="icons",r=".gif",i="URL("+t+n+r+")",s=12,o=20,u=20;e.cleditor.buttons.icon={name:"icon",css:{backgroundImage:i,backgroundPosition:"2px 2px"},title:"Insert Icon",command:"insertimage",popupName:"Icon",popupHover:!0,buttonClick:function(t,n){e(n.popup).width(60)},popupClick:function(e,n){var i=-parseInt(e.target.style.backgroundPosition)/o+1;n.value=t+i+r}};var a=e("<div>");for(var f=0;f<s;f++)e("<div>").css({width:o,height:u,backgroundImage:i,backgroundPosition:f*-o}).css("float","left").appendTo(a);e.cleditor.buttons.icon.popupContent=a.children(),e.cleditor.defaultOptions.controls=e.cleditor.defaultOptions.controls.replace("| cut","icon | cut")})(jQuery);