class ComplaintsController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /complaints
  # GET /complaints.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: ComplaintsDatatable.new(view_context) }
    end
  end

  # GET /complaints/1
  # GET /complaints/1.json
  def show
    @complaint = Complaint.find(params[:id])
    authorize @complaint

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @complaint }
    end
  end

  # GET /complaints/new
  # GET /complaints/new.json
  def new
    @complaint = Complaint.new
    authorize @complaint

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @complaint }
    end
  end

  # GET /complaints/1/edit
  def edit
    @complaint = Complaint.find(params[:id])
    authorize @complaint
  end

  # POST /complaints
  # POST /complaints.json
  def create
    @complaint = Complaint.new(params[:complaint])
    authorize @complaint
    @complaint.complaint_actions.build(:user_id => current_user.id, :complaint_status_new => @complaint.status, :action => params[:complaint][:action])

    respond_to do |format|
      if @complaint.save
        format.html { redirect_to complaints_path, notice: I18n.t('notices.object_sucessfully_created', model: Complaint.model_name.human) }
        format.json { render json: @complaint, status: :created, location: @complaint }
      else
        format.html { render action: "new" }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /complaints/1
  # PUT /complaints/1.json
  def update
    @complaint = Complaint.find(params[:id])
    authorize @complaint
    @complaint.assign_attributes(params[:complaint])
    @complaint.complaint_actions.build(:user_id => current_user.id, :complaint_status_old => @complaint.status_was, :complaint_status_new => @complaint.status, :action => params[:complaint][:action])

    respond_to do |format|
      if @complaint.save
        format.html { redirect_to complaints_path, notice: I18n.t('notices.object_sucessfully_updated', model: Complaint.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /complaints/1
  # DELETE /complaints/1.json
  def destroy
    @complaint = Complaint.find(params[:id])
    authorize @complaint
    @complaint.destroy

    respond_to do |format|
      format.html { redirect_to complaints_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :complaints
  end
  
end
