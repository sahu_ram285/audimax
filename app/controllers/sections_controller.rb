class SectionsController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /sections/new
  # GET /sections/new.json
  def new
    @regulation = Regulation.find(params[:regulation_id])
    @section = @regulation.sections.build
    authorize @section
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @section }
    end
  end

  # GET /sections/1/edit
  def edit
    @section = Section.find(params[:id])
    authorize @section
    @regulation = Regulation.find(@section.regulation_id)
  end

  # POST /sections
  # POST /sections.json
  def create
  	@regulation = Regulation.find(params[:regulation_id])
  	@section = @regulation.sections.build(params[:section])
    authorize @section

    respond_to do |format|
      if @section.save
        format.html { redirect_to regulation_url(@regulation), notice: I18n.t('notices.object_sucessfully_created', model: Section.model_name.human) }
        format.json { render json: @section, status: :created, location: @section }
      else
        format.html { render action: "new" }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sections/1
  # PUT /sections/1.json
  def update
    @section = Section.find(params[:id])
    authorize @section
    scopes = Scope.find(params[:scope_ids]) rescue []
	  @section.scopes = scopes

    respond_to do |format|
      if @section.update_attributes(params[:section])
        format.html { redirect_to regulation_url(@section.regulation_id), notice: I18n.t('notices.object_sucessfully_updated', model: Section.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section = Section.find(params[:id])
    authorize @section
    @section.destroy

    respond_to do |format|
      format.html { redirect_to regulation_url(@section.regulation_id) }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end  
  
end
