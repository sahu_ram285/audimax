class CreateReportItemErrors < ActiveRecord::Migration
  def change
    create_table :report_item_errors do |t|
      t.integer :report_review_item_id
      t.integer :report_error_category_id
      t.text :description

      t.timestamps
    end
  end
end
