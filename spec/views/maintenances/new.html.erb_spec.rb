require 'spec_helper'

describe "maintenances/new" do
  before(:each) do
    assign(:maintenance, stub_model(Maintenance,
      :equipment_id => 1,
      :maintenance_type => "MyString",
      :employee_id => 1,
      :calibration_result => 1.5,
      :calibration_approved => "MyString"
    ).as_new_record)
  end

  it "renders new maintenance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", maintenances_path, "post" do
      assert_select "input#maintenance_equipment_id[name=?]", "maintenance[equipment_id]"
      assert_select "input#maintenance_maintenance_type[name=?]", "maintenance[maintenance_type]"
      assert_select "input#maintenance_employee_id[name=?]", "maintenance[employee_id]"
      assert_select "input#maintenance_calibration_result[name=?]", "maintenance[calibration_result]"
      assert_select "input#maintenance_calibration_approved[name=?]", "maintenance[calibration_approved]"
    end
  end
end
