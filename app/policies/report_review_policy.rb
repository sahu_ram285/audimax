class ReportReviewPolicy < ApplicationPolicy
    
  def create?
    @user.admin? or @user.has_role(:administrator, :manager, :reviewer)
  end
  
  def update?
    @user.admin? or (@user.has_role(:administrator, :manager, :reviewer) and not record.final?)
  end
  
  def destroy?
    @user.admin? or (@user.has_role(:administrator, :manager, :reviewer) and not record.final?)
  end
  
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if @user.admin? or @user.has_role(:administrator, :manager, :reviewer)
        scope
      else
        scope.joins(:report).where('reports.employee_id = :employee_id', :employee_id => @user.employee_id)
      end
    end
  end

end