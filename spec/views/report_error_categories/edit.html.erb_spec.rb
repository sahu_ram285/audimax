require 'spec_helper'

describe "report_error_categories/edit" do
  before(:each) do
    @report_error_category = assign(:report_error_category, stub_model(ReportErrorCategory,
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit report_error_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => report_error_categories_path(@report_error_category), :method => "post" do
      assert_select "input#report_error_category_name", :name => "report_error_category[name]"
      assert_select "textarea#report_error_category_description", :name => "report_error_category[description]"
    end
  end
end
