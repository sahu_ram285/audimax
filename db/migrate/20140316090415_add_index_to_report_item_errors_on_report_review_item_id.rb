class AddIndexToReportItemErrorsOnReportReviewItemId < ActiveRecord::Migration
  def change
    add_index :report_item_errors, :report_review_item_id
  end
end
