class AddReportIdToReportReview < ActiveRecord::Migration
  def change
    add_column :report_reviews, :report_id, :integer
  end
end
