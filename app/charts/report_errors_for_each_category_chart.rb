class ReportErrorsForEachCategoryChart
  
  def series_data
    ReportErrorCategory.joins('left outer join (select report_error_category_id, count(*) tot from report_errors inner join report_reviews on report_reviews.id = report_errors.report_review_id where report_reviews.created_at > date_sub(date_format(now(), \'%Y-%m-01\'), interval 11 month) group by report_error_category_id) t on report_error_categories.id = t.report_error_category_id').select('id, name, ifnull(tot,0) tot').all.map { |cat| {label: cat.name, data: [[0, cat.tot]]} }
  end

  def as_json(options = {})
    { data: series_data,
      options: {
        bars: {
            show: true, 
            barWidth: 1, 
            order: 1
        },
        xaxis: {
	        min: -5,
	        max: 5,
          ticks: [[0, I18n.t('charts.report_errors_for_each_category.errors_per_category')]]
        },
        yaxis: {
	        min: 0,
	        tickDecimals: 0
        },
        legend: {
            show: true,
	          container: "#chart-errors-per-category-legend"
        },
        grid: {
          hoverable: true
        }
      }
    }
  end
  
end
