require 'tokenizer'

class SessionsController < Devise::SessionsController
  
  def create
    logger.debug "SessionController::create"
    self.resource = warden.authenticate!(auth_options)
    
    if self.resource.class == User
      if self.resource.two_step_verification == 'TOKENIZER'
        warden.logout
        tokenizer = Tokenizer.new
        session[:tokenizer_user_id] = resource.id
      
        begin
          response = tokenizer.generate_token(self.resource.email)
        rescue Exception => e
          logger.debug "Error generating two-step verification token: #{e}"
          response = {}
        end
      
        if response.has_key?('id')
          session[:token_id] = response['id']
          respond_with resource, :location => accounts_users_verify_path
        else
          # Something went wrong, maybe user has no tokenizer account. Just return to the login page.
          # set resource to nil otherwise login form will use PUT i.s.o POST
          self.resource = nil
          render :new
        end
      else
        # No two-step verification. Do default login procedure
        super
      end
    else
      super
    end
  end
  
  def verify
    verification_successful = false
    
    @token_id = session[:token_id]
    
    tokenizer = Tokenizer.new
    
    begin
      response = tokenizer.verify_token(session[:token_id])
    rescue Exception => e
      logger.debug "Error verifying two-step verification token: #{e}"
      response = {}
    end

    if response['state'] == 'accepted'
      verification_successful = true
      user = User.find(session[:tokenizer_user_id])
      sign_in(user)
      url = after_sign_in_path_for(user)
    end
    
    respond_to do |format|
      if verification_successful
        format.html { redirec_to url } # todo: redirect to after login path
        format.json { render json: { :state => 'accepted', :url => url }}
      else
        format.html
        format.json { render json: { :state => 'pending', :url => root_url }}
      end
    end
    
  end

end
