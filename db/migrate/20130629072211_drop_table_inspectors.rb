class DropTableInspectors < ActiveRecord::Migration
  def up
    drop_table :inspectors
  end

  def down
  end
end
