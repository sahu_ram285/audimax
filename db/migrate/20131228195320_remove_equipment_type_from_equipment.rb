class RemoveEquipmentTypeFromEquipment < ActiveRecord::Migration
  def up
    remove_column :equipment, :equipment_type
  end

  def down
    add_column :equipment, :equipment_type, :string
  end
end
