/*
 * MWS Admin v2.1 - Login JS
 * This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
 * All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
 * Last Updated:
 * December 08, 2012
 *
 */
(function(e){e(document).ready(function(){e("#mws-login-form form").validate({rules:{username:{required:!0},password:{required:!0}},errorPlacement:function(e,t){},invalidHandler:function(t,n){e.fn.effect&&e("#mws-login").effect("shake",{distance:6,times:2},35)}}),e.fn.placeholder&&e("[placeholder]").placeholder()})})(jQuery);