class AddCountryToInspector < ActiveRecord::Migration
  def change
    add_column :inspectors, :country, :string
  end
end
