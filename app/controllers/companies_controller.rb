class CompaniesController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /companies
  # GET /companies.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: CompaniesDatatable.new(view_context) }
    end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    @company = Company.find(params[:id])
    authorize @company

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/new
  # GET /companies/new.json
  def new
    @company = Company.new
    authorize @company

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/1/edit
  def edit
    @company = Company.find(params[:id])
    authorize @company
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(params[:company])
    authorize @company

    respond_to do |format|
      if @company.save
        format.html { redirect_to companies_path, notice:  I18n.t('notices.object_sucessfully_created', model: Company.model_name.human) }
        format.json { render json: @company, status: :created, location: @company }
      else
        format.html { render action: "new" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PUT /companies/1
  # PUT /companies/1.json
  def update
    @company = Company.find(params[:id])
    authorize @company

    respond_to do |format|
      if @company.update_attributes(params[:company])
        format.html { redirect_to companies_path, notice: I18n.t('notices.object_sucessfully_updated', model: Company.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /companies/1/destroy_image
	def destroy_image
    @company = Company.find(params[:id])
    authorize @company

		@company.remove_image = true
		@company.save
    respond_to do |format|
      # format.html { redirect_to companies_url }
      format.js
    end		
	end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company = Company.find(params[:id])
    authorize @company
    @company.destroy

    respond_to do |format|
      format.html { redirect_to companies_url }
      format.json { head :no_content }
    end
  end

  private
  
  def set_current_section
    self.current_section = :company
  end
	
end
