class ApplicationController < ActionController::Base
  include AbstractController::Callbacks
  include Pundit
  protect_from_forgery
  
  before_filter  :set_locale
  
  layout :layout_by_resource
  
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  helper_method :current_section, :current_section?
  
  def current_section
    @current_section
  end
  
  def current_section=(section)
    @current_section = section
  end
  
  def current_section?(section)
    @current_section == section
  end

  private

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end
  
  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope == :client_user
      client_root_path
    else
      root_path
    end
  end
    
  def layout_by_resource
    if devise_controller? && resource_name == :client_user
      "client"
    else
      "application"
    end
  end
  
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  
  def self.default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end

end
