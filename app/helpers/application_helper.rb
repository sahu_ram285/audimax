module ApplicationHelper

  # strips the tags the javascript editor (cleditor) may leave behind
  def strip_editor_residue(s)
    s.gsub(/<div><br\/><\/div>|<br\/>/, "") unless s.nil?
  end
  
  def boolean_to_yesno(b)
    b ? t('form.yes') : t('form.no')
  end
  
  def country_name(country_code)
    country = ISO3166::Country[country_code]
    if country 
      country.translations[I18n.locale.to_s] || country.name
    else
      country_code
    end
  end
  
end
