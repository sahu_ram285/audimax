class ComplaintsDashboard
  
  def nr_complaints
    Complaint.last_12_months.count
  end

  def nr_open_complaints
    Complaint.where('status != ?', 'CLOSED').count
  end
  
  def days_since_last_complaint
    last_complaint = Complaint.order(:received_date).last
    if !last_complaint.nil?
      (Date.today - last_complaint.received_date).to_i
    end
  end
  
  def average_handling_days
    Complaint.last_12_months.where('status = ?', 'CLOSED').select('avg(datediff(complaints.handled_date, complaints.received_date)) as average_handling_days').first.average_handling_days.to_i
  end

end
