class RemoveEquipmentNrFromEquipment < ActiveRecord::Migration
  def up
    remove_column :equipment, :equipment_nr
  end

  def down
    add_column :equipment, :equipment_nr, :string
  end
end
