/*
Flot plugin for thresholding data. Controlled through the option
"threshold" in either the global series options

  series: {
    threshold: {
      below: number
      color: colorspec
    }
  }

or in a specific series

  $.plot($("#placeholder"), [{ data: [ ... ], threshold: { ... }}])

The data points below "below" are drawn with the specified color. This
makes it easy to mark points below 0, e.g. for budget data.

Internally, the plugin works by splitting the data into two series,
above and below the threshold. The extra series below the threshold
will have its label cleared and the special "originSeries" attribute
set to the original series. You may need to check for this in hover
events.
*/
(function(e){function n(t){function n(t,n,r){if(!n.threshold)return;var i=r.pointsize,s,o,u,a,f,l=e.extend({},n);l.datapoints={points:[],pointsize:i},l.label=null,l.color=n.threshold.color,l.threshold=null,l.originSeries=n,l.data=[];var c=n.threshold.below,h=r.points,p=n.lines.show;threspoints=[],newpoints=[];for(s=0;s<h.length;s+=i){o=h[s],u=h[s+1],f=a,u<c?a=threspoints:a=newpoints;if(p&&f!=a&&o!=null&&s>0&&h[s-i]!=null){var d=(o-h[s-i])/(u-h[s-i+1])*(c-u)+o;f.push(d),f.push(c);for(m=2;m<i;++m)f.push(h[s+m]);a.push(null),a.push(null);for(m=2;m<i;++m)a.push(h[s+m]);a.push(d),a.push(c);for(m=2;m<i;++m)a.push(h[s+m])}a.push(o),a.push(u)}r.points=newpoints,l.datapoints.points=threspoints,l.datapoints.points.length>0&&t.getData().push(l)}t.hooks.processDatapoints.push(n)}var t={series:{threshold:null}};e.plot.plugins.push({init:n,options:t,name:"threshold",version:"1.0"})})(jQuery);