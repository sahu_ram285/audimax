class NotificationsDatatable
  delegate :params, :raw, :link_to, :current_user, :toggle_read_notification_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: current_user.notifications.visible.count,
      iTotalDisplayRecords: notifications.total_entries,
      aaData: data
    }
  end

private

  def data
    notifications.map do |notification|
      [
        link_to(raw((notification.read ? "<i class='icol-email-open'</i>" : "<i class='icol-envelope'</i>")), toggle_read_notification_path(notification), method: :put),
        notification.message,
        notification.created_at.strftime("%Y-%m-%d %H:%M:%S"),
				'<span class="btn-group">'+
          link_to(raw("<i class='icon-trash'></i>"), notification, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small')+
          '</span>'
      ]
    end
  end

  def notifications
    @notifications ||= fetch_notifications
  end

  def fetch_notifications
    notifications = current_user.notifications.visible.order("#{sort_column} #{sort_direction}")
    notifications = notifications.page(page).per_page(per_page)
    notifications
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[notifications.read notifications.message notifications.created_at ]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end