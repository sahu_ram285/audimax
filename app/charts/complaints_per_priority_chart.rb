class ComplaintsPerPriorityChart
  
  #
  # ticks
  #
  # [ 0, '1']
  # [ 1, '2 ]
  # etc.
  #
  # data
  #
  # [0, 124]
  # [1, 300]
  # etc.
  #
  #
  
  def ticks
    Complaint.complaint_priority_values.each_with_index.map { |prio, i| [i, prio]}
  end
  
  def priority_color(priority)
    { '1' => '#b94a48', '2' => '#f89406', '3' => '#3a87ad', '4' => '#333333', '5' => '#999999' }[priority]
  end
  
  def data
    nr_complaints = Hash.new
    
    Complaint.select("complaints.priority, count(*) nr_complaints").where("complaints.received_date > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)").group("1").order("1").each do |prio|
      nr_complaints[prio.priority] = prio.nr_complaints
    end

    datasets = []
    Complaint.complaint_priority_values.each_with_index do |priority, i|
      data = [[ i, nr_complaints[priority] ]]
      datasets.push({ data: data, color: priority_color(priority)})
    end
    datasets
  end
    
  def as_json(options = {})
    { data: data,
      options: {
        series: {
          bars: {
            show: true
          }
        },
        bars: {
          align: "center",
          barWidth: 0.5,
          lineWidth: 1
        },
        xaxis: {
          ticks: ticks
        },
        yaxis: {
          min: 0,
		      tickDecimals: 0
        },
        legend: {
		      show: false,
        },
        grid: {
          hoverable: true,
          borderWidth: 1
        }
      }
    }
  end

end
