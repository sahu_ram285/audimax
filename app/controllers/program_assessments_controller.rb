class ProgramAssessmentsController < ApplicationController
  
  after_filter :verify_authorized, :except => :index
    
  # GET /program_assessments/1/edit
  def edit
    @program_assessment = ProgramAssessment.find(params[:id])
    authorize @program_assessment
  end

  # PUT /program_assessments/1
  # PUT /program_assessments/1.json
  def update
    @program_assessment = ProgramAssessment.find(params[:id])
    authorize @program_assessment

    respond_to do |format|
      if @program_assessment.update_attributes(params[:program_assessment])
        format.html { redirect_to @program_assessment.program, notice: I18n.t('notices.object_sucessfully_updated', model: ProgramAssessment.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @program_assessment.errors, status: :unprocessable_entity }
      end
    end
  end

end
