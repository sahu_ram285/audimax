class Section < ActiveRecord::Base
  attr_accessible :regulation_id, :title, :text, :nr, :instruction, :scope_ids, :seq_nr
  
  belongs_to :regulation
  has_and_belongs_to_many :scopes
  
  validates :regulation_id, :presence => true
  
  scope :for_scope, lambda { |scope_id| joins(:scopes).where("scopes.id = ?", scope_id) }
  
end
