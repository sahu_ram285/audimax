require 'spec_helper'

describe "clients/show" do
  before(:each) do
    @client = assign(:client, stub_model(Client,
      :name => "Name",
      :address => "Address",
      :postcode => "Postcode",
      :city => "City",
      :state => "State",
      :country => "Country",
      :phone => "Phone",
      :fax => "Fax",
      :email => "Email",
      :website => "Website",
      :pos_address => "Pos Address",
      :pos_postcode => "Pos Postcode",
      :pos_city => "Pos City",
      :pos_state => "Pos State",
      :pos_country => "Pos Country",
      :inv_address => "Inv Address",
      :inv_postcode => "Inv Postcode",
      :inv_city => "Inv City",
      :inv_state => "Inv State",
      :inv_country => "Inv Country"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Address/)
    rendered.should match(/Postcode/)
    rendered.should match(/City/)
    rendered.should match(/State/)
    rendered.should match(/Country/)
    rendered.should match(/Phone/)
    rendered.should match(/Fax/)
    rendered.should match(/Email/)
    rendered.should match(/Website/)
    rendered.should match(/Pos Address/)
    rendered.should match(/Pos Postcode/)
    rendered.should match(/Pos City/)
    rendered.should match(/Pos State/)
    rendered.should match(/Pos Country/)
    rendered.should match(/Inv Address/)
    rendered.should match(/Inv Postcode/)
    rendered.should match(/Inv City/)
    rendered.should match(/Inv State/)
    rendered.should match(/Inv Country/)
  end
end
