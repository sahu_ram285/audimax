class Client::ReportsController < ApplicationController
  layout 'client'
  
  before_filter :authenticate_client_user!
  after_filter :verify_authorized
  
  def show
    @report = policy_scope(Report).find(params[:id])
    authorize @report
    
    @report_items = @report.report_items.joins(:section).order("seq_nr").includes(:section)
    @assessment_names = Hash[ProgramAssessment.joins(:assessment).includes(:assessment).where("program_id = :program_id", :program_id => @report.scope.program_id).map { |a| [a.code, ((a.name.nil? or a.name.empty?) ? a.assessment.name : a.name)] }]
    @assessment_options = @assessment_names.map { |a| [a[1], a[0]] }

    @assessment_selection = I18n.t('reports.show.all_assessments')
    if params[:assessment].present?
      @assessment_selection = @assessment_names[params[:assessment]]
      @report_items = @report_items.where("report_items.assessment = :assessment", :assessment => params[:assessment])
    end

    @qr = RQRCode::QRCode.new("#{request.protocol}#{request.host_with_port}#{report_path(@report)}", :size => 6, :level => :h )
    
    @object_path = client_object_path(@report.inspection_object)

    respond_to do |format|
      format.html { render 'reports/show'}
      format.pdf  { render :template => 'reports/show', :pdf => "report#{@report.id}", :layout => "generic", :page_size => "A4", :disposition => :inline, :footer => { :left => "Inspectierapport #{@report.id}", :right => '[page]/[topage]', :font_size => 8 }, :show_as_html => params[:debug] }
    end
  end
  
  def pundit_user
    current_client_user
  end
  
end
