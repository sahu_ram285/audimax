class AddManufacturerToEquipmentType < ActiveRecord::Migration
  def change
    add_column :equipment_types, :manufacturer, :string
  end
end
