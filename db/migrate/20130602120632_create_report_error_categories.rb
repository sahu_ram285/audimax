class CreateReportErrorCategories < ActiveRecord::Migration
  def change
    create_table :report_error_categories do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
