class EquipmentDatatable
  delegate :params, :h, :link_to, :mail_to, :raw, :equipment_usage_label, :edit_equipment_path, :image_tag, :policy, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(Equipment).count,
      iTotalDisplayRecords: equipment.total_entries,
      aaData: data
    }
  end

private

  def data
    equipment.map do |equipment|
      [
        equipment.equipment_type.name,
        (equipment.equipment_type.image_url.nil? ? "" : link_to(image_tag(equipment.equipment_type.image_url(:thumb).to_s, :class => 'img-polaroid', :size => "30x18"), equipment)),
        equipment.nr,
        equipment.serial_nr,
        equipment_usage_label(equipment.usage),
        (equipment.employee.nil? ? "" : equipment.employee.fullname),
        equipment.in_service_date,
        equipment.out_of_service_date,
				'<span class="btn-group">'+
        link_to(raw("<i class='icon-search'></i>"), equipment, { :class => 'btn btn-small' })+
        (policy(equipment).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_equipment_path(equipment), { :class => 'btn btn-small' }) : "")+
        (policy(equipment).destroy? ? link_to(raw("<i class='icon-trash'></i>"), equipment, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
        '</span>'
      ]
    end
  end

  def equipment
    @equipment ||= fetch_equipment
  end

  def fetch_equipment
    equipment = policy_scope(Equipment).order("#{sort_column} #{sort_direction}")
    equipment = equipment.joins(:equipment_type, :employee).includes(:equipment_type, :employee)

    if params[:equipment_type_id].present?
      equipment = equipment.where("equipment.equipment_type_id = :equipment_type_id", :equipment_type_id => params[:equipment_type_id])
    end
    if params[:nr].present?
      equipment = equipment.where("equipment.nr = :nr", :nr => params[:nr])
    end
    if params[:serial_nr].present?
      equipment = equipment.where("equipment.serial_nr like :serial_nr", :serial_nr => "%#{params[:serial_nr]}%")
    end
    
    if params[:employee_id].present?
      equipment = equipment.where("equipment.employee_id = :employee_id", :employee_id => params[:employee_id])
    end
    
    if params[:usage].present?
      equipment = equipment.where("equipment.usage = :usage", usage: params[:usage])
    end

    equipment = equipment.page(page).per_page(per_page)
    equipment
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[equipment_types.name equipment_types.image  equipment.nr equipment.serial_nr equipment.usage employees.lastname equipment.in_service_date equipment.out_of_service_date]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end