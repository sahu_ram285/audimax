class CreateCalibrationMethodManuals < ActiveRecord::Migration
  def change
    create_table :calibration_method_manuals do |t|
      t.integer :calibration_method_id
      t.string :manual
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
