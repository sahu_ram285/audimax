class ReportItem < ActiveRecord::Base
  attr_accessible :assessment, :comments, :report_id, :section_id
  
  belongs_to :report
  belongs_to :section
  has_many :report_errors
  has_many :attachments, :class_name => 'ReportItemAttachment'
  
  before_save :default_values

  def self.assessment_name(assessment)
    ReportItem.human_attribute_name("assessment.#{assessment.downcase}")
  end

  def default_values
    self.assessment ||= 'NONE'
  end
  
  def assessment_name
    assessment = ProgramAssessment.where("program_id = :program_id and code = :code", :program_id => self.report.scope.program_id, :code => self.assessment).first
    assessment.name
  end

end
