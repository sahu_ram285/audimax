# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

window.audimax = {} if !window.audimax?

((audimax) ->
	audimax.remove_error_fields = (link) ->
		$(link).prev('input[type=hidden]').val(true)
		$(link).closest('.am-error-fields').hide()
		
	audimax.add_error_fields = (link, content) ->
		new_id = new Date().getTime()
		regexp = new RegExp("new_report_error", "g")
		$(link).closest('td').prev('td').find('table.report_errors').append(content.replace(regexp, new_id))
		if $.fn.select2
			$('select.mws-select2').select2()	
		if $.fn.selectpicker
			$(".selectpicker").selectpicker()
		if $.fn.autosize
			$('.autosize').autosize()
		$('.selectpicker').change ->
			btn = $(this).next('div.bootstrap-select').children('button') 
			btn.removeClass 'btn-success btn-danger btn-warning btn-inverse'
			if $(this).val() == "COM"
				btn.addClass 'btn-success'
			else if $(this).val() == "NC1"
				btn.addClass 'btn-danger'
			else if $(this).val() == "NC2"
				btn.addClass 'btn-warning'
			else if $(this).val() == "NA"
				btn.addClass 'btn-inverse'
)(window.audimax)
