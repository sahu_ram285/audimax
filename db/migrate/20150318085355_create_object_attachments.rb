class CreateObjectAttachments < ActiveRecord::Migration
  def change
    create_table :object_attachments do |t|
      t.integer :inspection_object_id
      t.string :attachment
      t.text :description

      t.timestamps
    end
  end
end
