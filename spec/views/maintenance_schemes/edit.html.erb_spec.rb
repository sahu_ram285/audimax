require 'spec_helper'

describe "maintenance_schemes/edit" do
  before(:each) do
    @maintenance_scheme = assign(:maintenance_scheme, stub_model(MaintenanceScheme,
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit maintenance_scheme form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", maintenance_scheme_path(@maintenance_scheme), "post" do
      assert_select "input#maintenance_scheme_name[name=?]", "maintenance_scheme[name]"
      assert_select "textarea#maintenance_scheme_description[name=?]", "maintenance_scheme[description]"
    end
  end
end
