namespace :audimax do
	desc "Set section sequence number"
	task :set_section_seq_nr => :environment do
		Regulation.all.each do |r|
			seq_nr = 0
			Section.where("regulation_id = :regulation_id", :regulation_id => r.id).order("id").each do |s|
				seq_nr += 1
				s.seq_nr = seq_nr
				s.save!
			end	
		end
	end
end
