require 'spec_helper'

describe "complaints/index" do
  before(:each) do
    assign(:complaints, [
      stub_model(Complaint,
        :status => "Status",
        :priority => "Priority",
        :complaint_type => "Complaint Type",
        :complainant_name => "Complainant Name",
        :complainant_address => "Complainant Address",
        :complainant_postalcode => "Complainant Postalcode",
        :complainant_city => "Complainant City",
        :complainant_state => "Complainant State",
        :complainant_country => "Complainant Country",
        :complainant_phone1 => "Complainant Phone1",
        :complainant_phone2 => "Complainant Phone2",
        :complainant_email => "Complainant Email",
        :complainant_employee_id => 1,
        :complainant_client_id => 2,
        :notes => "MyText",
        :assigned_to_id => 3,
        :owner_id => 4,
        :inspection_object_id => 5,
        :client_id => 6,
        :employee_id => 7,
        :report_id => 8,
        :planned_handled_date => "",
        :complaint => "MyText",
        :cause => "MyText",
        :impact => "MyText",
        :resolution => "MyText"
      ),
      stub_model(Complaint,
        :status => "Status",
        :priority => "Priority",
        :complaint_type => "Complaint Type",
        :complainant_name => "Complainant Name",
        :complainant_address => "Complainant Address",
        :complainant_postalcode => "Complainant Postalcode",
        :complainant_city => "Complainant City",
        :complainant_state => "Complainant State",
        :complainant_country => "Complainant Country",
        :complainant_phone1 => "Complainant Phone1",
        :complainant_phone2 => "Complainant Phone2",
        :complainant_email => "Complainant Email",
        :complainant_employee_id => 1,
        :complainant_client_id => 2,
        :notes => "MyText",
        :assigned_to_id => 3,
        :owner_id => 4,
        :inspection_object_id => 5,
        :client_id => 6,
        :employee_id => 7,
        :report_id => 8,
        :planned_handled_date => "",
        :complaint => "MyText",
        :cause => "MyText",
        :impact => "MyText",
        :resolution => "MyText"
      )
    ])
  end

  it "renders a list of complaints" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Priority".to_s, :count => 2
    assert_select "tr>td", :text => "Complaint Type".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant Name".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant Address".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant Postalcode".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant City".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant State".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant Country".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant Phone1".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant Phone2".to_s, :count => 2
    assert_select "tr>td", :text => "Complainant Email".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
