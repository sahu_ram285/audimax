class AccreditationsController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /companies/:company_id/accreditations/new
  def new
    @company = Company.find(params[:company_id])
    @accreditation = @company.accreditations.build
    authorize @accreditation

    respond_to do |format|
      format.html # new.html.erb
    end
  end
  
  # GET /companies/:company_id/accreditations/:id/edit(.:format)
  def edit
    @company = Company.find(params[:company_id])
    @accreditation = @company.accreditations.find(params[:id])
    authorize @accreditation
  end
  
  # POST /companies/:company_id/accreditations
  def create
  	@company = Company.find(params[:company_id])
    @accreditation = @company.accreditations.build(params[:accreditation])
    authorize @accreditation

    respond_to do |format|
      if @accreditation.save
        format.html { redirect_to @company, notice: I18n.t('notices.object_sucessfully_created', model: Accreditation.model_name.human) }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  # PUT /companies/:company_id/accreditations/:id(.:format)
  def update
    @company = Company.find(params[:company_id])
    @accreditation = Accreditation.find(params[:id])
    authorize @accreditation

    respond_to do |format|
      if @accreditation.update_attributes(params[:accreditation])
        format.html { redirect_to company_url(@accreditation.company_id), notice: I18n.t('notices.object_sucessfully_updated', model: Accreditation.model_name.human) }
      else
        format.html { render action: "edit" }
      end
    end
  end
  
  # DELETE /companies/:company_id/accreditations/:id
  def destroy
    @accreditation = Accreditation.find(params[:id])
    authorize @accreditation
    @accreditation.destroy

    respond_to do |format|
      format.html { redirect_to company_url(@accreditation.company_id) }
    end
  end
  
  def set_current_section
    self.current_section = :company
  end
  
end
