class RemoveCalibrationIntervalFromEquipmentType < ActiveRecord::Migration
  def up
    remove_column :equipment_types, :calibration_interval
  end

  def down
    add_column :equipment_types, :calibration_interval, :string
  end
end
