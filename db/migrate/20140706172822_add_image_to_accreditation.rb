class AddImageToAccreditation < ActiveRecord::Migration
  def change
    add_column :accreditations, :image, :string
  end
end
