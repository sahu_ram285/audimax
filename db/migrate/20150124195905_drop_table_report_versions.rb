class DropTableReportVersions < ActiveRecord::Migration
  def up
    drop_table :report_versions
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
