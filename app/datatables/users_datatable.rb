class UsersDatatable
  delegate :params, :h, :link_to, :raw, :image_tag, :mail_to, :edit_user_path, :current_user, :policy, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(User).count,
      iTotalDisplayRecords: users.total_entries,
      aaData: data
    }
  end

private

  def data
    users.map do |user|
      [
				(user.image_url.nil? ? "" : link_to(image_tag(user.image_url(:thumb).to_s, :class => 'img-polaroid am-user-photo'), edit_user_path(user))),
        mail_to(user.email)+raw(user.admin? ? " <span class='label label-important'>admin</span>" : "")+raw((user == current_user ? " <span class='label label-warning'>you</span>" : "")),
        user.employee.nil? ? "" : user.employee.fullname,
        user.role_names.join(","),
        user.two_step_verification,
				(user.current_sign_in_at.nil? ? "" : user.current_sign_in_at.strftime("%Y-%m-%d %H:%M:%S")),
				'<span class="btn-group">'+
          (policy(user).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_user_path(user), { :class => 'btn btn-small' }) : "")+
          ((policy(user).destroy? and !user.admin?) ? link_to(raw("<i class='icon-trash'></i>"), user, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
          '</span>'
      ]
    end
  end

  def users
    @users ||= fetch_users
  end

  def fetch_users
    users = policy_scope(User).order("#{sort_column} #{sort_direction}")
    users = users.joins('LEFT OUTER JOIN employees ON employees.id = users.employee_id').includes(:employee)
    
    users = users.page(page).per_page(per_page)
    users
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[image users.email employees.lastname users.two_step_verification users.current_signed_in_at]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end