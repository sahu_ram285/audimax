class ReportErrorsPerCategoryChart
  
  def initialize
    @series_data = []
    @categories = []
    
    ReportError.last_12_months.joins(:report_error_category).select("report_error_categories.id, report_error_categories.name, count(*) as nr_errors").group("1,2").order("3 desc").each do |cat|
      @series_data.push({ label: cat.name, data: cat.nr_errors})
      @categories.push(cat.id)
    end
  end

  def categories
    @categories
  end
  
  def series_data
    @series_data
  end
  
end
