class AddInfoToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :phone, :string
    add_column :companies, :fax, :string
    add_column :companies, :email, :string
    add_column :companies, :web, :string
  end
end
