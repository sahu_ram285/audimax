require 'spec_helper'

describe "equipment/new" do
  before(:each) do
    assign(:equipment, stub_model(Equipment,
      :equipment_type => "MyString",
      :serial_nr => "MyString"
    ).as_new_record)
  end

  it "renders new equipment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", equipment_index_path, "post" do
      assert_select "input#equipment_equipment_type[name=?]", "equipment[equipment_type]"
      assert_select "input#equipment_serial_nr[name=?]", "equipment[serial_nr]"
    end
  end
end
