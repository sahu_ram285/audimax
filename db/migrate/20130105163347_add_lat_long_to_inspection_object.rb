class AddLatLongToInspectionObject < ActiveRecord::Migration
  def change
	add_column :inspection_objects, :latitude, :decimal
	add_column :inspection_objects, :longitude, :decimal
	add_column :inspection_objects, :country, :string
  end
end
