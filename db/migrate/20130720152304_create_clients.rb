class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :address
      t.string :postcode
      t.string :city
      t.string :state
      t.string :country
      t.string :phone
      t.string :fax
      t.string :email
      t.string :website
      t.string :pos_address
      t.string :pos_postcode
      t.string :pos_city
      t.string :pos_state
      t.string :pos_country
      t.string :inv_address
      t.string :inv_postcode
      t.string :inv_city
      t.string :inv_state
      t.string :inv_country

      t.timestamps
    end
  end
end
