/*
 * MWS Admin v2.1 - FileInput Plugin JS
 * This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
 * All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
 * Last Updated:
 * December 08, 2012
 *
 * 'Highly configurable' mutable plugin boilerplate
 * Author: @markdalgleish
 * Further changes, comments: @addyosmani
 * Licensed under the MIT license
 *
 */
!function(t,e,i,n){function o(t,e){arguments.length&&this._init(t,e)}o.prototype={defaults:{placeholder:"No file selected...",buttontext:"Browse..."},_init:function(e,i){this.element=t(e),this.options=t.extend({},this.defaults,i,this.element.data()),this._build()},_build:function(){this.element.css({position:"absolute",top:0,right:0,margin:0,cursor:"pointer",fontSize:"999px",opacity:0,zIndex:999,filter:"alpha(opacity=0)"}).on("change.fileupload",t.proxy(this._change,this)),this.container=t('<div class="fileinput-holder" style="position: relative;"></div>').append(t('<input type="text" class="fileinput-preview" style="width: 100%;" readonly="readonly" />').attr("placeholder",this.options.placeholder)).append(t('<span class="fileinput-btn btn" type="button" style="display:block; overflow: hidden; position: absolute; top: 0; right: 0; cursor: pointer;"></span>').text(this.options.buttontext)).insertAfter(this.element);var e=this.container.find(".fileinput-btn");this.element.appendTo(e),this.container.find(".fileinput-preview").css("paddingRight",e.outerWidth()+4+"px")},_change:function(t){var e=t.target.files!==n?t.target.files[0]:{name:t.target.value.replace(/^.+\\/,"")};e&&this.container.find(".fileinput-preview ").val(e.name)}},t.fn.fileInput=function(t){return this.each(function(){new o(this,t)})}}(jQuery,window,document);