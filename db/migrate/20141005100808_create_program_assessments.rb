class CreateProgramAssessments < ActiveRecord::Migration
  def change
    create_table :program_assessments do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
