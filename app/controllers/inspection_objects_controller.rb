class InspectionObjectsController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /inspection_objects
  # GET /inspection_objects.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json {
        if params[:style] == 'select'
          render json: InspectionObjectsSelect.new(view_context)
        else
          render json: InspectionObjectsDatatable.new(view_context)
        end
      }
    end
  end

  # GET /inspection_objects/1
  # GET /inspection_objects/1.json
  def show
    @inspection_object = InspectionObject.find(params[:id])
    authorize @inspection_object

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @inspection_object }
    end
  end

  # GET /inspection_objects/new
  # GET /inspection_objects/new.json
  def new
    @inspection_object = InspectionObject.new(:client_id => params[:client_id])
    authorize @inspection_object

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @inspection_object }
    end
  end

  # GET /inspection_objects/1/edit
  def edit
    @inspection_object = InspectionObject.find(params[:id])
    authorize @inspection_object
  end

  # POST /inspection_objects
  # POST /inspection_objects.json
  def create
    @inspection_object = InspectionObject.new(params[:inspection_object])
    authorize @inspection_object

    respond_to do |format|
      if @inspection_object.save
        format.html { redirect_to inspection_objects_path, notice: I18n.t('notices.object_sucessfully_created', model: InspectionObject.model_name.human) }
        format.json { render json: @inspection_object, status: :created, location: @inspection_object }
      else
        format.html { render action: "new" }
        format.json { render json: @inspection_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /inspection_objects/1
  # PUT /inspection_objects/1.json
  def update
    @inspection_object = InspectionObject.find(params[:id])
    authorize @inspection_object

    respond_to do |format|
      if @inspection_object.update_attributes(params[:inspection_object])
        format.html { redirect_to inspection_objects_path, notice: I18n.t('notices.object_sucessfully_updated', model: InspectionObject.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @inspection_object.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inspection_objects/1
  # DELETE /inspection_objects/1.json
  def destroy
    @inspection_object = InspectionObject.find(params[:id])
    authorize @inspection_object
    @inspection_object.destroy

    respond_to do |format|
      format.html { redirect_to inspection_objects_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end  
end
