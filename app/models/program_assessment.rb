class ProgramAssessment < ActiveRecord::Base
  attr_accessible :code, :name
  
  belongs_to :program
  belongs_to :assessment, :foreign_key => :code, :primary_key => :code
end
