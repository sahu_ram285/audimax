class AddCalibrationMethodIdToMaintenance < ActiveRecord::Migration
  def change
    add_column :maintenances, :calibration_method_id, :integer
  end
end
