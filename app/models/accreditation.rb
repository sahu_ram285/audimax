class Accreditation < ActiveRecord::Base
  attr_accessible :company_id, :end_date, :image, :remove_image, :scope_id, :start_date
  
  belongs_to :company
  belongs_to :scope
  
  validate :validate_dates
  validates :company_id, :scope_id, :presence => true
  
  mount_uploader :image, ImageUploader
  
  def validate_dates
    if !self.start_date.nil? && !self.end_date.nil? && self.start_date > self.end_date
      errors.add(:start_date, :later_than, max: Accreditation.human_attribute_name('end_date'))
    end
  end
  
end
