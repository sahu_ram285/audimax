require 'spec_helper'

describe "maintenances/show" do
  before(:each) do
    @maintenance = assign(:maintenance, stub_model(Maintenance,
      :equipment_id => 1,
      :maintenance_type => "Maintenance Type",
      :employee_id => 2,
      :calibration_result => 1.5,
      :calibration_approved => "Calibration Approved"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Maintenance Type/)
    rendered.should match(/2/)
    rendered.should match(/1.5/)
    rendered.should match(/Calibration Approved/)
  end
end
