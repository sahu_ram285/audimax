class AddUserIdToComplaintAction < ActiveRecord::Migration
  def change
    add_column :complaint_actions, :user_id, :integer
  end
end
