class Client::DashboardController < ApplicationController
  layout 'client'
  
  before_filter :authenticate_client_user!
  after_filter :verify_authorized, :except => :index
  
  def index
    @reports = policy_scope(Report).order("inspection_date desc")
  end
  
  def pundit_user
    current_client_user
  end
  
end
