require "spec_helper"

describe CalibrationMethodsController do
  describe "routing" do

    it "routes to #index" do
      get("/calibration_methods").should route_to("calibration_methods#index")
    end

    it "routes to #new" do
      get("/calibration_methods/new").should route_to("calibration_methods#new")
    end

    it "routes to #show" do
      get("/calibration_methods/1").should route_to("calibration_methods#show", :id => "1")
    end

    it "routes to #edit" do
      get("/calibration_methods/1/edit").should route_to("calibration_methods#edit", :id => "1")
    end

    it "routes to #create" do
      post("/calibration_methods").should route_to("calibration_methods#create")
    end

    it "routes to #update" do
      put("/calibration_methods/1").should route_to("calibration_methods#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/calibration_methods/1").should route_to("calibration_methods#destroy", :id => "1")
    end

  end
end
