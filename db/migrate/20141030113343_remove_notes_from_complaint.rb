class RemoveNotesFromComplaint < ActiveRecord::Migration
  def up
    remove_column :complaints, :notes
  end

  def down
    add_column :complaints, :notes, :text
  end
end
