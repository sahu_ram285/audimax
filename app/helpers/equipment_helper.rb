module EquipmentHelper
  
  def equipment_usage_label(usage)
    raw("<span class='label label-success'>"+Equipment.human_attribute_name("usage.#{usage.downcase}")+"</span>")
  end
  
end
