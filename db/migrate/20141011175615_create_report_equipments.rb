class CreateReportEquipments < ActiveRecord::Migration
  def change
    create_table :report_equipments do |t|
      t.integer :report_id
      t.integer :equipment_id

      t.timestamps
    end
  end
end
