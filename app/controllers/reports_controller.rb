class ReportsController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index

  # GET /reports
  # GET /reports.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: ReportsDatatable.new(view_context) }
    end
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
    @report = policy_scope(Report).find(params[:id])
    authorize @report
    
    @report_items = @report.report_items.joins(:section).order("seq_nr").includes(:section)
    @assessment_names = Hash[ProgramAssessment.joins(:assessment).includes(:assessment).where("program_id = :program_id", :program_id => @report.scope.program_id).map { |a| [a.code, ((a.name.nil? or a.name.empty?) ? a.assessment.name : a.name)] }]
    @assessment_options = @assessment_names.map { |a| [a[1], a[0]] }

    @assessment_selection = I18n.t('reports.show.all_assessments')
    if params[:assessment].present?
      @assessment_selection = @assessment_names[params[:assessment]]
      @report_items = @report_items.where("report_items.assessment = :assessment", :assessment => params[:assessment])
    end
    
    @qr = RQRCode::QRCode.new("#{request.protocol}#{request.host_with_port}#{report_path(@report)}", :size => 6, :level => :h )
    
    @object_path = inspection_object_path(@report.inspection_object)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @report }
      format.pdf  { render :pdf => "report#{@report.id}", :layout => "generic", :page_size => "A4", :disposition => :inline, :footer => { :left => "#{Report.model_name.human} #{@report.id}", :right => '[page]/[topage]', :font_size => 8 }, :show_as_html => params[:debug] }
    end
  end

  # GET /reports/new
  def new
    @report = Report.new(:employee_id => current_user.employee_id, :inspection_date => Date.today)
    authorize @report
  end

  # GET /reports/1/edit
  def edit
    @report = policy_scope(Report).find(params[:id])
    authorize @report

    @assessment_names = Hash[ProgramAssessment.joins(:assessment).includes(:assessment).where("program_id = :program_id", :program_id => @report.scope.program_id).map { |a| [a.code, ((a.name.nil? or a.name.empty?) ? a.assessment.name : a.name)] }]
    @assessment_options = @assessment_names.map { |a| [a[1], a[0]] }
  end

  # POST /reports
  def create
    @report = Report.create_report(params[:report][:inspection_object_nr], params[:report][:employee_id], params[:report][:inspection_date], params[:report][:scope_id])
    authorize @report

    respond_to do |format|
      if @report.save
        format.html { redirect_to edit_report_path(@report) }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /reports/1
  # PUT /reports/1.json
  def update
    @report = policy_scope(Report).find(params[:id])
    authorize @report

    respond_to do |format|
      if @report.update_attributes(params[:report])
        format.html { redirect_to @report, notice: I18n.t('notices.object_sucessfully_updated', model: Report.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
    @report = policy_scope(Report).find(params[:id])
    authorize @report
    @report.destroy

    respond_to do |format|
      format.html { redirect_to reports_url }
      format.json { head :no_content }
    end
  end
  
  # POST /reports/1/add_equipment?equipment_id=2
  def add_equipment
    @report = policy_scope(Report).find(params[:id])
    authorize @report, :update?
    @report.report_equipments.build(:equipment_id => params[:equipment_id])
    respond_to do |format|
      if @report.save
        format.html { redirect_to edit_report_path(@report) }
      else
        format.html { render action: "edit" }
      end
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end  
  
end
