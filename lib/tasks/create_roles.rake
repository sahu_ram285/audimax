namespace :audimax do

  def create_or_replace_role(name, description)
    role = Role.find_or_create_by_name(name)
    role.description = description
    role.save!
  end

  desc "Create roles"
  task :create_roles => :environment do
    roles = [
      { name: 'Administrator', description: 'Application administator role. No restrictions.'},
      { name: 'Inspector', description: 'Inspector role. Create reports. Read-only access to anything else.'},
      { name: 'Manager', description: 'Technical Manager role. Access to everything except application maintenance data.'},
      { name: 'Reviewer', description: 'Reviewer role. Access to everything that is required to do report reviews.'},
      { name: 'Administrative Assistant', description: 'Administrative assistant role. Access to master data.'},
      { name: 'Complaint Handler', description: 'Complaint handler role. Access to everything that is required to enter and handle complaints.'}
    ]
    
    roles.each do |role|
      puts "Creating role #{role[:name]}"
      create_or_replace_role(role[:name], role[:description])
    end
  end
end
