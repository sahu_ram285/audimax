class AddSeqNrToSections < ActiveRecord::Migration
  def change
    add_column :sections, :seq_nr, :integer
  end
end
