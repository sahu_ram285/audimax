require 'test_helper'

class InspectionObjectsControllerTest < ActionController::TestCase
  setup do
    @inspection_object = inspection_objects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inspection_objects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inspection_object" do
    assert_difference('InspectionObject.count') do
      post :create, inspection_object: { address: @inspection_object.address, city: @inspection_object.city, description: @inspection_object.description, email: @inspection_object.email, name: @inspection_object.name, nr: @inspection_object.nr, postalcode: @inspection_object.postalcode, state: @inspection_object.state, website: @inspection_object.website }
    end

    assert_redirected_to inspection_object_path(assigns(:inspection_object))
  end

  test "should show inspection_object" do
    get :show, id: @inspection_object
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inspection_object
    assert_response :success
  end

  test "should update inspection_object" do
    put :update, id: @inspection_object, inspection_object: { address: @inspection_object.address, city: @inspection_object.city, description: @inspection_object.description, email: @inspection_object.email, name: @inspection_object.name, nr: @inspection_object.nr, postalcode: @inspection_object.postalcode, state: @inspection_object.state, website: @inspection_object.website }
    assert_redirected_to inspection_object_path(assigns(:inspection_object))
  end

  test "should destroy inspection_object" do
    assert_difference('InspectionObject.count', -1) do
      delete :destroy, id: @inspection_object
    end

    assert_redirected_to inspection_objects_path
  end
end
