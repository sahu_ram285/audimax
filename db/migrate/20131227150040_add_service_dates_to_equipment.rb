class AddServiceDatesToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :in_service_date, :date
    add_column :equipment, :out_of_service_date, :date
  end
end
