require 'spec_helper'

describe "clients/index" do
  before(:each) do
    assign(:clients, [
      stub_model(Client,
        :name => "Name",
        :address => "Address",
        :postcode => "Postcode",
        :city => "City",
        :state => "State",
        :country => "Country",
        :phone => "Phone",
        :fax => "Fax",
        :email => "Email",
        :website => "Website",
        :pos_address => "Pos Address",
        :pos_postcode => "Pos Postcode",
        :pos_city => "Pos City",
        :pos_state => "Pos State",
        :pos_country => "Pos Country",
        :inv_address => "Inv Address",
        :inv_postcode => "Inv Postcode",
        :inv_city => "Inv City",
        :inv_state => "Inv State",
        :inv_country => "Inv Country"
      ),
      stub_model(Client,
        :name => "Name",
        :address => "Address",
        :postcode => "Postcode",
        :city => "City",
        :state => "State",
        :country => "Country",
        :phone => "Phone",
        :fax => "Fax",
        :email => "Email",
        :website => "Website",
        :pos_address => "Pos Address",
        :pos_postcode => "Pos Postcode",
        :pos_city => "Pos City",
        :pos_state => "Pos State",
        :pos_country => "Pos Country",
        :inv_address => "Inv Address",
        :inv_postcode => "Inv Postcode",
        :inv_city => "Inv City",
        :inv_state => "Inv State",
        :inv_country => "Inv Country"
      )
    ])
  end

  it "renders a list of clients" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Postcode".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Fax".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "Pos Address".to_s, :count => 2
    assert_select "tr>td", :text => "Pos Postcode".to_s, :count => 2
    assert_select "tr>td", :text => "Pos City".to_s, :count => 2
    assert_select "tr>td", :text => "Pos State".to_s, :count => 2
    assert_select "tr>td", :text => "Pos Country".to_s, :count => 2
    assert_select "tr>td", :text => "Inv Address".to_s, :count => 2
    assert_select "tr>td", :text => "Inv Postcode".to_s, :count => 2
    assert_select "tr>td", :text => "Inv City".to_s, :count => 2
    assert_select "tr>td", :text => "Inv State".to_s, :count => 2
    assert_select "tr>td", :text => "Inv Country".to_s, :count => 2
  end
end
