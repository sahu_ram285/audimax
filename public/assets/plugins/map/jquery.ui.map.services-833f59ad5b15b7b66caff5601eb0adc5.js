/*!
 * jQuery UI Google Map 3.0-rc
 * http://code.google.com/p/jquery-ui-map/
 * Copyright (c) 2010 - 2012 Johan Säll Larsson
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *		jquery.ui.map.js
 */
!function(e){e.extend(e.ui.gmap.prototype,{displayDirections:function(e,t,i){var s=this,o=this.get("services > DirectionsService",new google.maps.DirectionsService),r=this.get("services > DirectionsRenderer",new google.maps.DirectionsRenderer);t&&r.setOptions(t),o.route(e,function(e,t){"OK"===t?(r.setDirections(e),r.setMap(s.get("map"))):r.setMap(null),i(e,t)})},displayStreetView:function(e,t){this.get("map").setStreetView(this.get("services > StreetViewPanorama",new google.maps.StreetViewPanorama(this._unwrap(e),t)))},search:function(e,t){this.get("services > Geocoder",new google.maps.Geocoder).geocode(e,t)}})}(jQuery);