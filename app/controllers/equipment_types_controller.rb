class EquipmentTypesController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /equipment_types
  # GET /equipment_types.json
  def index
    @equipment_types = policy_scope(EquipmentType).order("name")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @equipment_types }
    end
  end

  # GET /equipment_types/1
  # GET /equipment_types/1.json
  def show
    @equipment_type = EquipmentType.find(params[:id])
    authorize @equipment_type

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @equipment_type }
    end
  end

  # GET /equipment_types/new
  # GET /equipment_types/new.json
  def new
    @equipment_type = EquipmentType.new
    authorize @equipment_type

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @equipment_type }
    end
  end

  # GET /equipment_types/1/edit
  def edit
    @equipment_type = EquipmentType.find(params[:id])
    authorize @equipment_type
  end

  # POST /equipment_types
  # POST /equipment_types.json
  def create
    @equipment_type = EquipmentType.new(params[:equipment_type])
    authorize @equipment_type
    maintenance_schemes = MaintenanceScheme.find(params[:maintenance_scheme_ids]) rescue []
    @equipment_type.maintenance_schemes = maintenance_schemes

    respond_to do |format|
      if @equipment_type.save
        format.html { redirect_to equipment_types_path, notice: I18n.t('notices.object_sucessfully_created', model: EquipmentType.model_name.human) }
        format.json { render json: @equipment_type, status: :created, location: @equipment_type }
      else
        format.html { render action: "new" }
        format.json { render json: @equipment_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /equipment_types/1
  # PUT /equipment_types/1.json
  def update
    @equipment_type = EquipmentType.find(params[:id])
    authorize @equipment_type
    maintenance_schemes = MaintenanceScheme.find(params[:maintenance_scheme_ids]) rescue []
    @equipment_type.maintenance_schemes = maintenance_schemes
    
    respond_to do |format|
      if @equipment_type.update_attributes(params[:equipment_type])
        format.html { redirect_to equipment_types_path, notice: I18n.t('notices.object_sucessfully_updated', model: EquipmentType.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @equipment_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment_types/1
  # DELETE /equipment_types/1.json
  def destroy
    @equipment_type = EquipmentType.find(params[:id])
    authorize @equipment_type
    @equipment_type.destroy

    respond_to do |format|
      format.html { redirect_to equipment_types_url }
      format.json { head :no_content }
    end
  end
  
  def set_current_section
    self.current_section = :assets
  end  
  
end
