class DropTableReportReviewItems < ActiveRecord::Migration
  def up
    drop_table :report_review_items
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
