class AddStatusToRegulation < ActiveRecord::Migration
  def change
    add_column :regulations, :status, :string
  end
end
