class Client::InspectionObjectsController < ApplicationController
  layout 'client'
  
  before_filter :authenticate_client_user!
  after_filter :verify_authorized, :except => :index
  
  def index
    @inspection_objects = policy_scope(InspectionObject).order("nr")
  end
  
  def show
    @inspection_object = policy_scope(InspectionObject).find(params["id"])
    authorize @inspection_object
  end
  
  def pundit_user
    current_client_user
  end
  
end
