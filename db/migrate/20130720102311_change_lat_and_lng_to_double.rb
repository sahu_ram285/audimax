class ChangeLatAndLngToDouble < ActiveRecord::Migration
  def up
    change_column :inspection_objects, :latitude, :double
    change_column :inspection_objects, :longitude, :double
  end

  def down
    change_column :inspection_objects, :latitude, :decimal
    change_column :inspection_objects, :longitude, :decimal
  end
end
