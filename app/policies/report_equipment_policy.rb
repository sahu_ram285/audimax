class ReportEquipmentPolicy < ApplicationPolicy
  def create?
    @user.admin? or @user.has_role(:administrator, :manager, :inspector)
  end
  
  def update?
    @user.admin? or @user.has_role(:administrator, :manager, :inspector)
  end
  
  def destroy?
    @user.admin? or @user.has_role(:administrator, :manager, :inspector)
  end 
end