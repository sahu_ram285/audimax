class AddWarnBeforeToMaintenanceScheme < ActiveRecord::Migration
  def change
    add_column :maintenance_schemes, :warn_before, :integer
  end
end
