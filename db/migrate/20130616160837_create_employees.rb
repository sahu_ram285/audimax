class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :firstname
      t.string :lastname
      t.date :birthdate
      t.string :address
      t.string :postcode
      t.string :city
      t.string :state
      t.string :country
      t.string :email
      t.string :phone1
      t.string :phone2
      t.integer :company_id
      t.date :employment_date
      t.integer :position_id

      t.timestamps
    end
  end
end
