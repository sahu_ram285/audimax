class ReportEquipment < ActiveRecord::Base
  attr_accessible :equipment_id, :report_id
  
  belongs_to :report
  belongs_to :equipment
end
