class Participation < ActiveRecord::Base
  attr_accessible :end_date, :inspection_object_id, :scope_id, :start_date
  
  belongs_to :inspection_object
  belongs_to :scope
  
	validates :inspection_object_id, :scope_id, :start_date, :presence => true
end
