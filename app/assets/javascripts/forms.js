;(function( $, window, document, undefined ) {

	$(document).ready(function() {
		
		// Move your code to forms.js.coffee
		
		function itemFormatResult(item) {
			var markup = '<div class="row-fluid">';
			for ( var key in item ) {
				if (item.hasOwnProperty(key)) {
					span = (key == 'id') ? 'span2' : 'span4';
					markup += '<div class="'+span+'">'+item[key]+'</div>';
				}
			}
			markup += '</div>';
			return markup;
		}

	    function itemFormatSelection(item) {
	       return item.id;
	    }
		
		if ( $.fn.select2 ) {
			$(".mws-select-ajax").each(function(){
				$(this).select2({
					placeholder: $(this).data('placeholder'),
					minimumInputLength: 1,
					allowClear: true,
					ajax: {
						url: $(this).data('source'),
						dataType: 'json',
						quietMillies: 250,
						data: function(term, page) {
							return {
								style: 'select',
								query: term,
								page: page
							}
						},
						results: function(data, page) {
							var more = (page*10) < data.total_items;
							return { results: data.items, more: more }
						},
						cache: true
					},
					initSelection: function(element, callback) {
						var id = $(element).val();
						callback({id: id});
					},
					formatResult: itemFormatResult,
					formatSelection: itemFormatSelection,
					formatNoMatches: function () { return I18n.t('js.select2.no_matches'); },
					formatLoadMore: function (pageNumber) { return I18n.t('js.select2.load_more'); },
					formatInputTooShort: function (input, min) { 
						nrCharacters = (min-input.length);
						if (nrCharacters == 1) {
							return I18n.t('js.select2.input_too_short_1');
						} else {
							return I18n.t('js.select2.input_too_short', {count: nrCharacters});
						}
					},
	        		formatSearching: function () { return I18n.t('js.select2.searching'); },
				});
			});
		}
		
	});
		
}) (jQuery, window, document);
