class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :employee_id, :password, :password_confirmation, :remember_me, :role_ids, :image, :remove_image, :two_step_verification
  # attr_accessible :title, :body
  
  belongs_to :company
  belongs_to :employee
  has_many :notifications
  has_and_belongs_to_many :roles
  
  before_destroy :prevent_destroy_admin
  
  mount_uploader :image, ImageUploader
  
  def role_names
    self.roles.map {|r| r.name}
  end
  
  def has_role(*roles_array)
    role_syms = self.roles.to_set { |r| r.name.parameterize.underscore.to_sym }
    roles_array.any? { |role| role_syms.include? role }
  end
  
  def display_name
    self.employee.nil? ? self.email : self.employee.fullname
  end
  
  def prevent_destroy_admin
    if self.admin?
      errors.add(:base, "Admin user cannot be destroyed")
      return false
    end
    true
  end
  
end
