require 'spec_helper'

describe "complaints/show" do
  before(:each) do
    @complaint = assign(:complaint, stub_model(Complaint,
      :status => "Status",
      :priority => "Priority",
      :complaint_type => "Complaint Type",
      :complainant_name => "Complainant Name",
      :complainant_address => "Complainant Address",
      :complainant_postalcode => "Complainant Postalcode",
      :complainant_city => "Complainant City",
      :complainant_state => "Complainant State",
      :complainant_country => "Complainant Country",
      :complainant_phone1 => "Complainant Phone1",
      :complainant_phone2 => "Complainant Phone2",
      :complainant_email => "Complainant Email",
      :complainant_employee_id => 1,
      :complainant_client_id => 2,
      :notes => "MyText",
      :assigned_to_id => 3,
      :owner_id => 4,
      :inspection_object_id => 5,
      :client_id => 6,
      :employee_id => 7,
      :report_id => 8,
      :planned_handled_date => "",
      :complaint => "MyText",
      :cause => "MyText",
      :impact => "MyText",
      :resolution => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Status/)
    rendered.should match(/Priority/)
    rendered.should match(/Complaint Type/)
    rendered.should match(/Complainant Name/)
    rendered.should match(/Complainant Address/)
    rendered.should match(/Complainant Postalcode/)
    rendered.should match(/Complainant City/)
    rendered.should match(/Complainant State/)
    rendered.should match(/Complainant Country/)
    rendered.should match(/Complainant Phone1/)
    rendered.should match(/Complainant Phone2/)
    rendered.should match(/Complainant Email/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/MyText/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/5/)
    rendered.should match(/6/)
    rendered.should match(/7/)
    rendered.should match(/8/)
    rendered.should match(//)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
  end
end
