class ClientsDatatable
  delegate :params, :h, :link_to, :number_to_currency, :raw, :edit_client_path, :image_tag, :policy, :policy_scope, :country_name, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(Client).count,
      iTotalDisplayRecords: clients.total_entries,
      aaData: data
    }
  end

private

  def data
    clients.map do |client|
      [
        (client.image_url.nil? ? "" : link_to(image_tag(client.image_url(:thumb).to_s, :class => 'img-polaroid', :size => "48x30"), client)),
        client.name,
        h(client.address),
        h(client.postcode),
        h(client.city),
        h(client.state),
        h(country_name(client.country)),
				'<span class="btn-group">'+
          link_to(raw("<i class='icon-search'></i>"), client, { :class => 'btn btn-small' })+
          (policy(client).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_client_path(client), { :class => 'btn btn-small' }) : "")+
          (policy(client).destroy? ? link_to(raw("<i class='icon-trash'></i>"), client, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
          '</span>'
      ]
    end
  end

  def clients
    @clients ||= fetch_clients
  end

  def fetch_clients
    clients = policy_scope(Client).order("#{sort_column} #{sort_direction}")
    clients = clients.page(page).per_page(per_page)
    if params[:sSearch].present?
      clients = clients.where("name like :search", search: "%#{params[:sSearch]}%")
    end
    if params[:name].present?
      clients = clients.where("name like :search", search: "%#{params[:name]}%")
    end
    clients
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[image name address postcode city state country]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end