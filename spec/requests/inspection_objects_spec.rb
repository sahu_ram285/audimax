require 'spec_helper'

describe "InspectionObjects" do

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "GET /inspection_objects" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get inspection_objects_path
      response.status.should be(200)
    end
  end
end
