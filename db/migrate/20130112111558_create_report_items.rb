class CreateReportItems < ActiveRecord::Migration
  def change
    create_table :report_items do |t|
      t.integer :report_id
      t.integer :section_id
      t.string :assessment
      t.text :comments

      t.timestamps
    end
  end
end
