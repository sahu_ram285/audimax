class AddEmployeeIdToReport < ActiveRecord::Migration
  def change
    add_column :reports, :employee_id, :integer
  end
end
