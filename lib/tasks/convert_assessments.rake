namespace :audimax do

  def convert_assessment(assessment)
    {"OK" => "COM", "NOK" => "NC1", "NA" => "NA", "NONE" => "NONE"}[assessment]
  end

  desc "Convert assessments"
  task :convert_assessments => :environment do
    ReportItem.all.each do |item|
      item.assessment = convert_assessment(item.assessment)
      item.save!
    end
	end
end
