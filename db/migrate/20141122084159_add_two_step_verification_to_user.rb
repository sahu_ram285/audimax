class AddTwoStepVerificationToUser < ActiveRecord::Migration
  def change
    add_column :users, :two_step_verification, :string
  end
end
