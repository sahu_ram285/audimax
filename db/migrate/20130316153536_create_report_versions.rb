class CreateReportVersions < ActiveRecord::Migration
  def change
    create_table :report_versions do |t|
      t.integer :report_id
      t.integer :inspection_object_id
      t.integer :inspector_id
      t.date :inspection_date
      t.integer :scope_id
      t.integer :regulation_id
      t.text :comments
      t.string :status
      t.integer :user_id
      t.string :action

      t.timestamps
    end
  end
end
