class ClientUsersController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  def index
    respond_to do |format|
      format.html
      format.json { render json: ClientUsersDatatable.new(view_context) }
    end
  end
  
  def new
    @client_user = ClientUser.new
    authorize @client_user
  end

  def edit
    @client_user = ClientUser.find(params[:id])
    authorize @client_user
  end

  def create
    @client_user = ClientUser.new(params[:client_user])
    authorize @client_user

    if @client_user.save
      redirect_to client_users_url, notice: I18n.t('notices.object_sucessfully_created', model: ClientUser.model_name.human)
    else
      render action: "new"
    end
  end
  
  def update
    @client_user = ClientUser.find(params[:id])
    authorize @client_user

    if params[:client_user][:password].blank?
      params[:client_user].delete(:password)
      params[:client_user].delete(:password_confirmation)
    end

    if @client_user.update_attributes(params[:client_user])
      redirect_to client_users_url, notice: I18n.t('notices.object_sucessfully_updated', model: ClientUser.model_name.human)
    else
      render action: "edit"
    end
  end 
  
  def destroy
  	@client_user = ClientUser.find(params[:id])
    authorize @client_user
    @client_user.destroy

    redirect_to client_users_url
  end 
  
  private
  
  def set_current_section
    self.current_section = :system
  end  
  
end
