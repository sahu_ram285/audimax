class CreateEquipmentTypes < ActiveRecord::Migration
  def change
    create_table :equipment_types do |t|
      t.string :name
      t.text :description
      t.integer :calibration_interval
      t.text :instruction

      t.timestamps
    end
  end
end
