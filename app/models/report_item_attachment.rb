class ReportItemAttachment < ActiveRecord::Base
  attr_accessible :attachment, :description, :report_item_id
  mount_uploader :attachment, AttachmentUploader
end
