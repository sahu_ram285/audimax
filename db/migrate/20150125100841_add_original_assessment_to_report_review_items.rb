class AddOriginalAssessmentToReportReviewItems < ActiveRecord::Migration
  def change
    add_column :report_review_items, :original_assessment, :string
  end
end
