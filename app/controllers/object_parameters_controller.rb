class ObjectParametersController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /object_parameters
  # GET /object_parameters.json
  def index
    @object_parameters = policy_scope(ObjectParameter).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @object_parameters }
    end
  end

  # GET /object_parameters/1
  # GET /object_parameters/1.json
  def show
    @object_parameter = ObjectParameter.find(params[:id])
    authorize @object_parameter

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @object_parameter }
    end
  end

  # GET /object_parameters/new
  # GET /object_parameters/new.json
  def new
    @inspection_object = InspectionObject.find(params[:inspection_object_id])
    @object_parameter = @inspection_object.object_parameters.build
    authorize @object_parameter

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @object_parameter }
    end
  end

  # GET /object_parameters/1/edit
  def edit
    @object_parameter = ObjectParameter.find(params[:id])
    authorize @object_parameter
  end

  # POST /object_parameters
  # POST /object_parameters.json
  def create
    @inspection_object = InspectionObject.find(params[:inspection_object_id])
    @object_parameter = @inspection_object.object_parameters.build(params[:object_parameter])
    authorize @object_parameter

    respond_to do |format|
      if @object_parameter.save
        format.html { redirect_to @inspection_object, notice: I18n.t('notices.object_sucessfully_created', model: ObjectParameter.model_name.human) }
        format.json { render json: @object_parameter, status: :created, location: @object_parameter }
      else
        format.html { render action: "new" }
        format.json { render json: @object_parameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /object_parameters/1
  # PUT /object_parameters/1.json
  def update
    @object_parameter = ObjectParameter.find(params[:id])
    authorize @object_parameter

    respond_to do |format|
      if @object_parameter.update_attributes(params[:object_parameter])
        format.html { redirect_to inspection_object_url(@object_parameter.inspection_object_id), notice: I18n.t('notices.object_sucessfully_updated', model: ObjectParameter.model_name.human) }
        
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @object_parameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /object_parameters/1
  # DELETE /object_parameters/1.json
  def destroy
    @object_parameter = ObjectParameter.find(params[:id])
    authorize @object_parameter
    @object_parameter.destroy

    respond_to do |format|
      format.html { redirect_to inspection_object_url(@object_parameter.inspection_object_id) }
      format.json { head :no_content }
    end
  end

  private
  
  def set_current_section
    self.current_section = :inspection
  end  
  
end
