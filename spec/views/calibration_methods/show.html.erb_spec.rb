require 'spec_helper'

describe "calibration_methods/show" do
  before(:each) do
    @calibration_method = assign(:calibration_method, stub_model(CalibrationMethod,
      :name => "Name",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
  end
end
