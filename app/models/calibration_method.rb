class CalibrationMethod < ActiveRecord::Base
  attr_accessible :description, :name
  
  has_many :calibration_method_manuals, dependent: :destroy
  
  validates :name, presence: true
end
