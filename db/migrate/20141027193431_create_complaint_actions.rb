class CreateComplaintActions < ActiveRecord::Migration
  def change
    create_table :complaint_actions do |t|
      t.integer :complaint_id
      t.integer :employee_id
      t.text :action
      t.string :complaint_status_old
      t.string :complaint_status_new

      t.timestamps
    end
  end
end
