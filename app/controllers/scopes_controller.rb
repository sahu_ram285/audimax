class ScopesController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /scopes/1
  # GET /scopes/1.json
  def show
    @scope = Scope.find(params[:id])
    authorize @scope

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @scope }
    end
  end

  # GET /scopes/new
  # GET /scopes/new.json
  def new
    @program = Program.find(params[:program_id])
    @scope = @program.scopes.build
    authorize @scope

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @scope }
    end
  end

  # GET /scopes/1/edit
  def edit
    @scope = Scope.find(params[:id])
    authorize @scope
  end

  # POST /scopes
  # POST /scopes.json
  def create
    @program = Program.find(params[:program_id])
    @scope = @program.scopes.build(params[:scope])
    authorize @scope

    respond_to do |format|
      if @scope.save
        format.html { redirect_to @program, notice: I18n.t('notices.object_sucessfully_created', model: Scope.model_name.human) }
        format.json { render json: @scope, status: :created, location: @scope }
      else
        format.html { render action: "new" }
        format.json { render json: @scope.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /scopes/1
  # PUT /scopes/1.json
  def update
    @scope = Scope.find(params[:id])
    authorize @scope

    respond_to do |format|
      if @scope.update_attributes(params[:scope])
        format.html { redirect_to @scope.program, notice: I18n.t('notices.object_sucessfully_updated', model: Scope.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @scope.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scopes/1
  # DELETE /scopes/1.json
  def destroy
    @scope = Scope.find(params[:id])
    authorize @scope
    @scope.destroy

    respond_to do |format|
      format.html { redirect_to program_path(@scope.program_id) }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end  
  
end
