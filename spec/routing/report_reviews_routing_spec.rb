require "spec_helper"

describe ReportReviewsController do
  describe "routing" do

    it "routes to #index" do
      get("/report_reviews").should route_to("report_reviews#index")
    end

    it "routes to #new" do
      get("/report_reviews/new").should route_to("report_reviews#new")
    end

    it "routes to #show" do
      get("/report_reviews/1").should route_to("report_reviews#show", :id => "1")
    end

    it "routes to #edit" do
      get("/report_reviews/1/edit").should route_to("report_reviews#edit", :id => "1")
    end

    it "routes to #create" do
      post("/report_reviews").should route_to("report_reviews#create")
    end

    it "routes to #update" do
      put("/report_reviews/1").should route_to("report_reviews#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/report_reviews/1").should route_to("report_reviews#destroy", :id => "1")
    end

  end
end
