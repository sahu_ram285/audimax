class ReportEquipmentsController < ApplicationController
  def create
  end

  def destroy
    @report_equipment = ReportEquipment.find(params[:id])
    authorize @report_equipment.report, :update?
    @report_equipment.destroy

    respond_to do |format|
      format.html { redirect_to edit_report_path(@report_equipment.report_id) }
      format.json { head :no_content }
      format.js
    end
  end
end
