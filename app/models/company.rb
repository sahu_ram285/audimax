class Company < ActiveRecord::Base
  attr_accessible :address, :city, :name, :postalcode, :state, :country, :phone, :fax, :email, :web, :image, :remove_image
  
  has_many :accreditations
  
  validates :name, :presence => true
  
  mount_uploader :image, ImageUploader
end
