require "spec_helper"

describe ReportErrorCategoriesController do
  describe "routing" do

    it "routes to #index" do
      get("/report_error_categories").should route_to("report_error_categories#index")
    end

    it "routes to #new" do
      get("/report_error_categories/new").should route_to("report_error_categories#new")
    end

    it "routes to #show" do
      get("/report_error_categories/1").should route_to("report_error_categories#show", :id => "1")
    end

    it "routes to #edit" do
      get("/report_error_categories/1/edit").should route_to("report_error_categories#edit", :id => "1")
    end

    it "routes to #create" do
      post("/report_error_categories").should route_to("report_error_categories#create")
    end

    it "routes to #update" do
      put("/report_error_categories/1").should route_to("report_error_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/report_error_categories/1").should route_to("report_error_categories#destroy", :id => "1")
    end

  end
end
