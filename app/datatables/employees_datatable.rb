class EmployeesDatatable
  delegate :params, :h, :link_to, :mail_to, :raw, :edit_employee_path, :image_tag, :policy, :policy_scope, :country_name, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(Employee).count,
      iTotalDisplayRecords: employees.total_entries,
      aaData: data
    }
  end

private

  def data
    employees.map do |employee|
      [
        (employee.image_url.nil? ? "" : link_to(image_tag(employee.image_url(:thumb).to_s, :class => 'img-polaroid', :size => "48x30"), employee)),
        employee.fullname,
        employee.position.name,
        employee.phone1,
        mail_to(employee.email),
        employee.city,
        country_name(employee.country),
				'<span class="btn-group">'+
        link_to(raw("<i class='icon-search'></i>"), employee, { :class => 'btn btn-small' })+
        (policy(employee).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_employee_path(employee), { :class => 'btn btn-small' }) : "") +
        (policy(employee).destroy? ? link_to(raw("<i class='icon-trash'></i>"), employee, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
        '</span>'
      ]
    end
  end

  def employees
    @employees ||= fetch_employees
  end

  def fetch_employees
    employees = policy_scope(Employee).order("#{sort_column} #{sort_direction}")
    employees = employees.joins(:position).includes(:position)

    employees = employees.page(page).per_page(per_page)
    employees
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[employees.image employees.lastname positions.name employees.phone1 employees.email employees.city employees.country]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end