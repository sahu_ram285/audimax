class Regulation < ActiveRecord::Base
  attr_accessible :end_date, :program_id, :start_date, :status, :title, :version
  
  belongs_to :program
  has_many :sections, :dependent => :destroy
  
  validates :program_id, :status, :title, :presence => true
  validates :status, inclusion: { in: %w(NEW FINAL), message: "%{value} is not a valid status" }
  
  after_initialize :set_default_values
  
  scope :valid_on, lambda { |date| where("start_date <= ? AND (end_date >= ? OR end_date IS NULL) AND status = 'FINAL'", date, date) }
  
  def set_default_values
    self.status ||= 'NEW'
  end
  
  def self.order_sections(sorted_ids)
    seq_nr = 1
    sorted_ids.each do |id|
      section = Section.find(id)
      if section.seq_nr != seq_nr
        section.seq_nr = seq_nr
        section.save!
      end
      seq_nr += 1
    end
  end
  
end
