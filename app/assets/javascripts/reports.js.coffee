# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
		
$ -> 
	$('.am-btn-instruction').click ->
		$(this).closest('td').prev('td').find('.am-instruction').toggle('fast')

	$('.am-btn-comments').click ->
		$(this).closest('td').prev('td').find('.am-comments').toggle('fast')

	$('.selectpicker').change ->
		btn = $(this).next('div.bootstrap-select').children('button') 
		btn.removeClass 'btn-success btn-danger btn-warning btn-inverse'
		if $(this).val() == "COM"
			btn.addClass 'btn-success'
		else if $(this).val() == "NC1"
			btn.addClass 'btn-danger'
		else if $(this).val() == "NC2"
			btn.addClass 'btn-warning'
		else if $(this).val() == "NA"
			btn.addClass 'btn-inverse'
