class ReportReviewsController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /report_reviews
  # GET /report_reviews.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: ReportReviewsDatatable.new(view_context) }
    end
  end

  # GET /report_reviews/1
  # GET /report_reviews/1.json
  def show
    @report_review = policy_scope(ReportReview).find(params[:id])
    authorize @report_review
    
    @assessment_names = Hash[ProgramAssessment.joins(:assessment).includes(:assessment).where("program_id = :program_id", :program_id => @report_review.report.scope.program_id).map { |a| [a.code, ((a.name.nil? or a.name.empty?) ? a.assessment.name : a.name)] }]

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @report_review }
    end
  end

  # GET /report_reviews/new
  # GET /report_reviews/new.json
  def new
    @report_review = ReportReview.new
    authorize @report_review

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @report_review }
    end
  end

  # GET /report_reviews/1/edit
  def edit
    @report_review = policy_scope(ReportReview).find(params[:id])
    authorize @report_review

    @assessment_names = Hash[ProgramAssessment.joins(:assessment).includes(:assessment).where("program_id = :program_id", :program_id => @report_review.report.scope.program_id).map { |a| [a.code, ((a.name.nil? or a.name.empty?) ? a.assessment.name : a.name)] }]
    @assessment_options = @assessment_names.map { |a| [a[1], a[0]] }
    @assessment_options_with_blank = @assessment_options + [['', '']]
    
    #@report_review.report_errors.build
  end

  # POST /report_reviews
  # POST /report_reviews.json
  def create
    authorize ReportReview, :create?
    @report_review = ReportReview.create_review(params[:report_id], current_user.employee_id)

    respond_to do |format|
      if @report_review
        format.html { redirect_to edit_report_review_path(@report_review) }
        format.json { render json: @report_review, status: :created, location: @report_review }
      else
        format.html { redirect_to reports_path, alert: I18n.t('alerts.create_review_failed') }
      end
    end
  end

  # PUT /report_reviews/1
  # PUT /report_reviews/1.json
  def update
    @report_review = policy_scope(ReportReview).find(params[:id])
    authorize @report_review

    respond_to do |format|
      if @report_review.update_attributes(params[:report_review])
        format.html { redirect_to @report_review, notice: I18n.t('notices.object_sucessfully_updated', model: ReportReview.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @report_review.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /report_reviews/1
  # DELETE /report_reviews/1.json
  def destroy
    @report_review = policy_scope(ReportReview).find(params[:id])
    authorize @report_review
    @report_review.destroy

    respond_to do |format|
      format.html { redirect_to report_reviews_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end
  
end
