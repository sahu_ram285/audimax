/*
 * jquery.flot.tooltip
 *
 * desc:	create tooltip with values of hovered point on the graph, 
					support many series, time mode, stacking and pie charts
					you can set custom tip content (also with use of HTML tags) and precision of values
 * version:	0.4.4
 * author: 	Krzysztof Urbas @krzysu [myviews.pl] with help of @ismyrnow
 * website:	https://github.com/krzysu/flot.tooltip
 * 
 * released under MIT License, 2012
*/
(function(e){var t={tooltip:!1,tooltipOpts:{content:"%s | X: %x | Y: %y.2",dateFormat:"%y-%0m-%0d",shifts:{x:10,y:20},defaultTheme:!0}},n=function(t){var n={x:0,y:0},r=t.getOptions(),i=function(e){n.x=e.x,n.y=e.y},s=function(e){var t={x:0,y:0};t.x=e.pageX,t.y=e.pageY,i(t)},o=function(t){var n=new Date(t);return e.plot.formatDate(n,r.tooltipOpts.dateFormat)};t.hooks.bindEvents.push(function(t,i){var a=r.tooltipOpts,f=t.getPlaceholder(),l;if(r.tooltip===!1)return;e("#flotTip").length>0?l=e("#flotTip"):(l=e("<div />").attr("id","flotTip"),l.appendTo("body").hide().css({position:"absolute"}),a.defaultTheme&&l.css({background:"#fff","z-index":"100",padding:"0.4em 0.6em","border-radius":"0.5em","font-size":"0.8em",border:"1px solid #111"})),e(f).bind("plothover",function(e,t,i){if(i){var s;r.xaxis.mode==="time"||r.xaxes[0].mode==="time"?s=u(a.content,i,o):s=u(a.content,i),l.html(s).css({left:n.x+a.shifts.x,top:n.y+a.shifts.y}).show()}else l.hide().html("")}),i.mousemove(s)});var u=function(e,t,n){var r=/%p\.{0,1}(\d{0,})/,i=/%s/,s=/%x\.{0,1}(\d{0,})/,o=/%y\.{0,1}(\d{0,})/;return typeof t.series.percent!="undefined"&&(e=a(r,e,t.series.percent)),typeof t.series.label!="undefined"&&(e=e.replace(i,t.series.label)),typeof n=="function"?e=e.replace(s,n(t.series.data[t.dataIndex][0])):typeof t.series.data[t.dataIndex][0]=="number"&&(e=a(s,e,t.series.data[t.dataIndex][0])),typeof t.series.data[t.dataIndex][1]=="number"&&(e=a(o,e,t.series.data[t.dataIndex][1])),e},a=function(e,t,n){var r;return t.match(e)!=="null"&&(RegExp.$1!==""&&(r=RegExp.$1,n=n.toFixed(r)),t=t.replace(e,n)),t}};e.plot.plugins.push({init:n,options:t,name:"tooltip",version:"0.4.4"})})(jQuery);