class ReportAssessmentsChart
  
  def assessment_name(assessment)
    Assessment.where(:code => assessment).first.name
  end
  
  def series_data
	  ReportItem.joins(:report).where("inspection_date > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)").group('assessment').select('assessment, count(*) nr_assessments').all.map { |a| {label: assessment_name(a.assessment), data: a.nr_assessments} }
  end
  
  def as_json(options = {})
    { data: series_data,
      options: {
    		series: {
    			pie: {
    				show: true
    			}
    		},
    		legend: {
    			show: false,
    		}  
      }
    }
  end
  
end
