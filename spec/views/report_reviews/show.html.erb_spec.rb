require 'spec_helper'

describe "report_reviews/show" do
  before(:each) do
    @report_review = assign(:report_review, stub_model(ReportReview,
      :report_id => 1,
      :status => "Status",
      :comments => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Status/)
    rendered.should match(/MyText/)
  end
end
