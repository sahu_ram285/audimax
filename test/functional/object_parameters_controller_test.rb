require 'test_helper'

class ObjectParametersControllerTest < ActionController::TestCase
  setup do
    @object_parameter = object_parameters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:object_parameters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create object_parameter" do
    assert_difference('ObjectParameter.count') do
      post :create, object_parameter: { inspection_object_id: @object_parameter.inspection_object_id, name: @object_parameter.name, value: @object_parameter.value }
    end

    assert_redirected_to object_parameter_path(assigns(:object_parameter))
  end

  test "should show object_parameter" do
    get :show, id: @object_parameter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @object_parameter
    assert_response :success
  end

  test "should update object_parameter" do
    put :update, id: @object_parameter, object_parameter: { inspection_object_id: @object_parameter.inspection_object_id, name: @object_parameter.name, value: @object_parameter.value }
    assert_redirected_to object_parameter_path(assigns(:object_parameter))
  end

  test "should destroy object_parameter" do
    assert_difference('ObjectParameter.count', -1) do
      delete :destroy, id: @object_parameter
    end

    assert_redirected_to object_parameters_path
  end
end
