class RemoveEmployeeIdFromNotification < ActiveRecord::Migration
  def up
    remove_column :notifications, :employee_id
  end

  def down
    add_column :notifications, :employee_id, :integer
  end
end
