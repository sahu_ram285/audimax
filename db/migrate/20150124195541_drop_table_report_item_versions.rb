class DropTableReportItemVersions < ActiveRecord::Migration
  def up
    drop_table :report_item_versions
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
