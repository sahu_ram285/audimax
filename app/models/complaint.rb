class Complaint < ActiveRecord::Base
  attr_accessible :action, :assigned_to_id, :cause, :client_id, :complainant_address, :complainant_city, :complainant_client_id, :complainant_country, :complainant_email, :complainant_employee_id, :complainant_name, :complainant_phone1, :complainant_phone2, :complainant_postalcode, :complainant_state, :complaint, :complaint_type, :employee_id, :handled_date, :impact, :inspection_object_id, :inspection_object_nr, :owner_id, :planned_handled_date, :priority, :received_date, :report_id, :resolution, :status
  attr_accessible :complaint_actions_attributes
  
  attr_accessor :action

  has_many :complaint_actions, :dependent => :destroy
  belongs_to :assignee, :foreign_key => :assigned_to_id, :class_name => 'Employee'
  belongs_to :owner, :foreign_key => :owner_id, :class_name => 'Employee'
  belongs_to :complainant_employee, :foreign_key => :complainant_employee_id, :class_name => 'Employee'
  belongs_to :complainant_client, :foreign_key => :complainant_client_id, :class_name => 'Client'
  belongs_to :employee
  belongs_to :client
  belongs_to :inspection_object
  belongs_to :report

  accepts_nested_attributes_for :complaint_actions  
    
  validates :complaint_type, :owner_id, :status, :presence => true
  validates :assigned_to_id, :presence => true, :unless => :is_closed
  #validate :report_id_is_valid
  validates :report, :presence => { message: :unknown_reference }, :if => :report_id_is_not_nil?
  validates :inspection_object, :presence => { message: :unknown_reference }, :if => :inspection_object_id_is_not_nil?
    
  after_initialize :set_default_values
  before_validation :set_handled_date
  
  scope :last_12_months, -> { where("complaints.received_date > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)") }
  
  # complaint_type
  #
  # COMPLAINT
  # DEVIANCE
  # APPEAL
  
  def self.complaint_type_values
    [ 'COMPLAINT', 'DEVIANCE', 'APPEAL']
  end
  
  def self.complaint_type_description(complaint_type)
    Complaint.human_attribute_name("complaint_type.#{complaint_type.downcase}")
  end
  
  def self.complaint_status_values
    ['NEW', 'ACKNOWLEDGED', 'ASSESSED', 'INVESTIGATED', 'RESOLUTION', 'INFORMED', 'CORRECTION', 'VERIFIED', 'CLOSED']
  end

  def self.complaint_status_description(status)
    Complaint.human_attribute_name("status.#{status.downcase}")
  end

  def self.complaint_valid_status_transitions(status)
    { 'NEW' => ['NEW', 'ACKNOWLEDGED', 'CLOSED'],
      'ACKNOWLEDGED' => ['ACKNOWLEDGED', 'ASSESSED', 'CLOSED'], 
      'ASSESSED' => ['ASSESSED', 'INVESTIGATED', 'CLOSED'],
      'INVESTIGATED' => ['INVESTIGATED', 'RESOLUTION', 'CLOSED'],
      'RESOLUTION' => ['RESOLUTION', 'INFORMED', 'CLOSED'],
      'INFORMED' => ['INFORMED', 'CORRECTION', 'CLOSED'],
      'CORRECTION' => ['CORRECTION', 'VERIFIED', 'CLOSED'],
      'VERIFIED' => ['VERIFIED', 'CLOSED'],
      'CLOSED' => ['CLOSED']
      }[status]
  end
  
  def self.complaint_priority_values
    ['1', '2', '3', '4', '5']
  end

  def self.complaint_priority_description(priority)
    {'1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'}[priority]
  end
  
  def set_default_values
    if self.new_record?
      self.status ||= 'NEW'
      self.received_date ||= Date.today
      self.priority ||= '5'
    end
  end
  
  # Automatically fill handled_date if status is set to closed.
  def set_handled_date
    if self.status == 'CLOSED' and self.status_was != 'CLOSED' and self.handled_date.nil?
      self.handled_date = Date.today
    end
  end
  
  def report_id_is_not_nil?
    !self.report_id.nil?
  end
  
  def inspection_object_id_is_not_nil?
    !self.inspection_object_id.nil?
  end
  
  def is_closed
    self.status == 'CLOSED'
  end
  
  #
  # Virtual attribute inspection_object_nr
  #
  
  def inspection_object_nr
  	inspection_object.nr if inspection_object
  end
  
  def inspection_object_nr=(nr)
    self.inspection_object = InspectionObject.find_by_nr(nr)
    if self.inspection_object.nil? and !nr.blank?
      self.inspection_object_id = -1 # force validation error
    end
  end
  
end
