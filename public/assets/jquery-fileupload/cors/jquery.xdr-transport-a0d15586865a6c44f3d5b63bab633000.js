/*
 * jQuery XDomainRequest Transport Plugin 1.1.2
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * Based on Julian Aubourg's ajaxHooks xdr.js:
 * https://github.com/jaubourg/ajaxHooks/
 */
/*jslint unparam: true */
/*global define, window, XDomainRequest */
(function(e){"use strict";typeof define=="function"&&define.amd?define(["jquery"],e):e(window.jQuery)})(function(e){"use strict";window.XDomainRequest&&!e.support.cors&&e.ajaxTransport(function(t){if(t.crossDomain&&t.async){t.timeout&&(t.xdrTimeout=t.timeout,delete t.timeout);var n;return{send:function(r,i){function o(t,r,s,o){n.onload=n.onerror=n.ontimeout=e.noop,n=null,i(t,r,s,o)}n=new XDomainRequest,t.type==="DELETE"?(t.url=t.url+(/\?/.test(t.url)?"&":"?")+"_method=DELETE",t.type="POST"):t.type==="PUT"&&(t.url=t.url+(/\?/.test(t.url)?"&":"?")+"_method=PUT",t.type="POST"),n.open(t.type,t.url),n.onload=function(){o(200,"OK",{text:n.responseText},"Content-Type: "+n.contentType)},n.onerror=function(){o(404,"Not Found")},t.xdrTimeout&&(n.ontimeout=function(){o(0,"timeout")},n.timeout=t.xdrTimeout),n.send(t.hasContent&&t.data||null)},abort:function(){n&&(n.onerror=e.noop(),n.abort())}}}})});