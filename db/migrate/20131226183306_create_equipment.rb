class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
      t.string :equipment_type
      t.string :serial_nr
      t.date :last_calibration_date

      t.timestamps
    end
  end
end
