/*!
 * jQuery UI Google Map 3.0-beta
 * http://code.google.com/p/jquery-ui-map/
 * Copyright (c) 2010 - 2011 Johan Säll Larsson
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *		jquery.ui.map.js
 */
!function(t){t.extend(t.ui.gmap.prototype,{microdata:function(e,r){var a=this;t('[itemtype="{0}"]'.replace("{0}",e)).each(function(e){r(a._traverse(t(this),{"@type":a._resolveType(t(this).attr("itemtype"))}),this,e)})},_traverse:function(e,r){var a=this;return e.children().each(function(){var e=t(this),i=e.attr("itemtype"),n=e.attr("itemProp");if(void 0!=i&&e.children().length>0)r[n]||(r[n]=[]),r[n].push({"@type":a._resolveType(i)}),a._traverse(e,r[n][r[n].length-1]);else if(n)if(r[n]){if("string"==typeof r[n]){var s=r[n];r[n]=[],r[n].push(s)}r[n].push(a._extract(e))}else r[n]=a._extract(e);else a._traverse(e,r)}),r},_extract:function(t){return t.attr("src")?t.attr("src"):t.attr("href")?t.attr("href"):t.attr("content")?t.attr("content"):t.attr("datetime")?t.attr("datetime"):t.text()?t.text():void 0},_resolveType:function(t){return t.indexOf("http")>-1?t=t.substr(t.lastIndexOf("/")+1).replace("?","").replace("#",""):t.indexOf(":")>-1&&(t=t.split(":")[1]),t}})}(jQuery);