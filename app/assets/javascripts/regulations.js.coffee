$ ->
	# Drag & drop sections to change their order
	$('#sections-table').sortable
		update: (event, ui) ->
			sortedIDs = $( "#sections-table" ).sortable("toArray", { attribute: "data-id"} )
			url = '/regulations/'+$('#sections-table').data('regulation-id')+'/order'
			$.post(url, { order: sortedIDs })
	$('#sections-table').disableSelection()
