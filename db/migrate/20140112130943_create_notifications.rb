class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :employee_id
      t.text :message
      t.boolean :read

      t.timestamps
    end
  end
end
