module ReportReviewsHelper
  
  def report_review_status_select_options_all
    ReportReview.report_review_status_values.map { |v| [ ReportReview.report_review_status_description(v), v ] }
  end
  
  def report_review_status_select_options(status)
    ReportReview.report_review_valid_status_transitions(status).map { |v| [ ReportReview.report_review_status_description(v), v ] }
  end
  
  def link_to_remove_error_fields(e)
    fields = e.hidden_field(:_destroy)
    fields += link_to_function raw("<i class='icon-trash'></i>"), "audimax.remove_error_fields(this)", :class => 'btn btn-small'
  end
  
  def link_to_add_error_fields(f, i, review)
    new_object = i.report_errors.new
    new_object.report_review_id = review.id
    new_object.old_assessment = i.assessment
    fields = f.fields_for :report_errors, new_object, :child_index => "new_report_error" do |e|
      render('error_fields', :e => e)
    end
    link_to_function raw("<i class='icon-warning-sign'></i>"), "audimax.add_error_fields(this, \"#{escape_javascript(fields)}\")", :class => 'btn btn-small'
  end
  
  def report_review_status_label(status)
    label_class = {'NEW' => 'default', 'FILED' => 'important', 'FINAL' => 'success'}[status]
		raw("<span class='label label-#{label_class}'>#{ReportReview.report_review_status_description(status)}</span>")
	end
  
end
