class RemoveInspectorIdFromReportVersion < ActiveRecord::Migration
  def up
    remove_column :report_versions, :inspector_id
  end

  def down
    add_column :report_versions, :inspector_id, :integer
  end
end
