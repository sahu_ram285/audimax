class UsersController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  def index
    respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context) }
    end
  end

  def new
    @user = User.new
    authorize @user

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
    @user = User.find(params[:id])
    authorize @user
  end

  def create
    @user = User.new(params[:user])
    authorize @user

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_url, notice: I18n.t('notices.object_sucessfully_created', model: User.model_name.human) }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @user = User.find(params[:id])
    authorize @user
  	roles = Role.find(params[:role_ids]) rescue []
  	@user.roles = roles

    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to users_url, notice: I18n.t('notices.object_sucessfully_updated', model: User.model_name.human) }
      else
        format.html { render action: "edit" }
      end
    end
  end  

  def destroy
  	@user = User.find(params[:id])
    authorize @user
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
    end

  end
  
  
  private
  
  def set_current_section
    self.current_section = :system
  end  

end
