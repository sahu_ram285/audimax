class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :inspection_object_id
      t.integer :inspector_id
      t.date :inspection_date
      t.integer :scope_id
      t.integer :regulation_id
      t.text :comments

      t.timestamps
    end
  end
end
