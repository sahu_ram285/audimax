class ComplaintsDatatable
  delegate :params, :h, :link_to, :number_to_currency, :raw, :edit_complaint_path, :complaint_path, :complaint_priority_span, :complaint_status_span, :button_to, :current_user, :policy, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(Complaint).count,
      iTotalDisplayRecords: complaints.total_entries,
      aaData: data
    }
  end

private

  def data
    complaints.map do |complaint|
      [
        complaint.id,
        Complaint.complaint_type_description(complaint.complaint_type),
        complaint.received_date,
        complaint_priority_span(complaint.priority),
        complaint.planned_handled_date,
        complaint.handled_date,
        complaint_status_span(complaint.status),
        (!complaint.assignee.nil?) ? complaint.assignee.fullname : "",
        (!complaint.owner.nil?) ? complaint.owner.fullname : "",
				'<span class="btn-group">'+
          link_to(raw("<i class='icon-search'></i>"), complaint, { :class => 'btn btn-small' })+
          (policy(complaint).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_complaint_path(complaint), { :class => 'btn btn-small' }) : "")+
          (policy(complaint).destroy? ? link_to(raw("<i class='icon-trash'></i>"), complaint, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
          '</span>'
      ]
    end
  end

  def complaints
    @complaints ||= fetch_complaints
  end

  def fetch_complaints
    complaints = policy_scope(Complaint).order("#{sort_column} #{sort_direction}")
    
    if params[:id].present?
      complaints = complaints.where("complaints.id = :id", id: params[:id])
    end
    if params[:complaint_type].present?
      complaints = complaints.where("complaints.complaint_type = :complaint_type", complaint_type: params[:complaint_type])
    end
    if params[:priority].present?
      complaints = complaints.where("complaints.priority = :priority", priority: params[:priority])
    end
    if params[:status].present?
      complaints = complaints.where("complaints.status = :status", status: params[:status])
    end
    if params[:assigned_to_id].present?
      complaints = complaints.where("complaints.assigned_to_id = :assigned_to_id", assigned_to_id: params[:assigned_to_id])
    end
    if params[:owner_id].present?
      complaints = complaints.where("complaints.owner_id = :owner_id", owner_id: params[:owner_id])
    end

    complaints = complaints.page(page).per_page(per_page)
    complaints
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[complaints.id complaints.complaint_type complaints.received_date complaints.priority complaints.planned_handled_data complaints.handled_data complaints.status]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end