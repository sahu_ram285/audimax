class CompaniesDatatable
  delegate :params, :h, :link_to, :number_to_currency, :raw, :edit_company_path, :image_tag, :policy, :policy_scope, :country_name, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(Company).count,
      iTotalDisplayRecords: companies.total_entries,
      aaData: data
    }
  end

private

  def data
    companies.map do |company|
      [
        (company.image_url.nil? ? "" : link_to(image_tag(company.image_url(:thumb).to_s, :size => "128x80"), company)),
        company.name,
				company.address,
				company.postalcode,
				company.city,
				company.state,
				country_name(company.country),
				'<span class="btn-group">'+
          link_to(raw("<i class='icon-search'></i>"), company, { :class => 'btn btn-small' })+
          (policy(company).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_company_path(company), { :class => 'btn btn-small' }) : "")+
          (policy(company).destroy? ? link_to(raw("<i class='icon-trash'></i>"), company, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
          '</span>'
      ]
    end
  end

  def companies
    @companies ||= fetch_companies
  end

  def fetch_companies
    companies = policy_scope(Company).order("#{sort_column} #{sort_direction}")
    companies = companies.page(page).per_page(per_page)
    companies
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[image name address postalcode city state country]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end