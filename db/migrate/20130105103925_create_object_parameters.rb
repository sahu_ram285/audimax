class CreateObjectParameters < ActiveRecord::Migration
  def change
    create_table :object_parameters do |t|
      t.integer :inspection_object_id
      t.string :name
      t.string :value

      t.timestamps
    end
  end
end
