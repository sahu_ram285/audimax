class AddEmploymentEndDateToEmployee < ActiveRecord::Migration
  def change
    add_column :employees, :employment_end_date, :date
  end
end
