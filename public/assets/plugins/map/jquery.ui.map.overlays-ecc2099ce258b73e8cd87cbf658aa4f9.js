/*!
 * jQuery UI Google Map 3.0-rc
 * http://code.google.com/p/jquery-ui-map/
 * Copyright (c) 2010 - 2012 Johan Säll Larsson
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *      jquery.ui.map.js
 */
!function(e){e.extend(e.ui.gmap.prototype,{addShape:function(a,s){var t=new google.maps[a](jQuery.extend({map:this.get("map")},s));return this.get("overlays > "+a,[]).push(t),e(t)},loadFusion:function(e,a){(a?this.get("overlays > FusionTablesLayer",new google.maps.FusionTablesLayer(a,e)):this.get("overlays > FusionTablesLayer",new google.maps.FusionTablesLayer)).setOptions(jQuery.extend({map:this.get("map")},e))},loadKML:function(e,a,s){this.get("overlays > "+e,new google.maps.KmlLayer(a,jQuery.extend({map:this.get("map")},s)))}})}(jQuery);