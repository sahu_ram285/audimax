class RemoveReportVersionIdFromReportReview < ActiveRecord::Migration
  def up
    remove_column :report_reviews, :report_version_id
  end

  def down
    add_column :report_reviews, :report_version_id, :integer
  end
end
