module ReportsHelper
  
  def report_status_select_options(status)
    Report.report_valid_status_transitions(status).map { |v| [ Report.report_status_description(v), v ] }
  end

	def report_status_label(status)
    label_class = {'NEW' => 'default', 'FILED' => 'important', 'REVIEW' => 'warning', 'FINAL' => 'success'}[status]
		raw("<span class='label label-#{label_class}'>#{Report.report_status_description(status)}</span>")
	end
	
	def report_assessment_class(assessment)
		{ 'COM' => 'btn-success', 'NC1' => 'btn-danger', 'NC2' => 'btn-warning', 'NA' => 'btn-inverse', 'NONE' => '', '' => '' }[assessment]
	end
	
  def report_assessment_cell_class(assessment)
		{ 'COM' => 'am-assessment-cell-success', 'NC1' => 'am-assessment-cell-error', 'NC2' => 'am-assessment-cell-warning', 'NA' => '', 'NONE' => 'am-assessment-cell-none' }[assessment]
  end

  def report_assessment_chart_color(assessment)
		{ 'COM' => '#5bb75b', 'NC1' => '#da4f49', 'NC2' => '#f89406', 'NA' => '#363636', 'NONE' => '#F4F447' }[assessment]
  end
  
end
