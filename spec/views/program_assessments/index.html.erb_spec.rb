require 'spec_helper'

describe "program_assessments/index" do
  before(:each) do
    assign(:program_assessments, [
      stub_model(ProgramAssessment,
        :code => "Code",
        :name => "Name"
      ),
      stub_model(ProgramAssessment,
        :code => "Code",
        :name => "Name"
      )
    ])
  end

  it "renders a list of program_assessments" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
