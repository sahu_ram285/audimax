class RemoveCalibrations < ActiveRecord::Migration
  def up
    drop_table :calibrations
  end

  def down
  end
end
