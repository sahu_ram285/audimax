require 'spec_helper'

describe "calibration_methods/new" do
  before(:each) do
    assign(:calibration_method, stub_model(CalibrationMethod,
      :name => "MyString",
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new calibration_method form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", calibration_methods_path, "post" do
      assert_select "input#calibration_method_name[name=?]", "calibration_method[name]"
      assert_select "textarea#calibration_method_description[name=?]", "calibration_method[description]"
    end
  end
end
