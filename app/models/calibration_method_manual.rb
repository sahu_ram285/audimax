class CalibrationMethodManual < ActiveRecord::Base
  attr_accessible :calibration_method_id, :end_date, :manual, :start_date
  
  belongs_to :calibration_method
  
  validates :manual, presence: true
  validate :validate_dates
  
  def validate_dates
    if !self.start_date.nil? && !self.end_date.nil? && self.start_date > self.end_date
      errors.add(:start_date, :later_than, max: CalibrationMethodManual.human_attribute_name('end_date'))
    end
  end
  
  mount_uploader :manual, AttachmentUploader
end
