class MaintenanceSchemesController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /maintenance_schemes
  # GET /maintenance_schemes.json
  def index
    @maintenance_schemes = policy_scope(MaintenanceScheme).order("name")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @maintenance_schemes }
    end
  end

  # GET /maintenance_schemes/1
  # GET /maintenance_schemes/1.json
  def show
    @maintenance_scheme = MaintenanceScheme.find(params[:id])
    authorize @maintenance_scheme

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @maintenance_scheme }
    end
  end

  # GET /maintenance_schemes/new
  # GET /maintenance_schemes/new.json
  def new
    @maintenance_scheme = MaintenanceScheme.new
    authorize @maintenance_scheme

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @maintenance_scheme }
    end
  end

  # GET /maintenance_schemes/1/edit
  def edit
    @maintenance_scheme = MaintenanceScheme.find(params[:id])
    authorize @maintenance_scheme
  end

  # POST /maintenance_schemes
  # POST /maintenance_schemes.json
  def create
    @maintenance_scheme = MaintenanceScheme.new(params[:maintenance_scheme])
    authorize @maintenance_scheme

    respond_to do |format|
      if @maintenance_scheme.save
        format.html { redirect_to maintenance_schemes_path, notice: I18n.t('notices.object_sucessfully_created', model: MaintenanceScheme.model_name.human) }
        format.json { render json: @maintenance_scheme, status: :created, location: @maintenance_scheme }
      else
        format.html { render action: "new" }
        format.json { render json: @maintenance_scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /maintenance_schemes/1
  # PUT /maintenance_schemes/1.json
  def update
    @maintenance_scheme = MaintenanceScheme.find(params[:id])
    authorize @maintenance_scheme

    respond_to do |format|
      if @maintenance_scheme.update_attributes(params[:maintenance_scheme])
        format.html { redirect_to maintenance_schemes_path, notice: I18n.t('notices.object_sucessfully_updated', model: MaintenanceScheme.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @maintenance_scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maintenance_schemes/1
  # DELETE /maintenance_schemes/1.json
  def destroy
    @maintenance_scheme = MaintenanceScheme.find(params[:id])
    authorize @maintenance_scheme
    @maintenance_scheme.destroy

    respond_to do |format|
      format.html { redirect_to maintenance_schemes_url }
      format.json { head :no_content }
    end
  end
  
  def set_current_section
    self.current_section = :assets
  end  
  
end
