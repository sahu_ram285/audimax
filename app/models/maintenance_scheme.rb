class MaintenanceScheme < ActiveRecord::Base
  attr_accessible :description, :interval, :maintenance_type, :name, :warn_before
  
  validates :interval, :maintenance_type, :name, :presence => true
  validates :interval, :numericality => { :greater_than => 0 }
  
  def maintenance_type_name
    self.maintenance_type == 'CA' ? 'Kalibratie' : ''
  end
  
end
