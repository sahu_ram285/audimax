class AddInstructionToSection < ActiveRecord::Migration
  def change
    add_column :sections, :instruction, :text
  end
end
