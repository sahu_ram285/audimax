class AddStartDateToAccreditations < ActiveRecord::Migration
  def change
    add_column :accreditations, :start_date, :date
    add_column :accreditations, :end_date, :date
  end
end
