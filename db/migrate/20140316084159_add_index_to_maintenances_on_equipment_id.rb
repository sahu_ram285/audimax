class AddIndexToMaintenancesOnEquipmentId < ActiveRecord::Migration
  def change
    add_index :maintenances, :equipment_id
  end
end
