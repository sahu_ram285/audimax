class ComplaintAction < ActiveRecord::Base
  attr_accessible :action, :complaint_id, :complaint_status_new, :complaint_status_old, :employee_id, :user_id
  
  belongs_to :complaint
  belongs_to :user
end
