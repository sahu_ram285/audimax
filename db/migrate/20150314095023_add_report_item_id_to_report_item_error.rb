class AddReportItemIdToReportItemError < ActiveRecord::Migration
  def change
    add_column :report_item_errors, :report_item_id, :integer
  end
end
