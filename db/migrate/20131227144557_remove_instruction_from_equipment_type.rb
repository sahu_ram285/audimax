class RemoveInstructionFromEquipmentType < ActiveRecord::Migration
  def up
    remove_column :equipment_types, :instruction
  end

  def down
    add_column :equipment_types, :instruction, :string
  end
end
