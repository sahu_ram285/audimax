class Employee < ActiveRecord::Base
  attr_accessible :address, :birthdate, :city, :company_id, :country, :email, :employment_date, :employment_end_date, :firstname, :image, :lastname, :notes, :phone1, :phone2, :position_id, :postcode, :remove_image, :state
  
  belongs_to :position
  belongs_to :company
  has_many :equipment
  
  mount_uploader :image, ImageUploader
  
  validate :validate_employment_dates
  validates :company_id, :position_id, :presence => true
  
  def validate_employment_dates
    if !self.employment_date.nil? && !self.employment_end_date.nil? && self.employment_date > self.employment_end_date
      errors.add(:employment_date, :later_than, max: Employee.human_attribute_name('employment_end_date'))
    end
  end
  
  def fullname
  	firstname + ' ' + lastname
  end
  
end
