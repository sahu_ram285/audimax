class AddProgramIdToProgramAssessment < ActiveRecord::Migration
  def change
    add_column :program_assessments, :program_id, :integer
  end
end
