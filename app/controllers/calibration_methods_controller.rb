class CalibrationMethodsController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /calibration_methods
  # GET /calibration_methods.json
  def index
    @calibration_methods = policy_scope(CalibrationMethod).order("name")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @calibration_methods }
    end
  end

  # GET /calibration_methods/1
  # GET /calibration_methods/1.json
  def show
    @calibration_method = CalibrationMethod.find(params[:id])
    authorize @calibration_method

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @calibration_method }
    end
  end

  # GET /calibration_methods/new
  # GET /calibration_methods/new.json
  def new
    @calibration_method = CalibrationMethod.new
    authorize @calibration_method

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @calibration_method }
    end
  end

  # GET /calibration_methods/1/edit
  def edit
    @calibration_method = CalibrationMethod.find(params[:id])
    authorize @calibration_method
  end

  # POST /calibration_methods
  # POST /calibration_methods.json
  def create
    @calibration_method = CalibrationMethod.new(params[:calibration_method])
    authorize @calibration_method

    respond_to do |format|
      if @calibration_method.save
        format.html { redirect_to calibration_methods_path, notice: I18n.t('notices.object_sucessfully_created', model: CalibrationMethod.model_name.human) }
        format.json { render json: @calibration_method, status: :created, location: @calibration_method }
      else
        format.html { render action: "new" }
        format.json { render json: @calibration_method.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /calibration_methods/1
  # PUT /calibration_methods/1.json
  def update
    @calibration_method = CalibrationMethod.find(params[:id])
    authorize @calibration_method

    respond_to do |format|
      if @calibration_method.update_attributes(params[:calibration_method])
        format.html { redirect_to calibration_methods_path, notice: I18n.t('notices.object_sucessfully_updated', model: CalibrationMethod.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @calibration_method.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /calibration_methods/1
  # DELETE /calibration_methods/1.json
  def destroy
    @calibration_method = CalibrationMethod.find(params[:id])
    authorize @calibration_method
    @calibration_method.destroy

    respond_to do |format|
      format.html { redirect_to calibration_methods_url }
      format.json { head :no_content }
    end
  end
  
  def set_current_section
    self.current_section = :assets
  end
  
end
