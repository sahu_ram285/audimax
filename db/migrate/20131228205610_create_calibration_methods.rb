class CreateCalibrationMethods < ActiveRecord::Migration
  def change
    create_table :calibration_methods do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
