/*!
 * jQuery UI Google Map 3.0-beta
 * http://code.google.com/p/jquery-ui-map/
 * Copyright (c) 2010 - 2011 Johan Säll Larsson
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *		jquery.ui.map.js
 */
(function(e){e.extend(e.ui.gmap.prototype,{rdfa:function(t,n){var r=this;e('[typeof="{0}"]'.replace("{0}",t)).each(function(t){n(r._traverse(e(this),{"@type":r._resolveType(e(this).attr("typeof"))}),this,t)})},_traverse:function(t,n){var r=this;return t.children().each(function(){var t=e(this),i=r._resolveType(t.attr("typeof")),s=r._resolveType(t.attr("rel")),o=r._resolveType(t.attr("property"));i||s||o?(s&&(t.children().length>0?(n[s]=[],r._traverse(t,n[s])):n[s]=r._extract(t,!0)),i&&(n.push({"@type":i}),r._traverse(t,n[n.length-1])),o&&(n[o]?(n[o]=[n[o]],n[o].push(r._extract(t,!1))):n[o]=r._extract(t,!1))):r._traverse(t,n)}),n},_extract:function(e,t){if(t){if(e.attr("src"))return e.attr("src");if(e.attr("href"))return e.attr("href")}if(e.attr("content"))return e.attr("content");if(e.text())return e.text();return},_resolveType:function(e){return e&&(e.indexOf("http")>-1?e=e.substr(e.lastIndexOf("/")+1).replace("?","").replace("#",""):e.indexOf(":")>-1&&(e=e.split(":")[1])),e}})})(jQuery);