class DropTableReportImages < ActiveRecord::Migration
  def up
    drop_table :report_images
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
