class PagesController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  
  def about
  end
  
  private
  
  def set_current_section
    self.current_section = :system
  end
  
end
