class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.integer :regulation_id
      t.string :title
      t.text :text

      t.timestamps
    end
  end
end
