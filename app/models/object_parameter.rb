class ObjectParameter < ActiveRecord::Base
  attr_accessible :inspection_object_id, :name, :value
  
  belongs_to :inspection_object
  
  validates :inspection_object_id, :name, :presence => true
end
