class InspectionObjectsDatatable
  delegate :params, :h, :link_to, :raw, :edit_inspection_object_path, :policy, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(InspectionObject).count,
      iTotalDisplayRecords: inspection_objects.total_entries,
      aaData: data
    }
  end

private

  def data
    inspection_objects.map do |inspection_object|
      [
        inspection_object.nr,
        inspection_object.name,
        inspection_object.client.name,
				'<span class="btn-group">'+
          link_to(raw("<i class='icon-search'></i>"), inspection_object, { :class => 'btn btn-small' })+
          (policy(inspection_object).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_inspection_object_path(inspection_object), { :class => 'btn btn-small' }) : "")+
          (policy(inspection_object).destroy? ? link_to(raw("<i class='icon-trash'></i>"), inspection_object, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")
      ]
    end
  end

  def inspection_objects
    @inspection_objects ||= fetch_inspection_objects
  end

  def fetch_inspection_objects
    inspection_objects = policy_scope(InspectionObject).order("#{sort_column} #{sort_direction}")
    inspection_objects = inspection_objects.joins(:client).includes(:client)
    
    if params[:nr].present?
      inspection_objects = inspection_objects.where("inspection_objects.nr = :nr", :nr => params[:nr])
    end
    if params[:name].present?
      inspection_objects = inspection_objects.where("lower(inspection_objects.name) like concat('%', lower(:name), '%')", :name => params[:name])
    end
    if params[:client_id].present?
      inspection_objects = inspection_objects.where("clients.id = :client_id", :client_id => params[:client_id])
    end
    if params[:q].present?
      inspection_objects = inspection_objects.where("lower(inspection_objects.nr) like concat('%', lower(:q), '%') or lower(inspection_objects.name) like concat('%', lower(:q), '%') or lower(clients.name) like concat('%', lower(:q), '%')", :q => params[:q])
    end

    inspection_objects = inspection_objects.page(page).per_page(per_page)
    inspection_objects
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[inspection_objects.nr inspection_objects.name clients.name]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end