# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#
# Create administrator user
#
puts "Creating Administrator user"
user = User.new(:email => 'admin@example.com', :password => 'admin123', :password_confirmation => 'admin123')
user.admin = true
user.save!

#
# Create application roles
#
roles = [
  { name: 'Administrator', description: 'Application administator role. No restrictions.'},
  { name: 'Inspector', description: 'Inspector role. Create reports. Read-only access to anything else.'},
  { name: 'Manager', description: 'Technical Manager role. Access to everything except application maintenance data.'},
  { name: 'Reviewer', description: 'Reviewer role. Access to everything that is required to do report reviews.'},
  { name: 'Administrative Assistant', description: 'Administrative assistant role. Access to master data.'},
  { name: 'Complaint Handler', description: 'Complaint handler role. Access to everything that is required to enter and handle complaints.'}
]
  
roles.each do |role|
  puts "Creating role #{role[:name]}"
  role = Role.find_or_create_by_name(role[:name])
  role.description = role[:description]
  role.save!  
end

#
# Create assessments
#
puts "Creating assessments"

assessments = [
  { code: 'COM', name: 'Compliant' },
  { code: 'NC1', name: 'Non Compliant Major' },
  { code: 'NC2', name: 'Non Compliant Minor' },
  { code: 'NA', name: 'Not Applicable' },
  { code: 'NONE', name: 'Not Assessed' }
]

assessments.each do |assessment|
  new_assessment = Assessment.new(assessment)
  new_assessment.save!
end
