class CalibrationMethodManualsController < ApplicationController
  
  after_filter :verify_authorized
  
  def new
    @calibration_method = CalibrationMethod.find(params[:calibration_method_id])
    @manual = @calibration_method.calibration_method_manuals.build
    authorize @manual
    
    respond_to do |format|
      format.html
    end
  end
  
  def edit
    @manual = CalibrationMethodManual.find(params[:id])
    authorize @manual
  end
  
  def create
  	@calibration_method = CalibrationMethod.find(params[:calibration_method_id])
    @manual = @calibration_method.calibration_method_manuals.build(params[:calibration_method_manual])
    authorize @manual

    respond_to do |format|
      if @manual.save
        format.html { redirect_to calibration_method_url(@calibration_method), notice: I18n.t('notices.object_sucessfully_created', model: CalibrationMethodManual.model_name.human) }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def update
    @manual = CalibrationMethodManual.find(params[:id])
    authorize @manual

    respond_to do |format|
      if @manual.update_attributes(params[:calibration_method_manual])
        format.html { redirect_to calibration_method_url(@manual.calibration_method_id), notice: I18n.t('notices.object_sucessfully_updated', model: CalibrationMethodManual.model_name.human) }
      else
        format.html { render action: "edit" }
      end
    end
  end
  
  def destroy
    @manual = CalibrationMethodManual.find(params[:id])
    authorize @manual
    @manual.destroy

    respond_to do |format|
      format.html { redirect_to calibration_method_url(@manual.calibration_method_id) }
    end
  end
  
end
