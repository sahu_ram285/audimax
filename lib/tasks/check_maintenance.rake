namespace :audimax do
  
  def send_notification(employee_id, message)
    if !employee_id.nil?
      user = User.find_by_employee_id(employee_id)
      if !user.nil?
        Notification.notify(user.id, message)
      end
    end
  end
  
  def timestamp
    Time.now.strftime('%Y-%d-%m %H:%M:%S')
  end
  
	desc "Check maintenance"
	task :check_maintenance => :environment do
    
    logger = Rails.logger

    logger.info("[#{timestamp}] Check maintenance")
    Equipment.joins(:equipment_type).includes(:equipment_type).where("in_service_date <= :today and (out_of_service_date is null or out_of_service_date < :today)", :today => Date.today).each do |equipment|
      equipment.equipment_type.maintenance_schemes.each do |scheme|
        
        last_maintenance = equipment.maintenances.where("maintenance_type = :maintenance_type", :maintenance_type => scheme.maintenance_type).order("maintenance_date").last
        last_maintenance_date = (last_maintenance.nil? ? equipment.in_service_date : last_maintenance.maintenance_date)
        
        max_maintenance_date = last_maintenance_date + scheme.interval
        
        if Date.today > max_maintenance_date
          send_notification(equipment.employee_id, "#{scheme.maintenance_type_name} termijn verstreken voor #{equipment.equipment_type.name}, serienummer #{equipment.serial_nr}, op #{max_maintenance_date}")
        elsif Date.today > max_maintenance_date - scheme.warn_before
          send_notification(equipment.employee_id, "#{scheme.maintenance_type_name} nodig voor #{equipment.equipment_type.name}, serienummer #{equipment.serial_nr}, op uiterlijk #{max_maintenance_date}")
        end
        
      end
    end
    logger.info("[#{timestamp}] Check maintenance done")
	end
end
