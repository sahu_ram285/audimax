class AddClientIdToInspectionObject < ActiveRecord::Migration
  def change
    add_column :inspection_objects, :client_id, :integer
  end
end
