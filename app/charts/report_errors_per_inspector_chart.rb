class ReportErrorsPerInspectorChart
  
  def initialize(category_id)
    @ticks = []
    @data = []
    create_ticks_and_data(category_id)
  end
  
  def create_ticks_and_data(category_id)
    categories = ReportError.last_12_months.joins(:report_error_category)
    if !category_id.nil?
      categories = categories.where("report_error_categories.id = ?", category_id)
    end
    categories = categories.select("employees.lastname as name, count(*) as nr_errors").group("1").order("2 asc")
    
    categories.each_with_index do |cat, i|
      @data.push([cat.nr_errors, i])
      @ticks.push([i, cat.name])
    end
  end
  
  def as_json(options = {})
    { data: [{
        data: @data
      }],
      options: {
        series: {
          bars: {
            show: true
          }
        },
        bars: {
          align: "center",
          barWidth: 0.5,
          horizontal: true,
          lineWidth: 1
        },
        xaxis: {
		      tickDecimals: 0
        },
        yaxis: {
          ticks: @ticks,
        },
        legend: {
		      show: false,
        },
        grid: {
          hoverable: true,
          borderWidth: 1
        }
      }
    }
  end

end
