require 'spec_helper'

describe "maintenance_schemes/show" do
  before(:each) do
    @maintenance_scheme = assign(:maintenance_scheme, stub_model(MaintenanceScheme,
      :name => "Name",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
  end
end
