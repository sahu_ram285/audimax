class AddReportReviewIdToReportItemError < ActiveRecord::Migration
  def change
    add_column :report_item_errors, :report_review_id, :integer
  end
end
