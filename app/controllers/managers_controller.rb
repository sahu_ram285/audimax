class ManagersController < ApplicationController

  before_filter :authenticate_user!

  def dashboard
    @dashboard = Dashboard.new
  end
  
  def reports
    respond_to do |format|
      format.json { render json: ReportsChart.new }
    end
  end
  
  def report_assessments
    respond_to do |format|
      format.json { render json: ReportAssessmentsChart.new }
    end
  end
  
  def report_errors_for_each_category
    respond_to do |format|
      format.json { render json: ReportErrorsForEachCategoryChart.new }
    end
  end
  
end
