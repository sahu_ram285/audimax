/**
 @preserve CLEditor Table Plugin v1.0.2
 http://premiumsoftware.net/cleditor
 requires CLEditor v1.2.2 or later
 
 Copyright 2010, Chris Landowski, Premium Software, LLC
 Dual licensed under the MIT or GPL Version 2 licenses.
*/
// ==ClosureCompiler==
// @compilation_level SIMPLE_OPTIMIZATIONS
// @output_file_name jquery.cleditor.table.min.js
// ==/ClosureCompiler==
(function(e){function t(t,n){e(n.popup).children(":button").unbind("click").bind("click",function(t){var r=n.editor,i=e(n.popup).find(":text"),s=parseInt(i[0].value),o=parseInt(i[1].value),u;if(s>0&&o>0){u="<table cellpadding=2 cellspacing=2 border=1>";for(y=0;y<o;y++){u+="<tr>";for(x=0;x<s;x++)u+="<td>"+x+","+y+"</td>";u+="</tr>"}u+="</table><br />"}u&&r.execCommand(n.command,u,null,n.button),i.val("4"),r.hidePopups(),r.focus()})}e.cleditor.buttons.table={name:"table",image:"table.gif",title:"Insert Table",command:"inserthtml",popupName:"table",popupClass:"cleditorPrompt",popupContent:"<table cellpadding=0 cellspacing=0><tr><td>Cols:<br><input type=text value=4 size=6></td><td>Rows:<br><input type=text value=4 size=6></td></tr></table><input type=button value=Submit>",buttonClick:t},e.cleditor.defaultOptions.controls=e.cleditor.defaultOptions.controls.replace("rule ","rule table ")})(jQuery);