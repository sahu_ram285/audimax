class CreateCalibrations < ActiveRecord::Migration
  def change
    create_table :calibrations do |t|
      t.integer :equipment_id
      t.date :calibration_date
      t.text :comments

      t.timestamps
    end
  end
end
