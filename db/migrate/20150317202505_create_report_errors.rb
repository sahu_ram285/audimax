class CreateReportErrors < ActiveRecord::Migration
  def change
    create_table :report_errors do |t|
      t.integer :report_review_id
      t.integer :report_error_category_id
      t.text :description
      t.string :old_assessment
      t.string :new_assessment
      t.integer :report_item_id

      t.timestamps
    end
  end
end
