class RemoveMaintenanceSchemeIdFromEquipmentType < ActiveRecord::Migration
  def up
    remove_column :equipment_types, :maintenance_scheme_id
  end

  def down
    add_column :equipment_types, :maintenance_scheme_id, :string
  end
end
