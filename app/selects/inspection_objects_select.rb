class InspectionObjectsSelect
  delegate :params, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      total_items: items.total_entries,
      items: data
    }
  end
  
  private
  
  def data
    items.map do |item|
      {
        id: item.nr,
        name: item.name,
        client_name: item.client.name,
      }
    end
  end
  
  def items
    @items ||= fetch_items
  end

  def fetch_items
    inspection_objects = policy_scope(InspectionObject).order("inspection_objects.nr")
    inspection_objects = inspection_objects.joins(:client).includes(:client)
    
    if params[:query].present?
      inspection_objects = inspection_objects.where("lower(inspection_objects.nr) like concat('%', lower(:query), '%') or lower(inspection_objects.name) like concat('%', lower(:query), '%') or lower(clients.name) like concat('%', lower(:query), '%')", :query => params[:query])
    end

    inspection_objects = inspection_objects.page(page).per_page(per_page)
    inspection_objects
  end
  
  def page
    params[:page].to_i > 0 ? params[:page].to_i : 1 
  end

  def per_page
    params[:items_per_page].to_i > 0 ? params[:items_per_page].to_i : 10
  end
  
end
