class ReportsDatatable
  delegate :params, :h, :link_to, :number_to_currency, :raw, :edit_report_path, :report_path, :report_status_label, :button_to, :current_user, :policy, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(Report).count,
      iTotalDisplayRecords: reports.total_entries,
      aaData: data
    }
  end

private

  def data
    reports.map do |report|
      [
        report.id,
        report_status_label(report.status),
        report.inspection_object.nr,
        report.inspection_object.client.name,
        report.inspection_date,
        report.scope.program.name,
        report.scope.name,
        report.employee.fullname,
				(report.status == "FILED" and !current_user.employee_id.nil? and policy(ReportReview).new?) ? (button_to("Review", { :controller => 'report_reviews', :action => 'create', :method => :post, :report_id => report.id}, :class => 'btn btn-mini btn-warning')) : nil,
				'<span class="btn-group">'+
          link_to(raw("<i class='icon-search'></i>"), report, { :class => 'btn btn-small' })+
          (policy(report).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_report_path(report), { :class => 'btn btn-small' }) : "")+
          (policy(report).destroy? ? link_to(raw("<i class='icon-trash'></i>"), report, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
          '</span>'+
					'<span class="btn-group">'+
					link_to(raw("<i class='icon-file-pdf'></i>"), report_path(report, :format => :pdf), { :target => "_blank", :class => 'btn btn-small' })+
					'</span>'
      ]
    end
  end

  def reports
    @reports ||= fetch_reports
  end

  def fetch_reports
    reports = policy_scope(Report).order("#{sort_column} #{sort_direction}")
    reports = reports.joins(inspection_object: :client).joins(:employee).joins(scope: :program).includes(inspection_object: :client).includes(:employee).includes(scope: :program)
    
    if params[:id].present?
      reports = reports.where("reports.id = :id", id: params[:id])
    end
    if params[:object_nr].present?
      reports = reports.where("inspection_objects.nr = :object_nr", :object_nr => params[:object_nr])
    end
    if params[:employee_id].present?
      reports = reports.where("reports.employee_id = :employee_id", :employee_id => params[:employee_id])
    end
    if params[:client_id].present?
      reports = reports.where("clients.id = :client_id", :client_id => params[:client_id])
    end

    reports = reports.page(page).per_page(per_page)
    reports
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[reports.id reports.status inspection_object.object_nr clients.name inspection_date programs.name scope.name employee.lastname]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end