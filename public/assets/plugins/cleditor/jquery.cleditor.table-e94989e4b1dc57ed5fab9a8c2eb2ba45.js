/**
 @preserve CLEditor Table Plugin v1.0.2
 http://premiumsoftware.net/cleditor
 requires CLEditor v1.2.2 or later
 
 Copyright 2010, Chris Landowski, Premium Software, LLC
 Dual licensed under the MIT or GPL Version 2 licenses.
*/
!function(t){function e(e,l){t(l.popup).children(":button").unbind("click").bind("click",function(){var e,n=l.editor,o=t(l.popup).find(":text"),a=parseInt(o[0].value),i=parseInt(o[1].value);if(a>0&&i>0){for(e="<table cellpadding=2 cellspacing=2 border=1>",y=0;i>y;y++){for(e+="<tr>",x=0;a>x;x++)e+="<td>"+x+","+y+"</td>";e+="</tr>"}e+="</table><br />"}e&&n.execCommand(l.command,e,null,l.button),o.val("4"),n.hidePopups(),n.focus()})}t.cleditor.buttons.table={name:"table",image:"table.gif",title:"Insert Table",command:"inserthtml",popupName:"table",popupClass:"cleditorPrompt",popupContent:"<table cellpadding=0 cellspacing=0><tr><td>Cols:<br><input type=text value=4 size=6></td><td>Rows:<br><input type=text value=4 size=6></td></tr></table><input type=button value=Submit>",buttonClick:e},t.cleditor.defaultOptions.controls=t.cleditor.defaultOptions.controls.replace("rule ","rule table ")}(jQuery);