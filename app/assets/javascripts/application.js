// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery-migrate-min
//= require jquery_ujs
//= require jquery.ui.all
//= require best_in_place
//= require best_in_place.jquery-ui
//= require jquery-fileupload
//= require jui/jquery-ui.custom
//= require jui/js/jquery.ui.touch-punch
//= require jui/js/globalize/globalize
//= require jui/js/globalize/cultures/globalize.culture.en-US
//= require plugins/autosize/jquery.autosize
//= require plugins/datatables/jquery.dataTables
//= require plugins/flot/jquery.flot
//= require plugins/flot/jquery.flot.canvas
//= require plugins/flot/jquery.flot.categories
//= require plugins/flot/jquery.flot.crosshair
//= require plugins/flot/jquery.flot.errorbars
//= require plugins/flot/jquery.flot.fillbetween
//= require plugins/flot/jquery.flot.image
//= require plugins/flot/jquery.flot.navigate
//= require plugins/flot/jquery.flot.pie
//= require plugins/flot/jquery.flot.resize
//= require plugins/flot/jquery.flot.selection
//= require plugins/flot/jquery.flot.stack
//= require plugins/flot/jquery.flot.symbol
//= require plugins/flot/jquery.flot.threshold
//= require plugins/flot/jquery.flot.time
//= require plugins/flot/jquery.flot.orderBars
//= require plugins/ibutton/jquery.ibutton
//= require plugins/select2/select2
//= require plugins/validate/jquery.validate
//= require plugins/plupload/plupload
//= require plugins/plupload/plupload.flash
//= require plugins/plupload/plupload.html4
//= require plugins/plupload/plupload.html5
//= require plugins/plupload/jquery.plupload.queue/jquery.plupload.queue
//= require plugins/plupload/i18n/nl
//= require plugins/prettyphoto/js/jquery.prettyPhoto
//= require custom-plugins/fileinput
//= require custom-plugins/picklist/picklist
//= require js/libs/jquery.mousewheel
//= require js/libs/jquery.placeholder
//= require bootstrap/js/bootstrap
//= require bootstrap-select
//= require i18n/translations
//= require js/core/mws
//= require tinymce-jquery
//= require_tree .
