class Scope < ActiveRecord::Base
  attr_accessible :name, :program_id, :visiting_frequency
  
  belongs_to :program
  has_and_belongs_to_many :sections
  has_many :participations, :dependent => :restrict
  has_many :inspection_objects, :through => :participations
  has_many :reports, :dependent => :restrict
  
  validates :program_id, :name, :presence => true
  validates :visiting_frequency, :allow_nil => true, :numericality => { :greater_than => 0 }
end
