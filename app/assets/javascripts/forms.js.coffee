$ ->
	# Datepicker
	if $.fn.datepicker
		$(".mws-datepicker").datepicker dateFormat: 'dd-mm-yy', showOtherMonths: true
		
	# iButton
	if $.fn.iButton
		$(".ibutton").iButton()
		
	# Select2
	if $.fn.select2
		$("select.mws-select2").select2
			placeholder: '',
			allowClear: true,
			formatNoMatches: ->
				I18n.t 'js.select2.no_matches'
			formatLoadMore: (pageNumber) -> 
				I18n.t('js.select2.load_more')
			formatInputTooShort: (input, min) -> 
				nrCharacters = min-input.length
				if nrCharacters == 1
					return I18n.t 'js.select2.input_too_short_1'
				else
					return I18n.t 'js.select2.input_too_short', {count: nrCharacters}
			formatSearching: ->
				I18n.t 'js.select2.searching'

	# Autosize
	if $.fn.autosize
		$(".autosize").autosize()
			
	# Picklist
	if $.fn.pickList
		$(".pickList").pickList enableFilters: false, enableCounters: false, sourceListLabel: I18n.t('js.picklist.source_list_label'), targetListLabel: I18n.t('js.picklist.target_list_label')
		
	# SelectPicker
	if $.fn.selectpicker
		$(".selectpicker").selectpicker noneSelectedText: 'Nieuwe beoordeling'
		
	if $.fn.tabs
		$(".mws-tabs").tabs()
		
	$(".best_in_place").best_in_place();
		
	# Datatable
	if $.fn.dataTable
		dataTablesTexts =
			sProcessing: I18n.t('js.datatables.processing')
			sLengthMenu: I18n.t('js.datatables.length_menu')
			sZeroRecords: I18n.t('js.datatables.zero_records')
			sInfo: I18n.t('js.datatables.info')
			sInfoEmpty: I18n.t('js.datatables.info_empty')
			sInfoFiltered: I18n.t('js.datatables.info_filtered')
			sInfoPostFix: ""
			sSearch: I18n.t('js.datatables.search')
			sEmptyTable: I18n.t('js.datatables.empty_table')
			sInfoThousands:	I18n.t('js.datatables.info_thousands')
			sLoadingRecords: I18n.t('js.datatables.loading_records')
			oPaginate:
				sFirst: I18n.t('js.datatables.paginate_first')
				sLast: I18n.t('js.datatables.paginate_last')
				sNext: I18n.t('js.datatables.paginate_next')
				sPrevious: I18n.t('js.datatables.paginate_previous')
				
		$(".mws-datatable").dataTable
			bServerSide: true
			sAjaxSource: $('.mws-datatable').data 'source'
			oLanguage: dataTablesTexts
			sDom: 't<"bottom"lipr>'
			bAutoWidth: false
			aoColumnDefs : [
				{ bSortable : false, aTargets : [ "no-sort" ] }
				{ sClass: "center", aTargets: [ "center" ] }
			]
			aaSorting: [[ $('.mws-datatable').data('sort-column'), $('.mws-datatable').data('sort-direction') ]]
			
	# Charts
	if $.plot
		$(".am-chart").each ->
			element = $(this)
			$.ajax
				url: $(this).data 'source'
				type: "GET"
				dataType: "json"
				success: (chart) ->
					$.plot element, chart.data, chart.options

	# Clear form
	$(".clear-form").click ->
		form = $(this).closest('form')
		form.find("input[type=text]").val('')
		form.children('.mws-select2').select2('data', null)
		form.children('.mws-select-ajax').select2('data', null)
		false
					
	# Dialogs
	$.rails.allowAction = (link) ->
		return true unless link.attr('data-confirm')
		$.rails.showConfirmDialog(link) # look bellow for implementations
		false # always stops the action since code runs asynchronously
 
	$.rails.confirmed = (link) ->
		link.removeAttr('data-confirm')
		link.trigger('click.rails')
  
	$.rails.showConfirmDialog = (link) ->
		message = link.attr 'data-confirm'
		title = I18n.t('js.confirm_dialog.title')
		html = """
			<div id="dialog-confirm" title="#{title}">
				<p>#{message}</p>
			</div>
			"""
		$(html).dialog
			resizable: false
			modal: true
			buttons:
				OK: ->
					$.rails.confirmed link
					$(this).dialog "close"
				Cancel: ->
					$(this).dialog "close"

