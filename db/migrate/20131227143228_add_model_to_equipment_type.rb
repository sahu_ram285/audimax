class AddModelToEquipmentType < ActiveRecord::Migration
  def change
    add_column :equipment_types, :model, :string
  end
end
