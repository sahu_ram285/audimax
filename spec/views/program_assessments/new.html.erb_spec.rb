require 'spec_helper'

describe "program_assessments/new" do
  before(:each) do
    assign(:program_assessment, stub_model(ProgramAssessment,
      :code => "MyString",
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new program_assessment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", program_assessments_path, "post" do
      assert_select "input#program_assessment_code[name=?]", "program_assessment[code]"
      assert_select "input#program_assessment_name[name=?]", "program_assessment[name]"
    end
  end
end
