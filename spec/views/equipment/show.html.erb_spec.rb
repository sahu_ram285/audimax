require 'spec_helper'

describe "equipment/show" do
  before(:each) do
    @equipment = assign(:equipment, stub_model(Equipment,
      :equipment_type => "Equipment Type",
      :serial_nr => "Serial Nr"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Equipment Type/)
    rendered.should match(/Serial Nr/)
  end
end
