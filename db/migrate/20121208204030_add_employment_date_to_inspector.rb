class AddEmploymentDateToInspector < ActiveRecord::Migration
  def change
    add_column :inspectors, :employment_date, :date
  end
end
