class NotificationsController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
   
  # GET /notifications
  # GET /notifications.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: NotificationsDatatable.new(view_context) }
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    # Notifications are not actually deleted. We keep them to check if a notification has already been sent.
    # They are marked as deleted to hide them from the user.
    @notification = Notification.find(params[:id])
    authorize @notification
    @notification.deleted = true
    @notification.save!

    respond_to do |format|
      format.html { redirect_to notifications_url }
      format.json { head :no_content }
    end
  end
  
  def toggle_read
    @notification = Notification.find(params[:id])
    authorize @notification, :update?
    @notification.toggle_read
    
    respond_to do |format|
      if @notification.save
        format.html { redirect_to notifications_path }
        format.json { head :no_content }
      end
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end 

end
