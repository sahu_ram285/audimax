/*!
 * jQuery UI Google Map 3.0-beta
 * http://code.google.com/p/jquery-ui-map/
 * Copyright (c) 2010 - 2011 Johan Säll Larsson
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *		jquery.ui.map.js
 */
!function(t){t.extend(t.ui.gmap.prototype,{rdfa:function(e,r){var n=this;t('[typeof="{0}"]'.replace("{0}",e)).each(function(e){r(n._traverse(t(this),{"@type":n._resolveType(t(this).attr("typeof"))}),this,e)})},_traverse:function(e,r){var n=this;return e.children().each(function(){var e=t(this),a=n._resolveType(e.attr("typeof")),s=n._resolveType(e.attr("rel")),i=n._resolveType(e.attr("property"));a||s||i?(s&&(e.children().length>0?(r[s]=[],n._traverse(e,r[s])):r[s]=n._extract(e,!0)),a&&(r.push({"@type":a}),n._traverse(e,r[r.length-1])),i&&(r[i]?(r[i]=[r[i]],r[i].push(n._extract(e,!1))):r[i]=n._extract(e,!1))):n._traverse(e,r)}),r},_extract:function(t,e){if(e){if(t.attr("src"))return t.attr("src");if(t.attr("href"))return t.attr("href")}return t.attr("content")?t.attr("content"):t.text()?t.text():void 0},_resolveType:function(t){return t&&(t.indexOf("http")>-1?t=t.substr(t.lastIndexOf("/")+1).replace("?","").replace("#",""):t.indexOf(":")>-1&&(t=t.split(":")[1])),t}})}(jQuery);