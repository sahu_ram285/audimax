/**
 @preserve CLEditor XHTML Plugin v1.0.0
 http://premiumsoftware.net/cleditor
 requires CLEditor v1.3.0 or later
 
 Copyright 2010, Chris Landowski, Premium Software, LLC
 Dual licensed under the MIT or GPL Version 2 licenses.

 Based on John Resig's HTML Parser Project (ejohn.org)
 http://ejohn.org/files/htmlparser.js
 Original code by Erik Arvidsson, Mozilla Public License
 http://erik.eae.net/simplehtmlparser/simplehtmlparser.js
*/
// ==ClosureCompiler==
// @compilation_level SIMPLE_OPTIMIZATIONS
// @output_file_name jquery.cleditor.xhtml.min.js
// ==/ClosureCompiler==
(function(e){var t=e.cleditor.defaultOptions.updateTextArea;e.cleditor.defaultOptions.updateTextArea=function(n){return t&&(n=t(n)),e.cleditor.convertHTMLtoXHTML(n)},e.cleditor.convertHTMLtoXHTML=function(e){function v(e){var t={},n=e.split(",");for(var r=0;r<n.length;r++)t[n[r]]=!0;return t}function m(e,t,n,f){t=t.toLowerCase();if(s[t])while(l.last()&&o[l.last()])g("",l.last());u[t]&&l.last()==t&&g("",t),f=i[t]||!!f,f||l.push(t);var c=[];n.replace(r,function(e,t){var n=arguments[2]?arguments[2]:arguments[3]?arguments[3]:arguments[4]?arguments[4]:a[t]?t:"";c.push({name:t,escaped:n.replace(/(^|[^\\])"/g,'$1\\"')})}),d+="<"+t;for(var h=0;h<c.length;h++)d+=" "+c[h].name+'="'+c[h].escaped+'"';d+=(f?"/":"")+">"}function g(e,t){if(!t)var n=0;else{t=t.toLowerCase();for(var n=l.length-1;n>=0;n--)if(l[n]==t)break}if(n>=0){for(var r=l.length-1;r>=n;r--)d+="</"+l[r]+">";l.length=n}}function y(e,t){d=d.replace(e,t)}var t=/^<(\w+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/,n=/^<\/(\w+)[^>]*>/,r=/(\w+)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g,i=v("area,base,basefont,br,col,frame,hr,img,input,isindex,link,meta,param,embed"),s=v("address,applet,blockquote,button,center,dd,del,dir,div,dl,dt,fieldset,form,frameset,hr,iframe,ins,isindex,li,map,menu,noframes,noscript,object,ol,p,pre,script,table,tbody,td,tfoot,th,thead,tr,ul"),o=v("a,abbr,acronym,applet,b,basefont,bdo,big,br,button,cite,code,del,dfn,em,font,i,iframe,img,input,ins,kbd,label,map,object,q,s,samp,script,select,small,span,strike,strong,sub,sup,textarea,tt,u,var"),u=v("colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr"),a=v("checked,compact,declare,defer,disabled,ismap,multiple,nohref,noresize,noshade,nowrap,readonly,selected"),f=v("script,style"),l=[];l.last=function(){return this[this.length-1]};var c,h,p=e,d="";while(e){!l.last()||!f[l.last()]?e.indexOf("<!--")==0?(c=e.indexOf("-->"),c>=0&&(d+=e.substring(0,c+3),e=e.substring(c+3))):e.indexOf("</")==0?(h=e.match(n),h&&(e=e.substring(h[0].length),h[0].replace(n,g))):e.indexOf("<")==0?(h=e.match(t),h&&(e=e.substring(h[0].length),h[0].replace(t,m))):(c=e.indexOf("<"),d+=c<0?e:e.substring(0,c),e=c<0?"":e.substring(c)):(e=e.replace(new RegExp("(.*)</"+l.last()+"[^>]*>"),function(e,t){return t=t.replace(/<!--(.*?)-->/g,"$1").replace(/<!\[CDATA\[(.*?)]]>/g,"$1"),d+=t,""}),g("",l.last()));if(e==p)throw"Parse Error: "+e;p=e}return g(),y(/<b>(.*?)<\/b>/g,"<strong>$1</strong>"),y(/<i>(.*?)<\/i>/g,"<em>$1</em>"),d}})(jQuery);