class RemoveReportReviewItemIdFromReportItemError < ActiveRecord::Migration
  def up
    remove_column :report_item_errors, :report_review_item_id
  end

  def down
    add_column :report_item_errors, :report_review_item_id, :integer
  end
end
