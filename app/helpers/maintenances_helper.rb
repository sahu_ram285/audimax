module MaintenancesHelper
  
  def calibration_approved_span(calibration_approved)
    label_class = calibration_approved == 'Y' ? 'label label-success' : 'label label-important'
    "<span class='#{label_class}'>"+Maintenance.human_attribute_name("calibration_approved.#{calibration_approved.downcase}")+"</span>"
  end
  
end
