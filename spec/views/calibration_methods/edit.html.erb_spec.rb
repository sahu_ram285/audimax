require 'spec_helper'

describe "calibration_methods/edit" do
  before(:each) do
    @calibration_method = assign(:calibration_method, stub_model(CalibrationMethod,
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit calibration_method form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", calibration_method_path(@calibration_method), "post" do
      assert_select "input#calibration_method_name[name=?]", "calibration_method[name]"
      assert_select "textarea#calibration_method_description[name=?]", "calibration_method[description]"
    end
  end
end
