require 'spec_helper'

describe "employees/show" do
  before(:each) do
    @employee = assign(:employee, stub_model(Employee,
      :firstname => "Firstname",
      :lastname => "Lastname",
      :gender => "Gender",
      :address => "Address",
      :postcode => "Postcode",
      :city => "City",
      :state => "State",
      :country => "Country",
      :email => "Email",
      :phone1 => "Phone1",
      :phone2 => "Phone2",
      :position_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Firstname/)
    rendered.should match(/Lastname/)
    rendered.should match(/Gender/)
    rendered.should match(/Address/)
    rendered.should match(/Postcode/)
    rendered.should match(/City/)
    rendered.should match(/State/)
    rendered.should match(/Country/)
    rendered.should match(/Email/)
    rendered.should match(/Phone1/)
    rendered.should match(/Phone2/)
    rendered.should match(/1/)
  end
end
