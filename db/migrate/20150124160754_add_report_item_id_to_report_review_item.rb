class AddReportItemIdToReportReviewItem < ActiveRecord::Migration
  def change
    add_column :report_review_items, :report_item_id, :integer
  end
end
