class ComplaintsPerTypeChart
  
  def assessment_name(assessment)
    Assessment.where(:code => assessment).first.name
  end
  
  def series_data
    Complaint.where("received_date > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)").group('complaint_type').select('complaint_type, count(*) nr_complaints').all.map { |a| {label: Complaint.complaint_type_description(a.complaint_type), data: a.nr_complaints} }
  end
  
  def as_json(options = {})
    { data: series_data,
      options: {
    		series: {
    			pie: {
    				show: true
    			}
    		},
    		legend: {
    			show: false,
    		}  
      }
    }
  end
  
end
