require 'spec_helper'

describe "complaints/new" do
  before(:each) do
    assign(:complaint, stub_model(Complaint,
      :status => "MyString",
      :priority => "MyString",
      :complaint_type => "MyString",
      :complainant_name => "MyString",
      :complainant_address => "MyString",
      :complainant_postalcode => "MyString",
      :complainant_city => "MyString",
      :complainant_state => "MyString",
      :complainant_country => "MyString",
      :complainant_phone1 => "MyString",
      :complainant_phone2 => "MyString",
      :complainant_email => "MyString",
      :complainant_employee_id => 1,
      :complainant_client_id => 1,
      :notes => "MyText",
      :assigned_to_id => 1,
      :owner_id => 1,
      :inspection_object_id => 1,
      :client_id => 1,
      :employee_id => 1,
      :report_id => 1,
      :planned_handled_date => "",
      :complaint => "MyText",
      :cause => "MyText",
      :impact => "MyText",
      :resolution => "MyText"
    ).as_new_record)
  end

  it "renders new complaint form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", complaints_path, "post" do
      assert_select "input#complaint_status[name=?]", "complaint[status]"
      assert_select "input#complaint_priority[name=?]", "complaint[priority]"
      assert_select "input#complaint_complaint_type[name=?]", "complaint[complaint_type]"
      assert_select "input#complaint_complainant_name[name=?]", "complaint[complainant_name]"
      assert_select "input#complaint_complainant_address[name=?]", "complaint[complainant_address]"
      assert_select "input#complaint_complainant_postalcode[name=?]", "complaint[complainant_postalcode]"
      assert_select "input#complaint_complainant_city[name=?]", "complaint[complainant_city]"
      assert_select "input#complaint_complainant_state[name=?]", "complaint[complainant_state]"
      assert_select "input#complaint_complainant_country[name=?]", "complaint[complainant_country]"
      assert_select "input#complaint_complainant_phone1[name=?]", "complaint[complainant_phone1]"
      assert_select "input#complaint_complainant_phone2[name=?]", "complaint[complainant_phone2]"
      assert_select "input#complaint_complainant_email[name=?]", "complaint[complainant_email]"
      assert_select "input#complaint_complainant_employee_id[name=?]", "complaint[complainant_employee_id]"
      assert_select "input#complaint_complainant_client_id[name=?]", "complaint[complainant_client_id]"
      assert_select "textarea#complaint_notes[name=?]", "complaint[notes]"
      assert_select "input#complaint_assigned_to_id[name=?]", "complaint[assigned_to_id]"
      assert_select "input#complaint_owner_id[name=?]", "complaint[owner_id]"
      assert_select "input#complaint_inspection_object_id[name=?]", "complaint[inspection_object_id]"
      assert_select "input#complaint_client_id[name=?]", "complaint[client_id]"
      assert_select "input#complaint_employee_id[name=?]", "complaint[employee_id]"
      assert_select "input#complaint_report_id[name=?]", "complaint[report_id]"
      assert_select "input#complaint_planned_handled_date[name=?]", "complaint[planned_handled_date]"
      assert_select "textarea#complaint_complaint[name=?]", "complaint[complaint]"
      assert_select "textarea#complaint_cause[name=?]", "complaint[cause]"
      assert_select "textarea#complaint_impact[name=?]", "complaint[impact]"
      assert_select "textarea#complaint_resolution[name=?]", "complaint[resolution]"
    end
  end
end
