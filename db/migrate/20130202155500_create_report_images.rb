class CreateReportImages < ActiveRecord::Migration
  def change
    create_table :report_images do |t|
      t.string :image
      t.text :description
      t.integer :report_id

      t.timestamps
    end
  end
end
