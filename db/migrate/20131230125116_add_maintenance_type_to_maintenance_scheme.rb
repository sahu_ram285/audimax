class AddMaintenanceTypeToMaintenanceScheme < ActiveRecord::Migration
  def change
    add_column :maintenance_schemes, :maintenance_type, :string
  end
end
