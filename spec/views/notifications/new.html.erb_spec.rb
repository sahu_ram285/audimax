require 'spec_helper'

describe "notifications/new" do
  before(:each) do
    assign(:notification, stub_model(Notification,
      :employee_id => 1,
      :text => "MyText",
      :read => false
    ).as_new_record)
  end

  it "renders new notification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", notifications_path, "post" do
      assert_select "input#notification_employee_id[name=?]", "notification[employee_id]"
      assert_select "textarea#notification_text[name=?]", "notification[text]"
      assert_select "input#notification_read[name=?]", "notification[read]"
    end
  end
end
