class Report < ActiveRecord::Base
  attr_accessible :comments, :employee_id, :inspection_date, :inspection_object_nr, :inspection_object_id, :report_items_attributes, :scope_id, :status
  
  belongs_to :inspection_object
  belongs_to :employee
  belongs_to :scope
  belongs_to :regulation
  has_many :report_items, :dependent => :destroy
  
  has_many :report_equipments, :dependent => :destroy
  has_many :equipment, :through => :report_equipments

  accepts_nested_attributes_for :report_items

  validates :employee_id, :inspection_object_id, :scope_id, :inspection_date, :presence => true
  
  def self.report_status_values
    [ 'NEW', 'FILED', 'REVIEW', 'FINAL']
  end
  
  def self.report_status_description(status)
    Report.human_attribute_name("status.#{status.downcase}")
  end
  
  def self.report_valid_status_transitions(status)
    { 'NEW' => ['NEW', 'FILED'],
      'FILED' => ['FILED', 'REVIEW', 'FINAL'], 
      'REVIEW' => ['FILED', 'REVIEW', 'FINAL'],
      'FINAL' => ['FINAL']
      }[status]
  end
  
  def self.create_report(object_nr, employee_id, inspection_date, scope_id)
    new_report = Report.new(:inspection_object_nr => object_nr, :employee_id => employee_id, :inspection_date => inspection_date, :scope_id => scope_id)
    
    if new_report.valid?
      new_report.status = 'NEW'
      new_report.regulation = new_report.scope.program.regulations.valid_on(new_report.inspection_date).first

      # Create report items from regulation
      if !new_report.regulation.nil?
        new_report.regulation.sections.for_scope(scope_id).each do |section|
          new_report.report_items.build(:section_id => section.id, :assessment => 'NONE')
        end
      end
    
      # Create report equipment from employee equipment
      new_report.employee.equipment.where(:usage => 'OPERATIONAL').each do |equipment|
        new_report.report_equipments.build(:equipment_id => equipment.id)
      end
    end

    new_report
  end
  
  #
  # Report status
  #
  # NEW     - A new report. Not yet filed, changes are still possible.
  # FILED   - Report is filed. Changes are nog longer possible. Report is ready for review.
  # REVIEW  - Report is being reviewed.
  # READY   - Report is ready. Review is done. Review is approved and changes in report have been applied. This is the final status.
  #
  
  #
  # Virtual attribute inspection_object_nr
  #
  
  def inspection_object_nr
  	inspection_object.nr if inspection_object
  end
  
  def inspection_object_nr=(nr)
  	self.inspection_object = InspectionObject.find_by_nr(nr) unless nr.blank?
  end
  
  def accreditation_logo
    accreditation = self.employee.company.accreditations.where("accreditations.scope_id = :scope_id and (:date >= accreditations.start_date and (:date <= accreditations.end_date or accreditations.end_date is null))", :scope_id => self.scope_id, :date => self.inspection_date).first
    accreditation.nil? ? "" : accreditation.image_url.to_s
  end

end
