# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150418083219) do

  create_table "accreditations", :force => true do |t|
    t.integer  "company_id"
    t.integer  "scope_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "image"
    t.date     "start_date"
    t.date     "end_date"
  end

  create_table "assessments", :force => true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "calibration_method_manuals", :force => true do |t|
    t.integer  "calibration_method_id"
    t.string   "manual"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "calibration_methods", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "client_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "client_id"
  end

  add_index "client_users", ["email"], :name => "index_client_users_on_email", :unique => true
  add_index "client_users", ["reset_password_token"], :name => "index_client_users_on_reset_password_token", :unique => true

  create_table "clients", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "postcode"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.string   "website"
    t.string   "pos_address"
    t.string   "pos_postcode"
    t.string   "pos_city"
    t.string   "pos_state"
    t.string   "pos_country"
    t.string   "inv_address"
    t.string   "inv_postcode"
    t.string   "inv_city"
    t.string   "inv_state"
    t.string   "inv_country"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "image"
  end

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "postalcode"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "country"
    t.string   "image"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.string   "web"
  end

  create_table "complaint_actions", :force => true do |t|
    t.integer  "complaint_id"
    t.integer  "employee_id"
    t.text     "action"
    t.string   "complaint_status_old"
    t.string   "complaint_status_new"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "user_id"
  end

  create_table "complaints", :force => true do |t|
    t.string   "status"
    t.string   "priority"
    t.string   "complaint_type"
    t.date     "received_date"
    t.string   "complainant_name"
    t.string   "complainant_address"
    t.string   "complainant_postalcode"
    t.string   "complainant_city"
    t.string   "complainant_state"
    t.string   "complainant_country"
    t.string   "complainant_phone1"
    t.string   "complainant_phone2"
    t.string   "complainant_email"
    t.integer  "complainant_employee_id"
    t.integer  "complainant_client_id"
    t.integer  "assigned_to_id"
    t.integer  "owner_id"
    t.integer  "inspection_object_id"
    t.integer  "client_id"
    t.integer  "employee_id"
    t.integer  "report_id"
    t.date     "planned_handled_date"
    t.date     "handled_date"
    t.text     "complaint"
    t.text     "cause"
    t.text     "impact"
    t.text     "resolution"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "employees", :force => true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.date     "birthdate"
    t.string   "address"
    t.string   "postcode"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "email"
    t.string   "phone1"
    t.string   "phone2"
    t.integer  "company_id"
    t.date     "employment_date"
    t.integer  "position_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "image"
    t.text     "notes"
    t.date     "employment_end_date"
  end

  create_table "equipment", :force => true do |t|
    t.string   "serial_nr"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "usage"
    t.date     "in_service_date"
    t.date     "out_of_service_date"
    t.integer  "equipment_type_id"
    t.string   "nr"
    t.integer  "employee_id"
  end

  add_index "equipment", ["employee_id"], :name => "index_equipment_on_employee_id"

  create_table "equipment_types", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "image"
    t.string   "manufacturer"
    t.string   "model"
    t.string   "manual"
  end

  create_table "equipment_types_maintenance_schemes", :id => false, :force => true do |t|
    t.integer "equipment_type_id"
    t.integer "maintenance_scheme_id"
  end

  create_table "inspection_objects", :force => true do |t|
    t.string   "nr"
    t.string   "name"
    t.text     "description"
    t.string   "address"
    t.string   "postalcode"
    t.string   "city"
    t.string   "state"
    t.string   "email"
    t.string   "website"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.float    "latitude"
    t.float    "longitude"
    t.string   "country"
    t.integer  "client_id"
  end

  create_table "maintenance_schemes", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "maintenance_type"
    t.integer  "interval"
    t.integer  "warn_before"
  end

  create_table "maintenances", :force => true do |t|
    t.integer  "equipment_id"
    t.string   "maintenance_type"
    t.date     "maintenance_date"
    t.integer  "employee_id"
    t.float    "calibration_result"
    t.string   "calibration_approved"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "calibration_method_id"
    t.string   "calibration_certificate"
  end

  add_index "maintenances", ["equipment_id"], :name => "index_maintenances_on_equipment_id"

  create_table "notifications", :force => true do |t|
    t.text     "message"
    t.boolean  "read"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.boolean  "deleted"
  end

  create_table "object_attachments", :force => true do |t|
    t.integer  "inspection_object_id"
    t.string   "attachment"
    t.text     "description"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "object_attachments", ["inspection_object_id"], :name => "index_object_attachments_on_inspection_object_id"

  create_table "object_parameters", :force => true do |t|
    t.integer  "inspection_object_id"
    t.string   "name"
    t.string   "value"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "participations", :force => true do |t|
    t.integer  "inspection_object_id"
    t.integer  "scope_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "positions", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "program_assessments", :force => true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "program_id"
  end

  create_table "programs", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "image"
  end

  create_table "regulations", :force => true do |t|
    t.integer  "program_id"
    t.string   "title"
    t.string   "version"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "status"
  end

  create_table "report_equipments", :force => true do |t|
    t.integer  "report_id"
    t.integer  "equipment_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "report_error_categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.boolean  "requires_assessment"
  end

  create_table "report_errors", :force => true do |t|
    t.integer  "report_review_id"
    t.integer  "report_error_category_id"
    t.text     "description"
    t.string   "old_assessment"
    t.string   "new_assessment"
    t.integer  "report_item_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "report_item_attachments", :force => true do |t|
    t.integer  "report_item_id"
    t.string   "attachment"
    t.text     "description"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "report_items", :force => true do |t|
    t.integer  "report_id"
    t.integer  "section_id"
    t.string   "assessment"
    t.text     "comments"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "report_items", ["report_id"], :name => "index_report_items_on_report_id"

  create_table "report_reviews", :force => true do |t|
    t.string   "status"
    t.integer  "reviewer_id"
    t.text     "comments"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "report_id"
  end

  create_table "reports", :force => true do |t|
    t.integer  "inspection_object_id"
    t.date     "inspection_date"
    t.integer  "scope_id"
    t.integer  "regulation_id"
    t.text     "comments"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "status"
    t.integer  "employee_id"
  end

  add_index "reports", ["inspection_date"], :name => "index_reports_on_inspection_date"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "scopes", :force => true do |t|
    t.integer  "program_id"
    t.string   "name"
    t.decimal  "visiting_frequency", :precision => 10, :scale => 0
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
  end

  create_table "scopes_sections", :id => false, :force => true do |t|
    t.integer "scope_id"
    t.integer "section_id"
  end

  create_table "sections", :force => true do |t|
    t.integer  "regulation_id"
    t.text     "text"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "nr"
    t.text     "instruction"
    t.string   "title"
    t.integer  "seq_nr"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "image"
    t.integer  "employee_id"
    t.string   "two_step_verification"
    t.boolean  "admin",                  :default => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
