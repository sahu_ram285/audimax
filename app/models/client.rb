class Client < ActiveRecord::Base
  attr_accessible :address, :city, :country, :email, :fax, :image, :inv_address, :inv_city, :inv_country, :inv_postcode, :inv_state, :name, :phone, :pos_address, :pos_city, :pos_country, :pos_postcode, :pos_state, :postcode, :remove_image, :state, :website
  
  has_many :inspection_objects
  
  mount_uploader :image, ImageUploader
  
  def self.search(name)
    clients = scoped
    clients = clients.where("name like :name", :name => "%#{name}%" ) if name.present?
    clients
  end
  
end
