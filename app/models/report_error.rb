class ReportError < ActiveRecord::Base
  attr_accessible :description, :new_assessment, :old_assessment, :report_error_category_id, :report_item_id, :report_review_id
  
  belongs_to :report_review
  belongs_to :report_item
  belongs_to :report_error_category
  
  scope :last_12_months, -> { joins(:report_review => { :report => :employee}).where("report_reviews.created_at > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)") }
end
