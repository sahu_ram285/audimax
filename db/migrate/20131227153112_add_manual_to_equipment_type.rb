class AddManualToEquipmentType < ActiveRecord::Migration
  def change
    add_column :equipment_types, :manual, :string
  end
end
