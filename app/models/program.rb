class Program < ActiveRecord::Base
  attr_accessible :description, :name, :image, :remove_image
  
  has_many :regulations, :order => 'start_date desc', :dependent => :destroy
  has_many :scopes, :order => 'name', :dependent => :destroy
  has_many :program_assessments, :dependent => :destroy
  
  validates :name, :presence => true
  mount_uploader :image, ImageUploader
  
  def nr_reports
    Report.joins(regulation: :program).where("programs.id = ?", self.id).count
  end
  
  def nr_objects
    Participation.joins(scope: :program).where('programs.id = ? and start_date <= ? and (end_date is null or end_date >= ?)', self.id, Date.today, Date.today).count(:inspection_object_id, :distinct => true)
  end
  
  def last_participation_date
    last_participation_date = Participation.joins(scope: :program).where('programs.id = ?', self.id).select('max(participations.created_at) as created_at').first.created_at
    last_participation_date.nil? ? nil : last_participation_date.to_date
  end
  
  def create_assessments
    Assessment.all.each do |assessment|
      self.program_assessments.build(:code => assessment.code)
    end
  end
  
end
