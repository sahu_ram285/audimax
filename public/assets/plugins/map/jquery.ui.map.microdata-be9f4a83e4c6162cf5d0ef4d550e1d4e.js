/*!
 * jQuery UI Google Map 3.0-beta
 * http://code.google.com/p/jquery-ui-map/
 * Copyright (c) 2010 - 2011 Johan Säll Larsson
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *		jquery.ui.map.js
 */
(function(e){e.extend(e.ui.gmap.prototype,{microdata:function(t,n){var r=this;e('[itemtype="{0}"]'.replace("{0}",t)).each(function(t){n(r._traverse(e(this),{"@type":r._resolveType(e(this).attr("itemtype"))}),this,t)})},_traverse:function(t,n){var r=this;return t.children().each(function(){var t=e(this),i=t.attr("itemtype"),s=t.attr("itemProp");if(i!=undefined&&t.children().length>0)n[s]||(n[s]=[]),n[s].push({"@type":r._resolveType(i)}),r._traverse(t,n[s][n[s].length-1]);else if(s)if(n[s]){if(typeof n[s]=="string"){var o=n[s];n[s]=[],n[s].push(o)}n[s].push(r._extract(t))}else n[s]=r._extract(t);else r._traverse(t,n)}),n},_extract:function(e){if(e.attr("src"))return e.attr("src");if(e.attr("href"))return e.attr("href");if(e.attr("content"))return e.attr("content");if(e.attr("datetime"))return e.attr("datetime");if(e.text())return e.text();return},_resolveType:function(e){return e.indexOf("http")>-1?e=e.substr(e.lastIndexOf("/")+1).replace("?","").replace("#",""):e.indexOf(":")>-1&&(e=e.split(":")[1]),e}})})(jQuery);