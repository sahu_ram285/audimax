class ParticipationsController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /participations/new
  # GET /participations/new.json
  def new
    @inspection_object = InspectionObject.find(params[:inspection_object_id])
    @participation = @inspection_object.participations.build
    authorize @participation

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @participation }
    end
  end

  # GET /participations/1/edit
  def edit
    @participation = Participation.find(params[:id])
    authorize @participation
  end

  # POST /participations
  # POST /participations.json
  def create
  
    @inspection_object = InspectionObject.find(params[:inspection_object_id])
    @participation = @inspection_object.participations.build(params[:participation])
    authorize @participation

    respond_to do |format|
      if @participation.save
        format.html { redirect_to inspection_object_url(@participation.inspection_object_id), notice: I18n.t('notices.object_sucessfully_created', model: Participation.model_name.human) }
        format.json { render json: @participation, status: :created, location: @participation }
      else
        format.html { render action: "new" }
        format.json { render json: @participation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /participations/1
  # PUT /participations/1.json
  def update
    @participation = Participation.find(params[:id])
    authorize @participation

    respond_to do |format|
      if @participation.update_attributes(params[:participation])
        format.html { redirect_to inspection_object_url(@participation.inspection_object_id), notice: I18n.t('notices.object_sucessfully_updated', model: Participation.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @participation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /participations/1
  # DELETE /participations/1.json
  def destroy
    @participation = Participation.find(params[:id])
    authorize @participation
    @participation.destroy

    respond_to do |format|
      format.html { redirect_to inspection_object_url(@participation.inspection_object_id) }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end  
  
end
