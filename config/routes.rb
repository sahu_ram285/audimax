Audimax::Application.routes.draw do

  scope "(:locale)", locale: /de|en|nl/ do
  
  constraints :subdomain => /.*client$/ do
    scope :module => 'client', :as => 'client' do
      resources :inspection_objects, :as => :objects, :only => [:index, :show]
      resources :reports, :only => [:show]
      match 'contact' => 'pages#contact'
      root to: 'dashboard#index'
    end
  end

  resources :calibration_methods do
    resources :calibration_method_manuals
  end
  
  resources :calibration_method_manuals, only: [:edit, :destroy, :update]
  
  resources :client_users
  
  resources :clients do
    resources :inspection_objects
  end
    
  resources :companies do
  	member do
  		post 'destroy_image'
  	end
    resources :accreditations, only: [:new, :edit, :create, :update, :destroy]
  end
  resources :complaints
  resources :employees do
    resources :equipment
  end
  resources :equipment do
    resources :maintenances
  end
  resources :equipment_types
  resources :inspection_objects do
    resources :participations, :only => [:new, :create]
  	resources :object_parameters, :only => [:new, :create]
  	resources :object_attachments, :only => [:new, :create]
  end
  resources :maintenance_schemes
  resources :maintenances

  get "management/report_errors"
  get "management/report_errors_per_inspector"
  get "management/complaints"
  get "management/complaints_open"
  get "management/complaints_received"
  get "management/complaints_per_type"
  get "management/complaints_per_priority"
  get "management/complaints_overdue"

  get "managers/dashboard"
  get "managers/reports"
  get "managers/report_assessments"
  get "managers/report_errors_for_each_category"

  resources :notifications, :only => [:index, :destroy] do
    member do
      put 'toggle_read'
    end
  end
  resources :object_parameters, :only => [:destroy, :edit, :update]
  resources :object_attachments, :only => [:destroy, :update]
  resources :participations, :only => [:destroy, :edit, :update]
  resources :positions
  resources :program_assessments, :only => [:edit, :update]
  resources :programs do
  	resources :regulations, :only => [:new, :create]
  	resources :scopes, :only => [:new, :create]
  end
  resources :regulations, :only => [:destroy, :edit, :show, :update] do
  	resources :sections, :only => [:new, :create, :edit, :update]
    member do
      post 'order'
    end
  end
  resources :reports do
    member do
      post 'add_equipment'
    end
  end
  resources :report_equipments, :only => [:destroy]
  resources :report_error_categories
  resources :report_errors
  resources :report_reviews
  resources :report_items, :only => [] do
    resources :report_item_attachments
  end
  resources :report_item_attachments, :only => [:destroy, :update]
  resources :scopes, :only => [:destroy, :edit, :show, :update]
  resources :sections, :only => [:destroy, :edit, :update]

  devise_for :users, controllers: {sessions: "sessions"}, :path_prefix => 'accounts', :skip => [:registrations] 
  as :user do
    get 'accounts/users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put 'accounts/users' => 'devise/registrations#update', :as => 'user_registration'
  end
  
  devise_scope :user do
    get 'accounts/users/verify', to: 'sessions#verify'
  end

  devise_for :client_users, :path_prefix => 'accounts', :skip => [:registrations]
  as :client_user do
    get 'accounts/client_users/edit' => 'client_user/registrations#edit', :as => 'edit_client_user_registration'
    put 'accounts/client_users' => 'client_user/registrations#update', :as => 'client_user_registration'
  end
  
  resources :users, :only => [:index, :new, :edit, :create, :update, :destroy]

  match 'about' => 'pages#about'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'
  root :to => 'managers#dashboard'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
  
end
end
