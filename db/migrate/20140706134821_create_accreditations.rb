class CreateAccreditations < ActiveRecord::Migration
  def change
    create_table :accreditations do |t|
      t.integer :company_id
      t.integer :scope_id

      t.timestamps
    end
  end
end
