class ObjectAttachment < ActiveRecord::Base
  attr_accessible :attachment, :description, :inspection_object_id
  
  mount_uploader :attachment, AttachmentUploader
end
