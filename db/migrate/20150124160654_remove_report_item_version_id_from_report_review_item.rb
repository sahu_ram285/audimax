class RemoveReportItemVersionIdFromReportReviewItem < ActiveRecord::Migration
  def up
    remove_column :report_review_items, :report_item_version_id
  end

  def down
    add_column :report_review_items, :report_item_version_id, :integer
  end
end
