class CreateMaintenanceSchemes < ActiveRecord::Migration
  def change
    create_table :maintenance_schemes do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
