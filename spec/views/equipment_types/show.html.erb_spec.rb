require 'spec_helper'

describe "equipment_types/show" do
  before(:each) do
    @equipment_type = assign(:equipment_type, stub_model(EquipmentType,
      :name => "Name",
      :description => "MyText",
      :calibration_interval => 1,
      :instruction => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(/MyText/)
  end
end
