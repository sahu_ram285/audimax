class ReportsChart
  
  def ticks
    (1..12).map { |n| [n, (Date.today - (12-n).months).strftime("%Y-%m")] }
  end

  # Nr. reports per month in the last 12 months  
  def series_nr_reports_data
    data = Array.new(12, 0)
    Report.select("11 - period_diff(date_format(now(), '%Y%m'), date_format(inspection_date, '%Y%m')) as month, count(*) nr_reports").where("inspection_date > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)").group('1').order('1').each do |r|
      data[r.month] = r.nr_reports
    end
    data.each_with_index.map { |e, i| [i+1, e]}
  end

  # Nr. reviews per month in the last 12 months
  def series_nr_reviews_data
    data = Array.new(12, 0)
		ReportReview.select("11 - period_diff(date_format(now(), '%Y%m'), date_format(created_at, '%Y%m')) as month, count(*) nr_reviews").where("created_at > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)").group('1').order('1').each do |r|
			data[r.month] = r.nr_reviews;
    end
    data.each_with_index.map { |e, i| [i+1, e]}
  end    
  
  def as_json(options = {})
    {
      data: [{
        data: series_nr_reports_data,
        label: I18n.t('charts.reports.nr_reports'),
        color: "#c75d7b"
      }, {
        data: series_nr_reviews_data,
        label: I18n.t('charts.reports.nr_reviews'),
        color: "#c5d52b"
        
      }],
      options: {
  			tooltip: true,
  			series: {
  				lines: {
  					show: true
  				},
  				points: {
  					show: true
  				}
  			},
  			grid: {
  				borderWidth: 0, 
  				hoverable: true,
  				clickable: true
  			},
  			xaxis: {
  				ticks: ticks
  			},
        yaxis: {
          min: 0,
          tickDecimals: 0
        }
      }
    }
  end
  
end
