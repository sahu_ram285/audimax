class CreateReportItemAttachments < ActiveRecord::Migration
  def change
    create_table :report_item_attachments do |t|
      t.integer :report_item_id
      t.string :attachment
      t.text :description

      t.timestamps
    end
  end
end
