require 'spec_helper'

describe "report_error_categories/new" do
  before(:each) do
    assign(:report_error_category, stub_model(ReportErrorCategory,
      :name => "MyString",
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new report_error_category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => report_error_categories_path, :method => "post" do
      assert_select "input#report_error_category_name", :name => "report_error_category[name]"
      assert_select "textarea#report_error_category_description", :name => "report_error_category[description]"
    end
  end
end
