require 'spec_helper'

describe "employees/index" do
  before(:each) do
    assign(:employees, [
      stub_model(Employee,
        :firstname => "Firstname",
        :lastname => "Lastname",
        :gender => "Gender",
        :address => "Address",
        :postcode => "Postcode",
        :city => "City",
        :state => "State",
        :country => "Country",
        :email => "Email",
        :phone1 => "Phone1",
        :phone2 => "Phone2",
        :position_id => 1
      ),
      stub_model(Employee,
        :firstname => "Firstname",
        :lastname => "Lastname",
        :gender => "Gender",
        :address => "Address",
        :postcode => "Postcode",
        :city => "City",
        :state => "State",
        :country => "Country",
        :email => "Email",
        :phone1 => "Phone1",
        :phone2 => "Phone2",
        :position_id => 1
      )
    ])
  end

  it "renders a list of employees" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Firstname".to_s, :count => 2
    assert_select "tr>td", :text => "Lastname".to_s, :count => 2
    assert_select "tr>td", :text => "Gender".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Postcode".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone1".to_s, :count => 2
    assert_select "tr>td", :text => "Phone2".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
