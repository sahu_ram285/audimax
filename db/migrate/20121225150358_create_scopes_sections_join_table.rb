class CreateScopesSectionsJoinTable < ActiveRecord::Migration
	def change
		create_table :scopes_sections, :id => false do |t|
			t.integer :scope_id
			t.integer :section_id
		end
	end
end
