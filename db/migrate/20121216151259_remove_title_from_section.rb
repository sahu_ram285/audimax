class RemoveTitleFromSection < ActiveRecord::Migration
  def up
    remove_column :sections, :title
  end

  def down
    add_column :sections, :title, :string
  end
end
