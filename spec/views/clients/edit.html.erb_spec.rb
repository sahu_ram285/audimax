require 'spec_helper'

describe "clients/edit" do
  before(:each) do
    @client = assign(:client, stub_model(Client,
      :name => "MyString",
      :address => "MyString",
      :postcode => "MyString",
      :city => "MyString",
      :state => "MyString",
      :country => "MyString",
      :phone => "MyString",
      :fax => "MyString",
      :email => "MyString",
      :website => "MyString",
      :pos_address => "MyString",
      :pos_postcode => "MyString",
      :pos_city => "MyString",
      :pos_state => "MyString",
      :pos_country => "MyString",
      :inv_address => "MyString",
      :inv_postcode => "MyString",
      :inv_city => "MyString",
      :inv_state => "MyString",
      :inv_country => "MyString"
    ))
  end

  it "renders the edit client form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => clients_path(@client), :method => "post" do
      assert_select "input#client_name", :name => "client[name]"
      assert_select "input#client_address", :name => "client[address]"
      assert_select "input#client_postcode", :name => "client[postcode]"
      assert_select "input#client_city", :name => "client[city]"
      assert_select "input#client_state", :name => "client[state]"
      assert_select "input#client_country", :name => "client[country]"
      assert_select "input#client_phone", :name => "client[phone]"
      assert_select "input#client_fax", :name => "client[fax]"
      assert_select "input#client_email", :name => "client[email]"
      assert_select "input#client_website", :name => "client[website]"
      assert_select "input#client_pos_address", :name => "client[pos_address]"
      assert_select "input#client_pos_postcode", :name => "client[pos_postcode]"
      assert_select "input#client_pos_city", :name => "client[pos_city]"
      assert_select "input#client_pos_state", :name => "client[pos_state]"
      assert_select "input#client_pos_country", :name => "client[pos_country]"
      assert_select "input#client_inv_address", :name => "client[inv_address]"
      assert_select "input#client_inv_postcode", :name => "client[inv_postcode]"
      assert_select "input#client_inv_city", :name => "client[inv_city]"
      assert_select "input#client_inv_state", :name => "client[inv_state]"
      assert_select "input#client_inv_country", :name => "client[inv_country]"
    end
  end
end
