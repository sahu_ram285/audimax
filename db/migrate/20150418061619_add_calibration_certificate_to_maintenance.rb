class AddCalibrationCertificateToMaintenance < ActiveRecord::Migration
  def change
    add_column :maintenances, :calibration_certificate, :string
  end
end
