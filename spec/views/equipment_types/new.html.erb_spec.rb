require 'spec_helper'

describe "equipment_types/new" do
  before(:each) do
    assign(:equipment_type, stub_model(EquipmentType,
      :name => "MyString",
      :description => "MyText",
      :calibration_interval => 1,
      :instruction => "MyText"
    ).as_new_record)
  end

  it "renders new equipment_type form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", equipment_types_path, "post" do
      assert_select "input#equipment_type_name[name=?]", "equipment_type[name]"
      assert_select "textarea#equipment_type_description[name=?]", "equipment_type[description]"
      assert_select "input#equipment_type_calibration_interval[name=?]", "equipment_type[calibration_interval]"
      assert_select "textarea#equipment_type_instruction[name=?]", "equipment_type[instruction]"
    end
  end
end
