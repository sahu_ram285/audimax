class ReportItemAttachmentsController < ApplicationController
  def new
  end

  def create
    @report_item = ReportItem.find(params[:report_item_id])
    @attachment = @report_item.attachments.build
    @attachment.attachment = params[:file]
    #authorize @attachment
    
    @attachment.save

    render :layout => false
  end

  def update
    @attachment = ReportItemAttachment.find(params[:id])
    #authorize @attachment

    respond_to do |format|
      if @attachment.update_attributes(params[:report_item_attachment])
        format.html { redirect_to reports_path, notice: I18n.t('notices.object_sucessfully_updated', model: ReportItemAttachment.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @attachment = ReportItemAttachment.find(params[:id])
    #authorize @attachment
    @attachment.destroy

    respond_to do |format|
      format.html { redirect_to reports_url }
      format.json { head :no_content }
      format.js
    end
  end
end
