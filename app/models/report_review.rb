class ReportReview < ActiveRecord::Base
  attr_accessible :comments, :report_id, :reviewer_id, :status
  attr_accessible :report_errors_attributes
  
  belongs_to :report
  belongs_to :employee, :foreign_key => :reviewer_id
  has_many :report_errors, :dependent => :destroy
  
  accepts_nested_attributes_for :report_errors, :allow_destroy => true
  
  validates :reviewer_id, :presence => true
  
  def self.report_review_status_values
    [ 'NEW', 'FILED', 'FINAL']
  end
  
  def self.report_review_status_description(status)
    ReportReview.human_attribute_name("status.#{status.downcase}")
  end
  
  def self.report_review_valid_status_transitions(status)
    { 'NEW' => ['NEW', 'FILED', 'FINAL'],
      'FILED' => ['FILED', 'FINAL'], 
      'FINAL' => ['FINAL']
      }[status]
  end
  
  def self.create_review(report_id, reviewer_id)
    new_review = nil
    ReportReview.transaction do 
      report = Report.find(report_id)
      if report.status == 'FILED'
        report.status = 'REVIEW'
        report.save!
      end
    
      new_review = ReportReview.new(:report_id => report_id, :reviewer_id => reviewer_id)
      new_review.status = 'NEW'
      
      new_review.save!
    end
    new_review
  end
  
  def final?
    self.status == 'FINAL'
  end
  
end
