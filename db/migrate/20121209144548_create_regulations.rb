class CreateRegulations < ActiveRecord::Migration
  def change
    create_table :regulations do |t|
      t.integer :program_id
      t.string :title
      t.string :version
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
