class AddImageToEquipmentType < ActiveRecord::Migration
  def change
    add_column :equipment_types, :image, :string
  end
end
