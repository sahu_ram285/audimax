class ComplaintsOverdueChart
  
  def series_label(overdue)
    overdue == 'Y' ? I18n.t('charts.complaints_overdue.ovedue') : I18n.t('charts.complaints_overdue.on_time')
  end
  
  def series_color(overdue)
    overdue == 'Y' ? '#da4f49' : '#5bb75b'
  end
  
  def series_data
    Complaint.select('case when handled_date > planned_handled_date then \'Y\' else \'N\' end as overdue, count(*) as nr_complaints').where("received_date > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month) and planned_handled_date is not null and handled_date is not null and status = \'CLOSED\'").group('overdue').all.map { |c| { label: series_label(c.overdue), data: c.nr_complaints, color: series_color(c.overdue)}}
  end
  
  def as_json(options = {})
    { data: series_data,
      options: {
    		series: {
    			pie: {
    				show: true,
						label: { 
							show: true,
							radius: 0.85
						}
    			}
    		},
    		legend: {
    			show: true,
    		}  
      }
    }
  end
  
end
