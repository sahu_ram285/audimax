class Notification < ActiveRecord::Base
  attr_accessible :user_id, :message, :read
  
  belongs_to :user
  
  after_initialize :default_values
  
  scope :visible, -> { where(deleted: false) }
  
  #
  # send
  # Sends a notification to the user if the notification has not already been sent.
  #
  
  def self.notify(user_id, message)
    notification = Notification.where("user_id = :user_id and message = :message", :user_id => user_id, :message => message).first
    if notification.nil?
      new_notification = Notification.create(:user_id => user_id, :message => message, :read => 'N')
      new_notification.save!
    end
  end
  
  def toggle_read
    self.read = !self.read
  end
  
  def default_values
    if self.deleted.nil?
      self.deleted = false
    end
  end
    
end
