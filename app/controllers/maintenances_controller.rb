class MaintenancesController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /maintenances
  # GET /maintenances.json
  def index
    @maintenances = policy_scope(Maintenance).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @maintenances }
    end
  end

  # GET /maintenances/1
  # GET /maintenances/1.json
  def show
    @maintenance = Maintenance.find(params[:id])
    authorize @maintenance

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @maintenance }
    end
  end

  # GET /maintenances/new
  # GET /maintenances/new.json
  def new
    @equipment = Equipment.find(params[:equipment_id])
    @maintenance = @equipment.maintenances.build
    authorize @maintenance

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @maintenance }
    end
  end

  # GET /maintenances/1/edit
  def edit
    @maintenance = Maintenance.find(params[:id])
    authorize @maintenance
  end

  # POST /maintenances
  # POST /maintenances.json
  def create
  	@equipment = Equipment.find(params[:equipment_id])
    @maintenance = @equipment.maintenances.build(params[:maintenance])
    authorize @maintenance

    respond_to do |format|
      if @maintenance.save
        format.html { redirect_to equipment_url(@maintenance.equipment_id), notice: I18n.t('notices.object_sucessfully_created', model: Maintenance.model_name.human) }
        format.json { render json: @maintenance, status: :created, location: @maintenance }
      else
        format.html { render action: "new" }
        format.json { render json: @maintenance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /maintenances/1
  # PUT /maintenances/1.json
  def update
    @maintenance = Maintenance.find(params[:id])
    authorize @maintenance

    respond_to do |format|
      if @maintenance.update_attributes(params[:maintenance])
        format.html { redirect_to equipment_url(@maintenance.equipment_id), notice: I18n.t('notices.object_sucessfully_updated', model: Maintenance.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @maintenance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maintenances/1
  # DELETE /maintenances/1.json
  def destroy
    @maintenance = Maintenance.find(params[:id])
    authorize @maintenance
    @maintenance.destroy

    respond_to do |format|
      format.html { redirect_to equipment_url(@maintenance.equipment_id) }
      format.json { head :no_content }
    end
  end
  
  def set_current_section
    self.current_section = :assets
  end
  
end
