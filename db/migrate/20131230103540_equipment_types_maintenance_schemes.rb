class EquipmentTypesMaintenanceSchemes < ActiveRecord::Migration
  def up
    create_table :equipment_types_maintenance_schemes, :id => false do |t|
      t.references :equipment_type
      t.references :maintenance_scheme
    end
  end

  def down
    drop_table :equipment_types_maintenance_schemes
  end
end
