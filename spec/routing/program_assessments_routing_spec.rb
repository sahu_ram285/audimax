require "spec_helper"

describe ProgramAssessmentsController do
  describe "routing" do

    it "routes to #index" do
      get("/program_assessments").should route_to("program_assessments#index")
    end

    it "routes to #new" do
      get("/program_assessments/new").should route_to("program_assessments#new")
    end

    it "routes to #show" do
      get("/program_assessments/1").should route_to("program_assessments#show", :id => "1")
    end

    it "routes to #edit" do
      get("/program_assessments/1/edit").should route_to("program_assessments#edit", :id => "1")
    end

    it "routes to #create" do
      post("/program_assessments").should route_to("program_assessments#create")
    end

    it "routes to #update" do
      put("/program_assessments/1").should route_to("program_assessments#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/program_assessments/1").should route_to("program_assessments#destroy", :id => "1")
    end

  end
end
