class ComplaintPolicy < ApplicationPolicy
  def create?
    @user.admin? or @user.has_role(:administrator, :manager, :complaint_handler)
  end
  
  def update?
    @user.admin? or @user.has_role(:administrator, :manager, :complaint_handler)
  end
  
  def destroy?
    @user.admin? or @user.has_role(:administrator, :manager)
  end
end