require 'spec_helper'

describe "report_reviews/new" do
  before(:each) do
    assign(:report_review, stub_model(ReportReview,
      :report_id => 1,
      :status => "MyString",
      :comments => "MyText"
    ).as_new_record)
  end

  it "renders new report_review form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => report_reviews_path, :method => "post" do
      assert_select "input#report_review_report_id", :name => "report_review[report_id]"
      assert_select "input#report_review_status", :name => "report_review[status]"
      assert_select "textarea#report_review_comments", :name => "report_review[comments]"
    end
  end
end
