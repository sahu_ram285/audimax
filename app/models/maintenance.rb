class Maintenance < ActiveRecord::Base
  attr_accessible :calibration_approved, :calibration_certificate, :calibration_method_id, :calibration_result, :employee_id, :equipment_id, :maintenance_date, :maintenance_type, :remove_calibration_certificate
  
  belongs_to :employee
  belongs_to :calibration_method
  
  mount_uploader :calibration_certificate, AttachmentUploader
end
