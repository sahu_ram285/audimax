class CreateReportReviewItems < ActiveRecord::Migration
  def change
    create_table :report_review_items do |t|
      t.integer :report_review_id
      t.integer :report_item_version_id
      t.string :assessment

      t.timestamps
    end
  end
end
