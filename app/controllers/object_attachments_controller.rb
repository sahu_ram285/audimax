class ObjectAttachmentsController < ApplicationController
  
  before_filter :authenticate_user!
  after_filter :verify_authorized
  
  def new
    @inspection_object = InspectionObject.find(params[:inspection_object_id])
    @object_attachment = @inspection_object.object_attachments.build
    authorize @object_attachment

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @object_attachment }
    end
  end

  def create
    @inspection_object = InspectionObject.find(params[:inspection_object_id])
    @attachment = @inspection_object.object_attachments.build
    @attachment.attachment = params[:file]
    authorize @attachment
    
    @attachment.save

    render :layout => false
  end
  
  def update
    @attachment = ObjectAttachment.find(params[:id])
    authorize @attachment

    respond_to do |format|
      if @attachment.update_attributes(params[:object_attachment])
        format.html { redirect_to employees_path, notice: I18n.t('notices.object_sucessfully_updated', model: ObjectAttachment.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @attachment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @attachment = ObjectAttachment.find(params[:id])
    authorize @attachment
    @attachment.destroy

    respond_to do |format|
      format.html { redirect_to inspection_objects_url }
      format.json { head :no_content }
      format.js
    end
  end

end
