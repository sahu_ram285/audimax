class CalibrationMethodManualPolicy < ApplicationPolicy
  def create?
    @user.admin? or @user.has_role(:administrator, :manager, :administrative_assistant)
  end
  
  def update?
    @user.admin? or @user.has_role(:administrator, :manager, :administrative_assistant)
  end
  
  def destroy?
    @user.admin? or @user.has_role(:administrator, :manager, :administrative_assistant)
  end
end