class ClientUser::RegistrationsController < Devise::RegistrationsController
  
  def after_update_path_for(resource)
    client_root_path
  end

end
