require 'spec_helper'

describe "report_reviews/edit" do
  before(:each) do
    @report_review = assign(:report_review, stub_model(ReportReview,
      :report_id => 1,
      :status => "MyString",
      :comments => "MyText"
    ))
  end

  it "renders the edit report_review form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => report_reviews_path(@report_review), :method => "post" do
      assert_select "input#report_review_report_id", :name => "report_review[report_id]"
      assert_select "input#report_review_status", :name => "report_review[status]"
      assert_select "textarea#report_review_comments", :name => "report_review[comments]"
    end
  end
end
