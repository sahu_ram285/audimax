class Dashboard
  
  def number_of_reports
    @number_of_reports = Report.count if @number_of_reports.nil?
    @number_of_reports
  end
  
  def number_of_reviews
    @number_of_reviews = ReportReview.count if @number_of_reviews.nil?
    @number_of_reviews
  end
  
  def review_percentage
    number_of_reports == 0 ? 0 : (number_of_reviews/number_of_reports.to_f)*100
  end
  
  def last_inspection_date
    if @last_report.nil?
      report = Report.select('max(inspection_date) as last_inspection_date').first
      @last_inspection_date = report.last_inspection_date unless report.nil?
    end
    @last_inspection_date
  end

  def last_review_date
    if @last_review_date.nil?
      review = ReportReview.select('max(created_at) as last_review_date').first
      @last_review_date = review.last_review_date unless review.nil?
    end
    @last_review_date.to_date unless @last_review_date.nil?
  end
  
  def reports_per_day
    if @reports_per_day.nil?
      @reports_per_day = Report.select("count(*)/count(distinct inspection_date) as reports_per_day").where("inspection_date between date_sub(now(), interval 30 day) and now()").first.reports_per_day.to_f
    end
    @reports_per_day
  end
  
  def errors_per_report
    if @errors_per_report.nil?
      @errors_per_report = ReportError.select('count(*)/count(distinct report_review_id) errors_per_report').first.errors_per_report.to_f
    end
    @errors_per_report
  end
  
  def nok_assessments_per_report
    if @nok_assessments_per_report.nil?
      @nok_assessments_per_report = ReportItem.select("sum(if(assessment in ('NC1', 'NC2'),1,0))/count(distinct report_id) nr_noks").first.nr_noks.to_f
    end
    @nok_assessments_per_report
  end
  
  def avg_days_before_review
    if @avg_days_before_review.nil?
      @avg_days_before_review = ReportReview.joins(:report).select('avg(datediff(report_reviews.created_at, inspection_date)) avg_days_before_review').first.avg_days_before_review.to_f
    end
    @avg_days_before_review
  end
  
  def number_of_clients
    @number_of_clients = Client.count if @number_of_clients.nil?
  end

  def number_of_objects
    @number_of_objects = InspectionObject.count if @number_of_objects.nil?
  end
  
  def number_of_samples
    0 # TODO: Implement samples
  end
  
  def number_of_complaints
    0 # TODO: Implement complaints
  end
  
end