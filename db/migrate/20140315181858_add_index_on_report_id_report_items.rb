class AddIndexOnReportIdReportItems < ActiveRecord::Migration
  def change
    add_index :report_items, :report_id
  end
end
