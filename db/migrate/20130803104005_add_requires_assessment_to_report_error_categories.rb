class AddRequiresAssessmentToReportErrorCategories < ActiveRecord::Migration
  def change
    add_column :report_error_categories, :requires_assessment, :boolean
  end
end
