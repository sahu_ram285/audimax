class RemoveLastCalibratioDateFromEquipment < ActiveRecord::Migration
  def up
    remove_column :equipment, :last_calibration_date
  end

  def down
    add_column :equipment, :last_calibration_date, :string
  end
end
