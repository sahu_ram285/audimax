class AddUsageToEquipment < ActiveRecord::Migration
  def change
    add_column :equipment, :usage, :string
  end
end
