class AddIndexInspectionDateToReports < ActiveRecord::Migration
  def change
    add_index :reports, :inspection_date
  end
end
