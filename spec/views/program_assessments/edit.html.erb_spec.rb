require 'spec_helper'

describe "program_assessments/edit" do
  before(:each) do
    @program_assessment = assign(:program_assessment, stub_model(ProgramAssessment,
      :code => "MyString",
      :name => "MyString"
    ))
  end

  it "renders the edit program_assessment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", program_assessment_path(@program_assessment), "post" do
      assert_select "input#program_assessment_code[name=?]", "program_assessment[code]"
      assert_select "input#program_assessment_name[name=?]", "program_assessment[name]"
    end
  end
end
