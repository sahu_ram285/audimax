module InspectionObjectsHelper
  
  def link_to_attachment(attachment)
    if !attachment.attachment_url.nil? 
      case File.extname(attachment.attachment_url).downcase
      when '.pdf'
        link_to image_tag(attachment.attachment_url(:thumb).to_s, :size => "48x30"), attachment.attachment_url, :target => "_blank"
      when '.png', '.jpg', '.gif'
        link_to image_tag(attachment.attachment_url(:thumb).to_s, :class => 'img-polaroid', :size => "48x30", :alt => ''), attachment.attachment_url, :rel => 'prettyPhoto'
      when '.doc', '.docx'
        link_to raw("<i class='icol32-page-white-word'></i>"), attachment.attachment_url
      when '.ppt', '.pptx'
        link_to raw("<i class='icol32-page-white-powerpoint'></i>"), attachment.attachment_url
      when '.xls', '.xlsx'
        link_to raw("<i class='icol32-page-white-excel'></i>"), attachment.attachment_url
      when '.zip'
        link_to raw("<i class='icol32-page-white-zip'></i>"), attachment.attachment_url
      when '.m4a', '.mp3', '.wav'
        audio_tag attachment.attachment_url, :controls => true
      else
        link_to raw("<i class='icol32-page-white'></i>"), attachment.attachment_url
      end
    end
  end
  
end
