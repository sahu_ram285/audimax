class AddIntervalToMaintenanceScheme < ActiveRecord::Migration
  def change
    add_column :maintenance_schemes, :interval, :integer
  end
end
