class EmployeesController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /employees
  # GET /employees.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: EmployeesDatatable.new(view_context) }
    end
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    @employee = Employee.find(params[:id])
    authorize @employee

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @employee }
    end
  end

  # GET /employees/new
  # GET /employees/new.json
  def new
    @employee = Employee.new
    authorize @employee

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @employee }
    end
  end

  # GET /employees/1/edit
  def edit
    @employee = Employee.find(params[:id])
    authorize @employee
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(params[:employee])
    authorize @employee

    respond_to do |format|
      if @employee.save
        format.html { redirect_to employees_path, notice: I18n.t('notices.object_sucessfully_created', model: Employee.model_name.human) }
        format.json { render json: @employee, status: :created, location: @employee }
      else
        format.html { render action: "new" }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /employees/1
  # PUT /employees/1.json
  def update
    @employee = Employee.find(params[:id])
    authorize @employee

    respond_to do |format|
      if @employee.update_attributes(params[:employee])
        format.html { redirect_to employees_path, notice: I18n.t('notices.object_sucessfully_updated', model: Employee.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee = Employee.find(params[:id])
    authorize @employee
    @employee.destroy

    respond_to do |format|
      format.html { redirect_to employees_url }
      format.json { head :no_content }
    end
  end
  
  def set_current_section
    self.current_section = :assets
  end
  
end
