require 'spec_helper'

describe "equipment_types/index" do
  before(:each) do
    assign(:equipment_types, [
      stub_model(EquipmentType,
        :name => "Name",
        :description => "MyText",
        :calibration_interval => 1,
        :instruction => "MyText"
      ),
      stub_model(EquipmentType,
        :name => "Name",
        :description => "MyText",
        :calibration_interval => 1,
        :instruction => "MyText"
      )
    ])
  end

  it "renders a list of equipment_types" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
