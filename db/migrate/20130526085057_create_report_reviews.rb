class CreateReportReviews < ActiveRecord::Migration
  def change
    create_table :report_reviews do |t|
      t.integer :report_version_id
      t.string :status
      t.integer :reviewer_id
      t.text :comments

      t.timestamps
    end
  end
end
