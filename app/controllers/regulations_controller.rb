class RegulationsController < ApplicationController

  before_filter :authenticate_user!, :set_current_section
  after_filter :verify_authorized, :except => :index
  
  # GET /regulations/1
  # GET /regulations/1.json
  def show
    @regulation = Regulation.find(params[:id])
    authorize @regulation
    
    @sections = Section.where("sections.regulation_id = ?", params[:id]).order("seq_nr")
    @scope_name = ""
    if params[:scope_id]
    	scope = Scope.find_by_id(params[:scope_id])
    	@scope_name = scope == nil ? nil : scope.name
    	@sections = @sections.for_scope(params[:scope_id])
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @regulation }
      format.xml
    end
  end

  # GET /regulations/new
  # GET /regulations/new.json
  def new
    @program = Program.find(params[:program_id])
    @regulation = @program.regulations.build
    authorize @regulation

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @regulation }
    end
  end

  # GET /regulations/1/edit
  def edit
    @regulation = Regulation.find(params[:id])
    authorize @regulation
  end

  # POST /regulations
  # POST /regulations.json
  def create
  	@program = Program.find(params[:program_id])
    @regulation = @program.regulations.build(params[:regulation])
    authorize @regulation

    respond_to do |format|
      if @regulation.save
        format.html { redirect_to @program, notice: I18n.t('notices.object_sucessfully_created', model: Regulation.model_name.human) }
        format.json { render json: @regulation, status: :created, location: @regulation }
      else
        format.html { render action: "new" }
        format.json { render json: @regulation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /regulations/1
  # PUT /regulations/1.json
  def update
    @regulation = Regulation.find(params[:id])
    authorize @regulation

    respond_to do |format|
      if @regulation.update_attributes(params[:regulation])
        format.html { redirect_to program_url(@regulation.program_id), notice: I18n.t('notices.object_sucessfully_updated', model: Regulation.model_name.human) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @regulation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /regulations/1
  # DELETE /regulations/1.json
  def destroy
    @regulation = Regulation.find(params[:id])
    authorize @regulation
    @regulation.destroy

    respond_to do |format|
      format.html { redirect_to program_url(@regulation.program_id) }
      format.json { head :no_content }
    end
  end
  
  # POST /regulations/1/order
  def order
    authorize Section, :update?
    Regulation.order_sections(params[:order])
    render :nothing => true
  end
  
  private
  
  def set_current_section
    self.current_section = :inspection
  end  
  
end
