class Tokenizer
  
  def initialize
    @app_id = Rails.configuration.tokenizer_app_id
    @app_key = Rails.configuration.tokenizer_app_key
  end

  def generate_token(email)
    url = "https://api.tokenizer.com/v1/authentications.json?app_id=#{@app_id}&app_key=#{@app_key}"
    # don't raise exceptions, but return the result
    response = RestClient.post(url, {:usr_email => email}) { |response, request, result| response }
    JSON.parse(response)
  end
  
  def verify_token(token_id)
    url = "https://api.tokenizer.com/v1/authentication/#{token_id}.json"
    response = RestClient.get url, { :params => { :app_id => @app_id, :app_key => @app_key } }
    JSON.parse(response)
  end
  
  def wait_for_token_verification(token_id, seconds)
    seconds.times do 
      response = verify_token(token_id)
      if response['state'] == 'accepted'
        return true
      end
      sleep 1
    end
    false
  end

end
