class AddIndexToEquipmentOnEmployeeId < ActiveRecord::Migration
  def change
    add_index :equipment, :employee_id
  end
end
