class ReportPolicy < ApplicationPolicy
    
  def create?
    @user.admin? or @user.has_role(:administrator, :manager, :inspector)
  end
  
  def update?
    @user.admin? or @user.has_role(:administrator, :manager) or (@user.has_role(:inspector) and (record.status == 'NEW' or record.status == 'REVIEW'))
  end
  
  def destroy?
    @user.admin? or @user.has_role(:administrator, :manager) or (@user.has_role(:inspector) and (record.status == 'NEW'))
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if @user.respond_to? :client_id
        scope.joins(:inspection_object).where("inspection_objects.client_id = :client_id and reports.status in ('FILED', 'REVIEW', 'FINAL')", :client_id => @user.client_id)
      else
        if @user.admin? or @user.has_role(:administrator, :manager, :reviewer)
          scope
        else
          scope.where(:employee_id => @user.employee_id)
        end
      end
    end
  end

end