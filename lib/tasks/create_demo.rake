namespace :audimax do
  
  def node_to_hash(node, elements)
    Hash[node.element_children.select { |c| elements.include? c.name }.collect { |c| [c.name, c.inner_html]}]
  end
  
  def node_to_hash2(node)
    Hash[node.element_children.collect { |c| [c.name, c.inner_html]}]
  end

  def node_to_hash3(node, elements)
    Hash[node.element_children.select { |c| !elements.include? c.name }.collect { |c| [c.name, c.inner_html]}]
  end
  
  def time
    start_time = Time.now
    yield
    puts "Done (#{(Time.now-start_time).round(1)}s)"
  end
  
  def random_assessment
    r = rand(100)
    r < 90 ? 'COM' : (r < 99 ? 'NC1' : 'NC2')
  end
  
  def create_report(inspection_object, inspection_date)
    inspectors_array = Employee.joins(:position).where('positions.name = "Inspecteur"')
    scope = Scope.find(1)
    
    report = Report.create_report(inspection_object.nr,inspectors_array.sample.id, inspection_date,scope.id)
    
    report.report_items.each do |item|
      if item.section.text.empty?
        item.assessment = 'NA'
      else
        item.assessment = random_assessment
      end
    end
    
    report.status = 'FILED'
    report.comments = Faker::Lorem.paragraph
    report.save!
  end
  
  def create_review(report, review_date)
    managers_array = Employee.joins(:position).where('positions.name = "Technisch Manager"')
    error_categories = ReportErrorCategory.all
    
    report_review = ReportReview.create_review(report.id, managers_array.sample.id)
    report_review.created_at = review_date
    
    report_review.report.report_items.sample(rand(3)).each do |report_item|
      report_review.report_errors.build(:report_item_id => report_item.id, :report_error_category_id => error_categories.sample.id)
    end
    report_review.save!
  end
  
  #
  # Users
  #
  
  def generate_users
    puts "Generating users..."
    Employee.all.each do |employee|
      new_user = User.new
      new_user.email = "#{employee.firstname}.#{employee.lastname}@example.com".downcase.gsub(/ /,'.')
      new_user.password = "user123"
      new_user.password_confirmation = "user123"
      new_user.employee_id = employee.id
      new_user.save!
    end
  end
  
  #
  # Equipment type maintenance schemes
  #

  def generate_equipment_type_maintenance_schemes
    puts "Generate equipment type maintenance schemes..."
    calibration_scheme = MaintenanceScheme.where('maintenance_type = :maintenance_type', :maintenance_type => 'CA').first
    EquipmentType.all.each do |equipment_type|
      equipment_type.maintenance_scheme_ids = [ calibration_scheme.id ]
      equipment_type.save!
    end
  end
  
  #
  # Equipment
  #

  def generate_equipment
    puts "Generating equipment..."
    EquipmentType.all.each do |equipment_type|
      nr = equipment_type.id*1000
      Employee.all.each do |employee|
        serial_nr = equipment_type.name[0..3].upcase+(1000+rand(1000)).to_s+'-'+(1000+rand(1000)).to_s
        employee.equipment.build(:nr => nr, :serial_nr => serial_nr, :usage => "OPERATIONAL", :in_service_date => employee.employment_date, :out_of_service_date => "", :equipment_type_id => equipment_type.id)
        employee.save!
        nr += 1
      end
    end
  end
  
  
  #
  # Maintenance
  #
  
  def generate_maintenance
    puts "Generating maintenance..."
    calibration_method = CalibrationMethod.first
    Equipment.joins(:employee, :equipment_type).includes(:employee, :equipment_type).each do |equipment|
      equipment.equipment_type.maintenance_schemes.each do |maintenance_scheme|
        if maintenance_scheme.maintenance_type == 'CA'
          calibration_method_id = calibration_method.id
          calibration_approved = 'Y'
        else
          calibration_method_id = nil
          calibration_approved = nil
        end
        window = maintenance_scheme.interval * 0.08
        maintenance_date = equipment.in_service_date + maintenance_scheme.interval + rand(-window..window)
        while maintenance_date < Date.today
          puts "Generating maintenance for equipment #{equipment.id} (#{equipment.in_service_date}) and date #{maintenance_date}"
          equipment.maintenances.build(
            :maintenance_type => maintenance_scheme.maintenance_type,
            :maintenance_date => maintenance_date,
            :calibration_approved => calibration_approved,
            :calibration_method_id => calibration_method_id,
            :employee_id => equipment.employee.id)
          equipment.save!
          maintenance_date += maintenance_scheme.interval + rand(-window..window)
        end
      end
    end
  end
  
  #
  # Inspection objects
  #
  
  def generate_objects
    puts "Generating objects..."
    start_time = Time.now
    Client.order('id').each do |client|
      puts "Generating objects for client #{client.id}"
      (1..1+rand(10)).each do |nr|
        new_object = InspectionObject.new
        new_object.nr = "C#{client.id}N#{nr}"
        new_object.name = "#{Bazaar.heroku}"
        new_object.description = "#{Faker::Lorem.paragraph}"
        new_object.address = "#{Faker::Address.street_address}"
        new_object.postalcode = "#{Faker::Address.postcode}"
        new_object.city = "#{Faker::Address.city}"
        new_object.state = "#{Faker::Address.state}"
        new_object.country = "NL"
        new_object.client_id = client.id
        new_object.save!
      end
    end
  end
  
  #
  # Reports
  #
  
  def generate_reports
    puts "Generating reports..."
    InspectionObject.all.each do |inspection_object|
      inspection_date = Date.parse('2012-01-01')
      inspection_date = inspection_date + rand(2*365)
      while inspection_date < Date.today do
        puts "Generating report for object #{inspection_object.id} and date #{inspection_date}"
        create_report(inspection_object, inspection_date)
        inspection_date = inspection_date + rand(2*365)
      end
    end
  end
  
  #
  # Reviews
  #

  def generate_reviews
    puts "Generating reviews..."
    Report.all.each do |report|
      review_date = report.inspection_date + rand(1..3)
      if review_date <= Date.today
        puts "Generating review for report #{report.id} and date #{review_date}"
        create_review(report, review_date)
      end
    end
  end
  
	desc "Create Demo"
	task :create_demo => :environment do
    
    #
    # Creates demo data from demo/demo.xml
    #
    # bundle exec rake db:reset
    # bundle exec rake audimax:create_demo
    #
    
    Faker::Config.locale = 'nl'
      
    f = File.open("demo/demo.xml")
    doc = Nokogiri::XML(f)
    f.close
    
    #
    # Companies
    #
    puts "Creating companies..."
    doc.xpath('/audimax/companies/company').each do |company_node|
      new_company_hash = node_to_hash2(company_node)
      new_company = Company.new(node_to_hash2(company_node).except('image'))
      new_company.save!
      new_company.image.store!(File.open(File.join(Rails.root, "demo/uploads/company/image/#{new_company_hash['image']}")))
      new_company.save!
    end
    puts "Done"
    
    
    #
    # Positions
    #
    puts "Creating positions..."
    doc.xpath('/audimax/positions/position').each do |position_node|
      new_position = Position.new(node_to_hash2(position_node))
      new_position.save!
    end
    puts "Done"
    
    # 
    # Employees
    # 
    puts "Creating employees..."
    doc.xpath('/audimax/employees/employee').each do |employee_node|
      employee_hash = node_to_hash2(employee_node)
      position = Position.where(:name => employee_hash['position']).first
      company = Company.where(:name => employee_hash['company']).first
      new_employee = Employee.new(node_to_hash2(employee_node).except('position', 'company', 'image'))
      new_employee.position = position
      new_employee.company = company
      new_employee.save!
      new_employee.image.store!(File.open(File.join(Rails.root, "demo/uploads/employee/image/#{employee_hash['image']}")))
      new_employee.save!
    end
    puts "Done"
    
    #
    # Clients
    #
    puts "Creating clients..."
    doc.xpath('/audimax/clients/client').each do |client_node|
      new_client = Client.new(node_to_hash2(client_node))
      new_client.save!
    end
    puts "Done"
    
    #
    # Programs
    #
    puts "Creating programs..."
    doc.xpath('/audimax/programs/program').each do |program_node|
      new_program = Program.new(node_to_hash2(program_node))
      new_program.save!
    end
    puts "Done"
    
    #
    # Scopes
    #
    puts "Creating scope..."
    doc.xpath('/audimax/scopes/scope').each do |scope_node|
      scope_hash = node_to_hash2(scope_node)
      program = Program.where(:name => scope_hash['program']).first
      new_scope = Scope.new(scope_hash.except('program'))
      new_scope.program = program
      new_scope.save!
    end
    puts "Done"


    #
    # Scopes
    #
    puts "Creating assessments..."
    doc.xpath('/audimax/assessments/assessment').each do |assessment_node|
      assessment_hash = node_to_hash2(assessment_node)
      program = Program.where(:name => assessment_hash['program']).first
      new_assessment = ProgramAssessment.new(assessment_hash.except('program'))
      new_assessment.program = program
      new_assessment.save!
    end
    puts "Done"
    
    #
    # Report Error categories
    #
    puts "Creating report error categories..."
    doc.xpath('/audimax/report_error_categories/report_error_category').each do |category_node|
      new_category = ReportErrorCategory.new(node_to_hash2(category_node))
      new_category.save!
    end
    puts "Done"    
    
    # 
    # Regulation
    # 
    
    puts "Create regulation..."
    n = node_to_hash(doc.at('/audimax/regulation'), ['title', 'version', 'status', 'start_date', 'end_date'])
    
    program = Program.first
    regulation = program.regulations.build(n)
    program.save!
    
    seq_nr = 1;
    doc.xpath('/audimax/regulation/sections/section').each do |s|
      section_hash = node_to_hash(s, ['nr', 'title', 'text', 'instruction'])
      section_hash['seq_nr'] = seq_nr
      seq_nr += 1
      section = regulation.sections.build(section_hash)
    
      s.xpath('/audimax/scopes/scope/name/text()').each do |scope_name|
        scope = program.scopes.where('name = ?', scope_name.text)
        if !scope.nil?
          section.scopes = scope
        end
      end
      
    end
    regulation.save!
    puts "Done"
    
    #
    # Calibration methods
    #
    
    puts "Creating calibration methods..."
    doc.xpath('/audimax/calibration_methods/calibration_method').each do |calibration_method_node|
      new_calibration_method = CalibrationMethod.new(node_to_hash2(calibration_method_node))
      new_calibration_method.save!
    end
    puts "Done"
    
    #
    # Equipment types
    #
    
    puts "Creating equipment types..."
    doc.xpath('/audimax/equipment_types/equipment_type').each do |equipment_type_node|
      new_equipment_type_hash = node_to_hash2(equipment_type_node)
      new_equipment_type = EquipmentType.new(node_to_hash2(equipment_type_node).except('image', 'manual'))
      new_equipment_type.save!
      new_equipment_type.image.store!(File.open(File.join(Rails.root, "demo/uploads/equipment_type/image/#{new_equipment_type_hash['image']}")))
      new_equipment_type.save!
      #new_equipment_type.manual.store!(File.open(File.join(Rails.root, "demo/uploads/equipment_type/manual/#{new_equipment_type_hash['manual']}")))
      #new_equipment_type.save!
    end
    puts "Done"
    
    #
    # Maintenance schemes
    #
    
    puts "Creating maintenance schemes..."
    doc.xpath('/audimax/maintenance_schemes/maintenance_scheme').each do |maintenance_scheme_node|
      new_maintenance_scheme = MaintenanceScheme.new(node_to_hash2(maintenance_scheme_node))
      new_maintenance_scheme.save!
    end
    puts "Done"
    
    time { generate_users }
    time { generate_equipment_type_maintenance_schemes }
    time { generate_equipment }
    time { generate_maintenance }
    time { generate_objects }
    time { generate_reports }
    time { generate_reviews }

  end

end
