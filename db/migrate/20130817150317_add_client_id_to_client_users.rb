class AddClientIdToClientUsers < ActiveRecord::Migration
  def change
    add_column :client_users, :client_id, :integer
  end
end
