class ReportReviewsDatatable
  delegate :current_user, :button_to, :params, :h, :link_to, :raw, :edit_report_review_path, :report_review_status_label, :policy, :policy_scope, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: policy_scope(ReportReview).count,
      iTotalDisplayRecords: report_reviews.total_entries,
      aaData: data
    }
  end

private

  def data
    report_reviews.map do |report_review|
      [
        report_review.id,
        report_review_status_label(report_review.status),
        report_review.created_at.to_date,
        report_review.employee.fullname,
        report_review.report_id,
        report_review.report.inspection_date,
        report_review.report.employee.fullname,
        report_review.report.inspection_object.nr,
				'<span class="btn-group">'+
          link_to(raw("<i class='icon-search'></i>"), report_review, { :class => 'btn btn-small' })+
          (policy(report_review).edit? ? link_to(raw("<i class='icon-pencil'></i>"), edit_report_review_path(report_review), { :class => 'btn btn-small' }) : "")+
          (policy(report_review).destroy? ? link_to(raw("<i class='icon-trash'></i>"), report_review, method: :delete, data: { confirm: I18n.t('form.confirm_delete') }, :class => 'btn btn-small') : "")+
          '</span>'
      ]
    end
  end

  def report_reviews
    @report_reviews ||= fetch_report_reviews
  end

  def fetch_report_reviews
    report_reviews = policy_scope(ReportReview).order("#{sort_column} #{sort_direction}")
    report_reviews = report_reviews.joins(report: :inspection_object).joins(:employee).includes(report: :inspection_object).includes(:employee)
    
    if params[:id].present?
      report_reviews = report_reviews.where("report_reviews.id = :id", :id => params[:id])
    end
    if params[:report_id].present?
      report_reviews = report_reviews.where("report_reviews.report_id = :report_id", :report_id => params[:report_id])
    end
    if params[:reviewer_id].present?
      report_reviews = report_reviews.where("report_reviews.reviewer_id = :reviewer_id", :reviewer_id => params[:reviewer_id])
    end
    if params[:employee_id].present?
      report_reviews = report_reviews.where("reports.employee_id = :employee_id", :employee_id => params[:employee_id])
    end
    if params[:status].present?
      report_reviews = report_reviews.where("report_reviews.status = :status", :status => params[:status])
    end

    report_reviews = report_reviews.page(page).per_page(per_page)
    report_reviews
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[report_reviews.id report_reviews.status report_reviews.created_at employees.lastname report_reviews.report_id reports.inspection_date inspection_objects.nr]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end