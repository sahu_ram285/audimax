class InspectionObjectPolicy < ApplicationPolicy
  def create?
    @user.admin? or @user.has_role(:administrator, :manager, :administrative_assistant)
  end
  
  def update?
    @user.admin? or @user.has_role(:administrator, :manager, :administrative_assistant)
  end
  
  def destroy?
    @user.admin? or @user.has_role(:administrator, :manager, :administrative_assistant)
  end
  
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if @user.respond_to? :client_id
        scope.where('client_id = :client_id', :client_id => @user.client_id)
      else
        scope
      end
    end
  end
  
end