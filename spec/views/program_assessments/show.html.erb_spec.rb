require 'spec_helper'

describe "program_assessments/show" do
  before(:each) do
    @program_assessment = assign(:program_assessment, stub_model(ProgramAssessment,
      :code => "Code",
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Code/)
    rendered.should match(/Name/)
  end
end
