module ComplaintsHelper
  
  def complaint_type_select_options
    Complaint.complaint_type_values.map { |v| [ Complaint.complaint_type_description(v), v ] }
  end
  
  def complaint_status_select_options(status)
    Complaint.complaint_valid_status_transitions(status).map { |v| [ Complaint.complaint_status_description(v), v ] }
  end

  def complaint_status_select_options_all
    Complaint.complaint_status_values.map { |v| [ Complaint.complaint_status_description(v), v ] }
  end

  def complaint_priority_select_options
    Complaint.complaint_priority_values.map { |v| [ Complaint.complaint_priority_description(v), v ] }
  end

  def complaint_status_class(status)
    { 'NEW' => '',
      'ACKNOWLEDGED' => 'label-warning',
      'ASSESSED' => 'label-warning',
      'INVESTIGATED' => 'label-warning',
      'RESOLUTION' => 'label-warning',
      'INFORMED' => 'label-warning',
      'CORRECTION' => 'label-warning',
      'VERIFIED' => 'label-warning',
      'CLOSED' => 'label-success' }[status]
  end
  
  def complaint_status_span(status)
    raw("<span class=\"label #{complaint_status_class(status)}\">#{Complaint.complaint_status_description(status)}</span>")
  end

  def complaint_priority_class(priority)
    { '1' => 'badge-important',
      '2' => 'badge-warning',
      '3' => 'badge-info',
      '4' => 'badge-inverse',
      '5' => ''}[priority]
  end

  def complaint_priority_span(priority)
    priority.blank? ? "" : raw("<span class=\"badge #{complaint_priority_class(priority)}\">#{priority}</span>")
  end
  
end
