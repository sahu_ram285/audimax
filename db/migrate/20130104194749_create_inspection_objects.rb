class CreateInspectionObjects < ActiveRecord::Migration
  def change
    create_table :inspection_objects do |t|
      t.string :nr
      t.string :name
      t.text :description
      t.string :address
      t.string :postalcode
      t.string :city
      t.string :state
      t.string :email
      t.string :website

      t.timestamps
    end
  end
end
