require 'spec_helper'

describe "maintenances/index" do
  before(:each) do
    assign(:maintenances, [
      stub_model(Maintenance,
        :equipment_id => 1,
        :maintenance_type => "Maintenance Type",
        :employee_id => 2,
        :calibration_result => 1.5,
        :calibration_approved => "Calibration Approved"
      ),
      stub_model(Maintenance,
        :equipment_id => 1,
        :maintenance_type => "Maintenance Type",
        :employee_id => 2,
        :calibration_result => 1.5,
        :calibration_approved => "Calibration Approved"
      )
    ])
  end

  it "renders a list of maintenances" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Maintenance Type".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Calibration Approved".to_s, :count => 2
  end
end
