class AddMaintenanceSchemeIdToEquipmentType < ActiveRecord::Migration
  def change
    add_column :equipment_types, :maintenance_scheme_id, :integer
  end
end
