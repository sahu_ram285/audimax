/*
 * MWS Admin v2.1 - FileInput Plugin JS
 * This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
 * All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
 * Last Updated:
 * December 08, 2012
 *
 * 'Highly configurable' mutable plugin boilerplate
 * Author: @markdalgleish
 * Further changes, comments: @addyosmani
 * Licensed under the MIT license
 *
 */
(function(e,t,n,r){function i(e,t){arguments.length&&this._init(e,t)}i.prototype={defaults:{placeholder:"No file selected...",buttontext:"Browse..."},_init:function(t,n){this.element=e(t),this.options=e.extend({},this.defaults,n,this.element.data()),this._build()},_build:function(){this.element.css({position:"absolute",top:0,right:0,margin:0,cursor:"pointer",fontSize:"999px",opacity:0,zIndex:999,filter:"alpha(opacity=0)"}).on("change.fileupload",e.proxy(this._change,this)),this.container=e('<div class="fileinput-holder" style="position: relative;"></div>').append(e('<input type="text" class="fileinput-preview" style="width: 100%;" readonly="readonly" />').attr("placeholder",this.options.placeholder)).append(e('<span class="fileinput-btn btn" type="button" style="display:block; overflow: hidden; position: absolute; top: 0; right: 0; cursor: pointer;"></span>').text(this.options.buttontext)).insertAfter(this.element);var t=this.container.find(".fileinput-btn");this.element.appendTo(t),this.container.find(".fileinput-preview").css("paddingRight",t.outerWidth()+4+"px")},_change:function(e){var t=e.target.files!==r?e.target.files[0]:{name:e.target.value.replace(/^.+\\/,"")};if(!t)return;this.container.find(".fileinput-preview ").val(t.name)}},e.fn.fileInput=function(e){return this.each(function(){new i(this,e)})}})(jQuery,window,document);