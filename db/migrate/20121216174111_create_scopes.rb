class CreateScopes < ActiveRecord::Migration
  def change
    create_table :scopes do |t|
      t.integer :program_id
      t.string :name
      t.decimal :visiting_frequency

      t.timestamps
    end
  end
end
