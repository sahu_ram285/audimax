class RemoveInspectorIdFromReport < ActiveRecord::Migration
  def up
    remove_column :reports, :inspector_id
  end

  def down
    add_column :reports, :inspector_id, :integer
  end
end
