class InspectionObject < ActiveRecord::Base
  attr_accessible :address, :city, :description, :email, :name, :nr, :postalcode, :client_id, :country, :state, :website, :latitude, :longitude
  
  belongs_to :client
  has_many :object_parameters, :order => 'name', :dependent => :destroy
  has_many :object_attachments, :dependent => :destroy
  has_many :participations, :dependent => :restrict
  has_many :scopes, :through => :participations
  has_many :reports, :dependent => :restrict
  
  validates :client_id, :presence => true
  
  geocoded_by :geocode_address
  after_validation :geocode, :if => :geocode_address_changed?

  def geocode_address
    [self.address, self.city, self.state, self.country].compact.join(', ')
  end
  
  def geocode_address_changed?
    self.address_changed? || self.city_changed? || self.state_changed? || self.country_changed?
  end
  
  def nr_reports
    Report.where('inspection_object_id = ?', self.id).count
  end
  
  def last_inspection_date
    Report.where('inspection_object_id = ?', self.id).select('max(inspection_date) as inspection_date').first.inspection_date
  end
  
end
