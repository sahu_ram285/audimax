class ReportErrorsChart

  def ticks
    (1..12).map { |n| [n, (Date.today - (12-n).months).strftime("%Y-%m")] }
  end
  
  def series_count
    1
  end
  
  def series_data(series)
    data = Array.new(12, 0)
     ReportError.joins(:report_review).select("11 - period_diff(date_format(now(), '%Y%m'), date_format(report_reviews.created_at, '%Y%m')) as month, count(report_errors.id)/count(distinct report_reviews.id) as avg_errors").where("report_reviews.created_at > date_sub(date_format(now(), '%Y-%m-01'), interval 11 month)").group("1").order("1").each_with_index do |e|
      data[e.month] = e.avg_errors.to_f.round(1)
    end
    data.each_with_index.map { |e, i| [i+1, e]}
  end
  
  def series_title(series)
    I18n.t('charts.report_errors.average_errors')
  end
  
  def as_json(options = {})
    {
      data: [{
        data: series_data(0),
      	label: series_title(0),
      	color: "#c75d7b"
      }],
      options: {
        tooltip: true,
        series: {
          lines: {
        	  show: true
        	},
        	points: {
        	  show: true
        	}
        },
				grid: {
          borderWidth: 0, 
        	hoverable: true,
        	clickable: true
        },
        xaxis: {
          ticks: ticks
        },
        yaxis: {
          min: 0
        }
      }
    }
  end

end
