class Client::PagesController < ApplicationController
  layout 'client'
  
  before_filter :authenticate_client_user!
  after_filter :verify_authorized, :except => :contact
  
  def contact
    @company = Company.first
  end
  
  def pundit_user
    current_client_user
  end
  
end
