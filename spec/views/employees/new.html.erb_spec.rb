require 'spec_helper'

describe "employees/new" do
  before(:each) do
    assign(:employee, stub_model(Employee,
      :firstname => "MyString",
      :lastname => "MyString",
      :gender => "MyString",
      :address => "MyString",
      :postcode => "MyString",
      :city => "MyString",
      :state => "MyString",
      :country => "MyString",
      :email => "MyString",
      :phone1 => "MyString",
      :phone2 => "MyString",
      :position_id => 1
    ).as_new_record)
  end

  it "renders new employee form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => employees_path, :method => "post" do
      assert_select "input#employee_firstname", :name => "employee[firstname]"
      assert_select "input#employee_lastname", :name => "employee[lastname]"
      assert_select "input#employee_gender", :name => "employee[gender]"
      assert_select "input#employee_address", :name => "employee[address]"
      assert_select "input#employee_postcode", :name => "employee[postcode]"
      assert_select "input#employee_city", :name => "employee[city]"
      assert_select "input#employee_state", :name => "employee[state]"
      assert_select "input#employee_country", :name => "employee[country]"
      assert_select "input#employee_email", :name => "employee[email]"
      assert_select "input#employee_phone1", :name => "employee[phone1]"
      assert_select "input#employee_phone2", :name => "employee[phone2]"
      assert_select "input#employee_position_id", :name => "employee[position_id]"
    end
  end
end
