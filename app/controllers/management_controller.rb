class ManagementController < ApplicationController
  
  before_filter :authenticate_user!, :set_current_section
  
  def report_errors
    @errors_per_category_chart = ReportErrorsPerCategoryChart.new
    respond_to do |format|
      format.html
      format.json { render json: ReportErrorsChart.new }
    end
  end
  
  def report_errors_per_inspector
    respond_to do |format|
      format.json { render json: ReportErrorsPerInspectorChart.new(params[:category_id]) }
      #format.json { render json: ReportErrorsPerInspectorChart.new(9) }
    end
  end
  
  def complaints
    @complaints_overdue_chart = ComplaintsOverdueChart.new
    @complaints_dashboard = ComplaintsDashboard.new
    
    respond_to do |format|
      format.html
    end
  end
  
  # chart
  def complaints_open
    respond_to do |format|
      format.json { render json: ComplaintsOpenChart.new}
    end
  end
  
  # chart
  def complaints_received
    respond_to do |format|
      format.json { render json: ComplaintsReceivedChart.new}
    end
  end
  
  # chart
  def complaints_per_type
    respond_to do |format|
      format.json { render json: ComplaintsPerTypeChart.new}
    end
  end
  
  # chart
  def complaints_per_priority
    respond_to do |format|
      format.json { render json: ComplaintsPerPriorityChart.new}
    end
  end
  
  #chart
  def complaints_overdue
    respond_to do |format|
      format.json { render json: ComplaintsOverdueChart.new}
    end
  end
  
  def set_current_section
    self.current_section = :management
  end 

end
