class AddAssessmentToReportItemErrors < ActiveRecord::Migration
  def change
    add_column :report_item_errors, :old_assessment, :string
    add_column :report_item_errors, :new_assessment, :string
  end
end
