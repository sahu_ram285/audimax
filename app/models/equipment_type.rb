class EquipmentType < ActiveRecord::Base
  attr_accessible :description, :image, :maintenance_scheme_ids, :manual, :manufacturer, :model, :name, :remove_image
  
  has_and_belongs_to_many :maintenance_schemes
  
  mount_uploader :image, ImageUploader
  mount_uploader :manual, AttachmentUploader
end
